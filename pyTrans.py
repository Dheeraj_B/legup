from numpy import *
from scipy.integrate import quad

a1 = 1.710329;
a2 = -0.747274
b0 = 0.009236
b1 = 0.018472
b2 = 0.009236

def computePower():
	with open("h_of_z") as f:
		content = f.readlines()
	for entry in content:
		funString = "h = lambda z: "+entry
		exec(funString) in globals()
		mod = lambda w: abs(h(exp(-1j*w)))**2
		c = quad(mod,-pi,pi)[0]/(2*pi)
		print c
		print h(1)


#computePower()
