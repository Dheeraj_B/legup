Installation Instructions:  
0) Download and copy the Verilog directory files into the directory legup-4.0/llvm/lib/Target/Verilog/ in your machine  
1) Follow the initial steps described in http://legup.eecg.utoronto.ca/docs/4.0/gettingstarted.html#getstarted upto the session "Compile LegUp Source"  
2) Manually install dragonegg by sudo apt-get  
3) unzip the tar file of legup and go into the directory "legup-4.0"  
4) Manually install graphviz by following the readme in the legup-4.0/gui/scheduleviewer/ directory in your machine  
5) Download the makefile from this repo and copy it in your legup-4.0 directory and type "make" inside the legup root directory  
6) Follow the steps in http://legup.eecg.utoronto.ca/docs/4.0/gettingstarted.html#getstarted to add paths to your ModelSim and Quartus location  

Running the tool:  
1) Download and copy the following files into your legup-4.0/examples directory

    - examples/makefile.common
    - examples/legup.tcl

2) Download and copy the following files into your legup-4.0/examples/sra directory

    - examples/sra/Makefile
    - examples/sra/ac_int.h
    - examples/sra/sra.cpp

3) Type "make" in the examples/sra directory