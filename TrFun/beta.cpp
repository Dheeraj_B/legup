#include<iostream>
#include<set>
#include<map>
#include<stack>
#include<vector>
#include<cassert>
#include<stack>

using namespace std;

size_t clk = 0;
std::map<size_t,std::vector<size_t> > adjList;
std::map<size_t,std::vector<size_t> > revadjList;
std::set<size_t> deletedVertices;
std::set<size_t> visited;
std::set<size_t> workVert;
size_t sinkNode;	// node with highest post order number


void explore(size_t u);
void DFS();
void span(size_t src,std::set<size_t>& cc);
size_t spanCC(size_t src,std::set<size_t>& cc);

int main()
{
	size_t N, cpyN, u,v, degree;
	cin>>N;
	cpyN = N;
	while(N>0)
	{
		N--;
		cin>>u;
		workVert.insert(u);
		cin>>degree;
		while(degree>0)
		{
			degree--;
			cin>>v;
			adjList[u].push_back(v);
			revadjList[v].push_back(u);
		}
	}
	DFS();
	cout<<"found sink node: "<<sinkNode<<endl;
	std::set<size_t> cc;
	spanCC(sinkNode,cc);
	assert(cc.size() > 0);
	cout<<"contents of sink SCC:"<<endl;
	for(std::set<size_t>::iterator vb = cc.begin(), ve = cc.end(); vb!=ve; ++vb)
	{
		cout<<*vb<<" ";
		deletedVertices.insert(*vb);
	}
	cout<<endl;
	
	DFS();
	cout<<"found sink node: "<<sinkNode<<endl;
	cc.clear();
	spanCC(sinkNode,cc);
	assert(cc.size() > 0);
	cout<<"contents of sink SCC:"<<endl;
	for(std::set<size_t>::iterator vb = cc.begin(), ve = cc.end(); vb!=ve; ++vb)
	{
		cout<<*vb<<" ";
		deletedVertices.insert(*vb);
	}
	cout<<endl;
	
	DFS();
	cout<<"found sink node: "<<sinkNode<<endl;
	cc.clear();
	spanCC(sinkNode,cc);
	assert(cc.size() > 0);
	cout<<"contents of sink SCC:"<<endl;
	for(std::set<size_t>::iterator vb = cc.begin(), ve = cc.end(); vb!=ve; ++vb)
	{
		cout<<*vb<<" ";
		deletedVertices.insert(*vb);
	}
	cout<<endl;
	
	
}

void explore(size_t u)
{
	visited.insert(u);
	for(size_t v = 0; v < revadjList[u].size(); ++v)
	{
		if(deletedVertices.find(revadjList[u][v]) == deletedVertices.end() && visited.find(revadjList[u][v]) == visited.end())
			explore(revadjList[u][v]);
	}
	sinkNode = u;
}

void DFS()
{
	clk = 0;
	visited.clear();
	
	for(std::map<size_t,std::vector<size_t> >::iterator u = revadjList.begin(), e = revadjList.end();
		u != e; ++u)
		{
			if(deletedVertices.find(u->first) == deletedVertices.end() && visited.find(u->first) == visited.end())
				explore(u->first);
		}
			
	
}

void span(size_t u,std::set<size_t>& cc, size_t& min)
{
	if(min > u)
		min = u;
	cc.insert(u);
	visited.insert(u);
	for(size_t v = 0; v < adjList[u].size(); ++v)
	{
		if(deletedVertices.find(adjList[u][v]) == deletedVertices.end() && visited.find(adjList[u][v]) == visited.end())
			span(adjList[u][v],cc);
	}
	
}

// wrapper for connected components algo
size_t spanCC(size_t src,std::set<size_t>& cc) // cc: set of nodes in a sink SCC
{
	size_t min = 99999;
	visited.clear();
	span(src,cc,min);
}

std::vector<size_t> st;
std::set<size_t> blockedSet;
std::map<size_t,size_t> blockedMap;

bool circuit(size_t u, std::set<size_t>& cc, size_t src)
{
	bool f = false;
	jvisited.insert(u);
	
	if(u == src)
	{
		cout<<"cycle: ";
		for(size_t k = 0; k < st.size(); k++)
			cout<<st[k]<<" ";
		cout<<u<<endl;
		return true;
	}
	
	st.push_back(u);
	if(blockedSet.find(u) == blockedSet.end())
		blockedSet.insert(u);
	else 	// should not explore in that direction
	{
		st.pop_back();
		return false;
	}
	
	for(size_t v = 0; v < adjList[u].size(); ++v)
	{
		if(cc.find(adjList[u][v]) == cc.end()) continue;

		if(deletedVertices.find(adjList[u][v]) == deletedVertices.end() && jvisited.find(adjList[u][v]) != jvisited.end())
		{
			f = f || circuit(adjList[u][v],cc); // becomes true iff atleast one cycle thru u is found
			if(blockedSet.find(adjList[u][v]) != blockedSet.end())
			{
				assert(blockedMap.find(adjList[u][v]) == blockedMap.end());
				blockedMap[adjList[u][v]] = u;
			}
		}
	}
	
	st.pop_back(); // delete from stack but not from blocked set
	// if all neighbours are explored and found a cycle means unblock
	if(f)
	{
		unblock(u);
	}
	return f;
}

void johnson(std::set<size_t> cc, size_t min)
{
	st.push(min);
	blockedSet.insert(min);
	circuit(min,cc,src);	// ignore cycle at first time
	deletedVertices.insert(min);
	
}

void unblock(size_t u)
{
	if(blockedMap.find(blockedMap[u]) != blockedMap.end())
		unblock(blockedMap[u]);
	
	if(blockedMap.find(u) != blockedMap.end())
		blockedMap.erase(u);
	blockedSet.erase(u);
}
