#include<iostream>
#include<set>
#include<map>
#include<vector>
#include<cassert>
#include<stack>
#include<string>
#include<algorithm>
#include<fstream>
#include<cstdio>
#include<memory>
#include<sstream>
#include<utility>
#include<cmath>
#include<iomanip>

#include "pow2.h"

#define MINOPWL 1
#define MAXOPWL 64
#define CONSTWL 32

using namespace std;

/*
22
0 1 1 1
1 2 8 9 1
2 1 3 1
3 1 11 1
4 1 0 a1
5 1 2 b1
6 1 1 a2
7 1 3 b2
8 1 2 b0
9 3 4 10 5 z
10 2 6 7 z
11 1 12 1
12 2 19 20 1
13 1 14 1
14 0 1
15 1 11 a1
16 1 13 b1
17 1 12 a2
18 1 14 b2
19 1 13 b0
20 3 15 21 16 z
21 2 17 18 z
0 14  
*/

std::map<string,double> cacheMap;

bool tr = false;
double minNoise = -80;
//double minSNR = 75;
double sigPower = 4;
ofstream Out;
size_t clk = 0;
size_t pctr = 0;
size_t lctr = 0;
size_t numNodes;
std::map<size_t,std::vector<size_t> > adjList;
std::map<size_t,std::vector<size_t> > revadjList;
std::map<size_t,std::vector<size_t> > adjList_cpy;
std::map<size_t,std::vector<size_t> > revadjList_cpy;
std::set<size_t> constIndices;
std::set<size_t> toReset;

std::set<size_t> visited;
std::map<size_t,size_t,std::greater<size_t> >post2v;
std::map<size_t,size_t> v2post;
std::set<size_t> work;
std::set<size_t> deletedVertices;
std::map<size_t,string> weights;
std::vector<size_t> pathStack;
std::map<size_t,bool> visitedNodes;

std::map<size_t,std::vector<size_t> > paths;
std::map<size_t,std::vector<size_t> > loops;
std::vector<std::vector<bool> > pathsNloops;
std::vector<std::vector<bool> > loopsNloops;
std::vector<std::string> pathWeights;
std::vector<std::string> loopWeights;
std::map<size_t,bool> liveLoops;

void enumPaths(size_t src, size_t dst);
void pathDFS(size_t& src, size_t& dst);
void printPathStack(size_t dst);

std::vector<std::set<size_t> > enumSCC();
void explore(size_t u);
void DFS();
void findSCC(size_t u,std::set<size_t>& cc, bool del = true);
std::vector<std::set<size_t> > processDecrPost();
void johnson();
bool circuit(size_t u, size_t src);
void unblock(size_t u);

void sortPaths();
void sortLoops();
bool merge(std::vector<size_t>& a, std::vector<size_t>& b, size_t beg1 = 0, size_t beg2 = 0);
std::string getNetWeight(std::vector<size_t> nodes);
void buildpathsNloops();
void buildloopsNloops();
std::string getProductTerms(size_t n, size_t r, int pathindex = -1);
void buildDenominator();
void buildNumerator();
void gBreak(size_t node);
void gWane();
void selectLiveLoops(size_t removedNode);
void killNonReachableLoops(size_t node);
std::string exec(const char* cmd);

size_t numMults = 0;
std::vector<int> wMax;
std::map<size_t,double> multConstants;
std::vector<size_t> WL;
std::vector<std::vector<double> >hMean; // operators + output + input 
std::vector<std::vector<double> >hVar;
std::vector<std::vector<double> >xtendedMean;
std::vector<std::vector<double> >xtendedVar;
std::vector<std::vector<size_t> > depList;
std::map<size_t,std::vector<size_t> > regDrivers;

void createExtensions();
std::pair<double,double> getOperatorStats(size_t i, std::vector<int> WL);
std::pair<double,double> getStats(double q, int k, int l);
double calculateSNR(std::vector<int> WL);
size_t getNumTrailingZeroes(float x, size_t f);
void houseKeepRegWidths(std::vector<int>& WM, size_t j, int mode = -1);
std::vector<int> getMWC();
void fillRegDrivers(size_t i);
std::vector<int> greedyAscent(std::vector<int> WL);
int findMaxIndex(std::vector<double> arr, std::set<int> prohibitedIndices);
double getCost(std::vector<int> WL);
std::vector<int> tabuSearch(std::vector<int> WL);
int findMaxIndex(std::vector<double> arr);
int findMinIndex(std::vector<double> arr);


std::string exec(const char* cmd) {
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) return "ERROR";
    char buffer[128];
    std::string result = "";
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}

int main(int argc, char *argv[])
{
	assert(argc > 1);

	minNoise = std::stod(argv[1]);
	
/*	multConstants[5] = 1.710329;
	multConstants[7] = -0.747274;
	multConstants[9] = 0.009236;
	multConstants[6] = 0.018472;
	multConstants[8] = 0.009236;
	multConstants[16] = 1.710329;
	multConstants[18] = -0.747274;
	multConstants[20] = 0.009236;
	multConstants[17] = 0.018472;
	multConstants[19] = 0.009236;*/
	
/*	multConstants[7] = 1.710329;
	multConstants[8] = -0.747274;*/
	
/*	multConstants[1] = 0.125;
	multConstants[2] = 0.125;
	multConstants[3] = 0.125;
	multConstants[4] = 0.125;
	multConstants[5] = 0.125;
	multConstants[6] = 0.125;
	multConstants[7] = 0.125;
	multConstants[8] = 0.125;*/
	
	std::vector<size_t> regIndices;
	
	size_t N, cpyN, u,v, degree,src,dst,numInps,numOuts,cpyIn, cpyOut;
	std::vector<size_t> PI,PO;
	std::set<size_t> outs;
	cin>>N;
	numNodes = N;
	while(N>0)
	{
		N--;
		cin>>u;
		work.insert(u);
		cin>>degree;
		while(degree>0)
		{
			degree--;
			cin>>v;
			adjList[u].push_back(v);
			revadjList[v].push_back(u);
		}
		std::string wt;
		cin>>wt;
		if(wt == "z") regIndices.push_back(u);
		if(wt != "z" && wt != "1")
		{
			multConstants[u] = std::stod(wt);
			numMults++;
		}
		weights[u] = wt;
	}
	cin>>numOuts;
	cpyOut = numOuts;
	while(cpyOut > 0)
	{
		cpyOut--;
		cin>>u;
		PO.push_back(u);
		outs.insert(u);
	}

	cout<<"-------------------\n";
	cout<<adjList.size()<<"\n";
	cout<<"\n";
	for(size_t i = 0; i<regIndices.size(); i++)									
	{
		fillRegDrivers(regIndices[i]);
	}
	
	johnson();
	bool init = true;
	for(std::vector<size_t>::iterator ib = PO.begin(), ie = PO.end(); ib!=ie; ib++)
	{
		Out.open("h_of_z");
		dst = *ib;
		size_t ctr = 0;
		weights[numNodes] = "1";
		while(ctr < numNodes)
		{
			if(ctr == dst) // no path from dst to dst since no self loops are present 
			{	
				//cout<<"detect "<<ctr<<"\n";
				ctr++;
				Out<<"(1/1)\n";
				continue; 
			}
	//		cout<<"node: ------------------------------------------ "<<ctr<<endl;
			gBreak(ctr);
			//cout<<"******* paths ***********\n";
			enumPaths(numNodes,dst);
			sortPaths();
			if(init)
			{
				sortLoops();		
			}
			toReset.clear();
			selectLiveLoops(ctr);
			buildpathsNloops();
			if(init)
			{
				buildloopsNloops();
				init = false;
			}
			buildNumerator();
			for(std::set<size_t>::iterator beg = toReset.begin(), en = toReset.end(); beg != en; ++beg)
				liveLoops[*beg] = true;
			buildDenominator();
			gWane();
			//if(ctr == 9)	assert(0);
			ctr++;
		}
		
	// TODO: while breaking edges no new loops are created. so no need to run johnson again	
		
		Out.close();
		
		exec("g++ -I/usr/include/python2.7 -L/usr/lib/python2.7/config/ -o py callback.cpp -lpython2.7 -ldl -lm -lutil -lz -pthread");
		std::string out = exec("./py pyTrans computePower");
		std::stringstream ss;
		ss<<out;
		double power;
		double avg;
	//	cout<<"printing powers:\n";
		std::vector<double> m,v;
		while(ss>>power)
		{
			ss>>avg;
			cout<<"power: "<<power<<" avg: "<<avg<<"\n";
			m.push_back(avg);
			v.push_back(power);
			//cout<<power<<"\n";
		}
		hMean.push_back(m);
		hVar.push_back(v);

	}
	
	createExtensions();
//	cout<<"const index size: "<<constIndices.size()<<endl;
	wMax.clear();
	wMax.resize(xtendedMean[0].size());
	cout<<"&& "<<wMax.size()<<"\n";
	std::vector<int> mwc;
	double fin, cpy;
	cpy = minNoise;
//	minNoise = minNoise - 40;
	int iteration = 1;
	
/*	for(int i = 0; i < xtendedMean.size(); i++)
	{
		cout<<i<<" : "<<endl;
		
		for(int j = 0; j<xtendedMean[i].size(); j++)
			cout<<xtendedMean[i][j]<<" ";
		cout<<"\n";

		for(int j = 0; j<xtendedVar[i].size(); j++)
			cout<<xtendedVar[i][j]<<" ";
		cout<<"\n";
		
	}
	assert(0);*/
	
	do
	{
		if(iteration == 11)	break;
		
		cout<<"iteration: "<< iteration++ <<"\n";
		
		//std::fill(wMax.begin(),wMax.end(),MAXOPWL);
		for(size_t i = 0; i<wMax.size(); i++)
		{
			if(constIndices.find(i) != constIndices.end())
			{
				cout<<"const index: "<<i<<endl;
				wMax[i] = CONSTWL;
			}
			else
				wMax[i] = MAXOPWL;
		}
		
		cout<<"getting MWC....\n";
		mwc = getMWC();
		for(size_t i = 0; i<mwc.size(); i++)
			cout<<mwc[i]<<" ";
		cout<<"\n";
		cout<<"greedy ascent\n";
			mwc = greedyAscent(mwc);
		cout<<"WLs after ascent..."<<endl;
		for(int i = 0; i<mwc.size();i++)
			cout<<mwc[i]<<" ";
		cout<<"\n";
		cout<<"SNR after algo 2: "<<calculateSNR(mwc)<<endl;	
//		cout<<"WLs after tabu search..."<<endl;
		cout<<"tabu\n";
		mwc = tabuSearch(mwc);
		for(int i = 0; i<mwc.size();i++)
			cout<<"("<<i<<","<<mwc[i]<<") ";
		cout<<"\n";
		//tr = true;
		cout<<"SNR after algo 3: "<<calculateSNR(mwc)<<endl;
/*		ofstream ty("fft.h");
		
		for(std::map<size_t,std::string> ::iterator b = weights.begin(), e = weights.end(); b!=e; ++b)
		{
			
			if(b->second == "1")
				ty << "ac_fixed<"<<40+mwc[b->first]<<",40,true> a"<<b->first<<";\n";
			else
				ty << "ac_fixed<"<<40+mwc[b->first]<<",40,true> m"<<b->first<<";\n";
		}
		
		ty.close();*/
		
/*	ofstream ty("types.txt");
	
	ty<<"ac_fixed<"<<20+mwc[63]<<",20,true> t;\n";
	ty<<"\n";

	ty<<"ac_fixed<"<<20+mwc[63]<<",20,true> iir(ac_fixed<"<<20+mwc[0]<<",20,true> x)\n";
	ty<<"{\n";

	ty<<"ac_fixed<"<<20+mwc[1]<<",20,true> m1;\n";
	ty<<"ac_fixed<"<<20+mwc[2]<<",20,true> m2;\n";
	ty<<"ac_fixed<"<<20+mwc[3]<<",20,true> m3;\n";
	ty<<"ac_fixed<"<<20+mwc[4]<<",20,true> m4;\n";
	ty<<"ac_fixed<"<<20+mwc[5]<<",20,true> m5;\n";
	ty<<"ac_fixed<"<<20+mwc[6]<<",20,true> m6;\n";
	ty<<"ac_fixed<"<<20+mwc[7]<<",20,true> m7;\n";
	ty<<"ac_fixed<"<<20+mwc[8]<<",20,true> m8;\n";
	ty<<"ac_fixed<"<<20+mwc[9]<<",20,true> m9;\n";
	ty<<"ac_fixed<"<<20+mwc[10]<<",20,true> m10;\n";
	ty<<"ac_fixed<"<<20+mwc[11]<<",20,true> m11;\n";
	ty<<"ac_fixed<"<<20+mwc[12]<<",20,true> m12;\n";
	ty<<"ac_fixed<"<<20+mwc[13]<<",20,true> m13;\n";
	ty<<"ac_fixed<"<<20+mwc[14]<<",20,true> m14;\n";
	ty<<"ac_fixed<"<<20+mwc[15]<<",20,true> m15;\n";
	ty<<"ac_fixed<"<<20+mwc[16]<<",20,true> m16;\n";
	ty<<"ac_fixed<"<<20+mwc[17]<<",20,true> m17;\n";
	ty<<"ac_fixed<"<<20+mwc[18]<<",20,true> m18;\n";
	ty<<"ac_fixed<"<<20+mwc[19]<<",20,true> m19;\n";
	ty<<"ac_fixed<"<<20+mwc[20]<<",20,true> m20;\n";
	ty<<"ac_fixed<"<<20+mwc[21]<<",20,true> m21;\n";
	ty<<"ac_fixed<"<<20+mwc[22]<<",20,true> m22;\n";
	ty<<"ac_fixed<"<<20+mwc[23]<<",20,true> m23;\n";
	ty<<"ac_fixed<"<<20+mwc[24]<<",20,true> m24;\n";
	ty<<"ac_fixed<"<<20+mwc[25]<<",20,true> m25;\n";
	ty<<"ac_fixed<"<<20+mwc[26]<<",20,true> m26;\n";
	ty<<"ac_fixed<"<<20+mwc[27]<<",20,true> m27;\n";
	ty<<"ac_fixed<"<<20+mwc[28]<<",20,true> m28;\n";
	ty<<"ac_fixed<"<<20+mwc[29]<<",20,true> m29;\n";
	ty<<"ac_fixed<"<<20+mwc[30]<<",20,true> m30;\n";
	ty<<"ac_fixed<"<<20+mwc[31]<<",20,true> m31;\n";
	ty<<"ac_fixed<"<<20+mwc[32]<<",20,true> m32;\n";
	ty<<"ac_fixed<"<<20+mwc[33]<<",20,true> a33;\n";
	ty<<"ac_fixed<"<<20+mwc[34]<<",20,true> a34;\n";
	ty<<"ac_fixed<"<<20+mwc[35]<<",20,true> a35;\n";
	ty<<"ac_fixed<"<<20+mwc[36]<<",20,true> a36;\n";
	ty<<"ac_fixed<"<<20+mwc[37]<<",20,true> a37;\n";
	ty<<"ac_fixed<"<<20+mwc[38]<<",20,true> a38;\n";
	ty<<"ac_fixed<"<<20+mwc[39]<<",20,true> a39;\n";
	ty<<"ac_fixed<"<<20+mwc[40]<<",20,true> a40;\n";
	ty<<"ac_fixed<"<<20+mwc[41]<<",20,true> a41;\n";
	ty<<"ac_fixed<"<<20+mwc[42]<<",20,true> a42;\n";
	ty<<"ac_fixed<"<<20+mwc[43]<<",20,true> a43;\n";
	ty<<"ac_fixed<"<<20+mwc[44]<<",20,true> a44;\n";
	ty<<"ac_fixed<"<<20+mwc[45]<<",20,true> a45;\n";
	ty<<"ac_fixed<"<<20+mwc[46]<<",20,true> a46;\n";
	ty<<"ac_fixed<"<<20+mwc[47]<<",20,true> a47;\n";
	ty<<"ac_fixed<"<<20+mwc[48]<<",20,true> a48;\n";
	ty<<"ac_fixed<"<<20+mwc[49]<<",20,true> a49;\n";
	ty<<"ac_fixed<"<<20+mwc[50]<<",20,true> a50;\n";
	ty<<"ac_fixed<"<<20+mwc[51]<<",20,true> a51;\n";
	ty<<"ac_fixed<"<<20+mwc[52]<<",20,true> a52;\n";
	ty<<"ac_fixed<"<<20+mwc[53]<<",20,true> a53;\n";
	ty<<"ac_fixed<"<<20+mwc[54]<<",20,true> a54;\n";
	ty<<"ac_fixed<"<<20+mwc[55]<<",20,true> a55;\n";
	ty<<"ac_fixed<"<<20+mwc[56]<<",20,true> a56;\n";
	ty<<"ac_fixed<"<<20+mwc[57]<<",20,true> a57;\n";
	ty<<"ac_fixed<"<<20+mwc[58]<<",20,true> a58;\n";
	ty<<"ac_fixed<"<<20+mwc[59]<<",20,true> a59;\n";
	ty<<"ac_fixed<"<<20+mwc[60]<<",20,true> a60;\n";
	ty<<"ac_fixed<"<<20+mwc[61]<<",20,true> a61;\n";
	ty<<"ac_fixed<"<<20+mwc[62]<<",20,true> a62;\n";
	ty<<"ac_fixed<"<<20+mwc[63]<<",20,true> a63;\n";
	ty<<"static_ac_fixed<"<<20+mwc[64]<<",20,true> r64=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[65]<<",20,true> r65=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[66]<<",20,true> r66=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[67]<<",20,true> r67=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[68]<<",20,true> r68=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[69]<<",20,true> r69=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[70]<<",20,true> r70=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[71]<<",20,true> r71=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[72]<<",20,true> r72=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[73]<<",20,true> r73=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[74]<<",20,true> r74=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[75]<<",20,true> r75=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[76]<<",20,true> r76=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[77]<<",20,true> r77=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[78]<<",20,true> r78=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[79]<<",20,true> r79=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[80]<<",20,true> r80=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[81]<<",20,true> r81=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[82]<<",20,true> r82=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[83]<<",20,true> r83=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[84]<<",20,true> r84=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[85]<<",20,true> r85=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[86]<<",20,true> r86=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[87]<<",20,true> r87=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[88]<<",20,true> r88=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[89]<<",20,true> r89=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[90]<<",20,true> r90=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[91]<<",20,true> r91=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[92]<<",20,true> r92=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[93]<<",20,true> r93=0;\n";
	ty<<"static_ac_fixed<"<<20+mwc[94]<<",20,true> r94=0;\n";

	ty<<"m1 = x*0.03125;\n\
m2 = r64*0.03125;\n\
m3 = r65*0.03125;\n\
m4 = r66*0.03125;\n\
m5 = r67*0.03125;\n\
m6 = r68*0.03125;\n\
m7 = r69*0.03125;\n\
m8 = r70*0.03125;\n\
m9 = r71*0.03125;\n\
m10 = r72*0.03125;\n\
m11 = r73*0.03125;\n\
m12 = r74*0.03125;\n\
m13 = r75*0.03125;\n\
m14 = r76*0.03125;\n\
m15 = r77*0.03125;\n\
m16 = r78*0.03125;\n\
m17 = r79*0.03125;\n\
m18 = r80*0.03125;\n\
m19 = r81*0.03125;\n\
m20 = r82*0.03125;\n\
m21 = r83*0.03125;\n\
m22 = r84*0.03125;\n\
m23 = r85*0.03125;\n\
m24 = r86*0.03125;\n\
m25 = r87*0.03125;\n\
m26 = r88*0.03125;\n\
m27 = r89*0.03125;\n\
m28 = r90*0.03125;\n\
m29 = r91*0.03125;\n\
m30 = r92*0.03125;\n\
m31 = r93*0.03125;\n\
m32 = r94*0.03125;\n\
a33 = m1+m2;\n\
a34 = m3+a33;\n\
a35 = m4+a34;\n\
a36 = m5+a35;\n\
a37 = m6+a36;\n\
a38 = m7+a37;\n\
a39 = m8+a38;\n\
a40 = m9+a39;\n\
a41 = m10+a40;\n\
a42 = m11+a41;\n\
a43 = m12+a42;\n\
a44 = m13+a43;\n\
a45 = m14+a44;\n\
a46 = m15+a45;\n\
a47 = m16+a46;\n\
a48 = m17+a47;\n\
a49 = m18+a48;\n\
a50 = m19+a49;\n\
a51 = m20+a50;\n\
a52 = m21+a51;\n\
a53 = m22+a52;\n\
a54 = m23+a53;\n\
a55 = m24+a54;\n\
a56 = m25+a55;\n\
a57 = m26+a56;\n\
a58 = m27+a57;\n\
a59 = m28+a58;\n\
a60 = m29+a59;\n\
a61 = m30+a60;\n\
a62 = m31+a61;\n\
a63 = m32+a62;\n\
r94 = r93;\n\
r93 = r92;\n\
r92 = r91;\n\
r91 = r90;\n\
r90 = r89;\n\
r89 = r88;\n\
r88 = r87;\n\
r87 = r86;\n\
r86 = r85;\n\
r85 = r84;\n\
r84 = r83;\n\
r83 = r82;\n\
r82 = r81;\n\
r81 = r80;\n\
r80 = r79;\n\
r79 = r78;\n\
r78 = r77;\n\
r77 = r76;\n\
r76 = r75;\n\
r75 = r74;\n\
r74 = r73;\n\
r73 = r72;\n\
r72 = r71;\n\
r71 = r70;\n\
r70 = r69;\n\
r69 = r68;\n\
r68 = r67;\n\
r67 = r66;\n\
r66 = r65;\n\
r65 = r64;\n\
r64 = x;\n\
\n\
return a63;\n\
}\n";
	
	ty.close();*/

/*	ofstream ty("types.txt");
	
	ty<<"ac_fixed<"<<20+mwc[3]<<",20,true> t;\n";
	ty<<"ac_fixed<"<<20+mwc[12]<<",20,true> A0 = 1.710329;\n";
	ty<<"ac_fixed<"<<20+mwc[14]<<",20,true> A1 = -0.747274;\n";
	ty<<"ac_fixed<"<<20+mwc[16]<<",20,true> B0 = 0.009236;\n";
	ty<<"ac_fixed<"<<20+mwc[13]<<",20,true> B1 = 0.018472;\n";
	ty<<"ac_fixed<"<<20+mwc[15]<<",20,true> B2 = 0.009236;\n";
	ty<<"\n";

	ty<<"ac_fixed<"<<20+mwc[3]<<",20,true> iir(ac_fixed<"<<20+mwc[11]<<",20,true> x)\n";
	ty<<"{\n";
	ty<<"\tac_fixed<"<<20+mwc[0]<<",20,true> a0;\n";
	ty<<"\tac_fixed<"<<20+mwc[1]<<",20,true> a1;\n";
	ty<<"\tac_fixed<"<<20+mwc[2]<<",20,true> a2;\n";
	ty<<"\tac_fixed<"<<20+mwc[3]<<",20,true> a3;\n";
	ty<<"\tac_fixed<"<<20+mwc[4]<<",20,true> m0;\n";
	ty<<"\tac_fixed<"<<20+mwc[5]<<",20,true> m1;\n";
	ty<<"\tac_fixed<"<<20+mwc[6]<<",20,true> m2;\n";
	ty<<"\tac_fixed<"<<20+mwc[7]<<",20,true> m3;\n";
	ty<<"\tac_fixed<"<<20+mwc[8]<<",20,true> m4;\n";
	ty<<"\n";
	
	ty<<"\tstatic ac_fixed<"<<20+mwc[9]<<",20,true> reg1=0;\n";
	ty<<"\tstatic ac_fixed<"<<20+mwc[10]<<",20,true> reg2=0;\n";
	
	ty.close();*/

		assert(0);		
		
//		cout<<"sim and snr\n";
		exec("g++ -std=c++11 -I/usr/include/python2.7 -L/usr/lib/python2.7/config/ -o py iir.cpp -lpython2.7 -ldl -lm -lutil -lz -pthread");
		std::string err = exec("./py snr computeError");
		std::stringstream ess;
		ess << err;
		ess >> fin;
//		cout<<fin<<endl;
		
		//minNoise = minNoise + (minNoise - fin);
		// or should it be:
//		cout<<"offset: "<<cpy - fin<<endl;
		 minNoise = cpy + (cpy - fin);
//		cout<<"percentage err: "<< std::setprecision(2) <<fabs((fin-cpy)/cpy)*100<<"\n";
//		assert(0);
//		cout<<"new noise: "<<minNoise<<endl;
		
	}while(fabs((fin-cpy)/cpy) > 0.2);

	cout<<"WLs after adaptive search..."<<endl;
	for(int i = 0; i<mwc.size();i++)
		cout<<"("<<i<<","<<mwc[i]<<") ";
	cout<<"\n";
	cout<<"-------- final error: "<< std::setprecision(2) <<fabs((fin-cpy)/cpy)*100<<"\n";
	//tr = true;
	

	
}

std::vector<std::set<size_t> > enumSCC()
{
	//cout<<"enumSCC\n";
	post2v.clear();
	v2post.clear();
	clk = 0;
	DFS();

//	for(std::map<size_t,size_t>::iterator h = v2post.begin(), g = v2post.end(); h!=g; ++h)
//		cout<<h->first<<" : "<<h->second<<endl;
//	assert(0);
	visited.clear();
	std::vector<std::set<size_t> > ret = processDecrPost();
	return ret;
}

void explore(size_t u)
{
	visited.insert(u);
	for(size_t v = 0; v < revadjList[u].size(); ++v)
	{
		if(work.find(revadjList[u][v]) == work.end()) continue;
		if(deletedVertices.find(revadjList[u][v]) == deletedVertices.end() && visited.find(revadjList[u][v]) == visited.end())
			explore(revadjList[u][v]);
	}
	v2post[u] = clk;
	post2v[clk++] = u;
}


void DFS()
{
	//cout<<"DFS\n";
	visited.clear();
	for(std::map<size_t,std::vector<size_t> >::iterator u = revadjList.begin(), e = revadjList.end();
		u != e; ++u)
		{
			if(work.find(u->first) == work.end()) continue;
			if(deletedVertices.find(u->first) == deletedVertices.end() && visited.find(u->first) == visited.end())
				explore(u->first);
		}
}

void findSCC(size_t u,std::set<size_t>& cc, bool del)
{
	visited.insert(u);
	cc.insert(u);
	//cout<<u<<" ";
	if(del)
	{
		size_t post = v2post[u];
		v2post.erase(u);
		post2v.erase(post);
	}
	


	for(size_t v = 0; v < adjList[u].size(); ++v)
	{
		if(del && work.find(adjList[u][v]) == work.end()) continue;
		if(deletedVertices.find(adjList[u][v]) == deletedVertices.end() && visited.find(adjList[u][v]) == visited.end())
		{
			findSCC(adjList[u][v],cc,del);
		}
	}

	
}

std::vector<std::set<size_t> > processDecrPost()
{
	//cout<<"processDecrPost\n";
	std::vector<std::set<size_t> > ccList;
	std::set<size_t> cc;
	//cout<<"components:\n";
	while(!post2v.empty())
	{
		//cout<<"src: "<<post2v.begin()->second<<endl;
		findSCC((post2v.begin()->second),cc);
		ccList.push_back(cc);
		//cout<<endl;
		cc.clear();
	}
	return ccList;
}

std::vector<size_t> st;
std::set<size_t> blockedSet;
std::map<size_t,size_t> blockedMap;


void johnson()
{
	//cout<<"johnson"<<endl;
	std::vector<std::set<size_t> >  ccList = enumSCC();
	std::stack<std::set<size_t> > ccStack;
	for(size_t i = 0; i<ccList.size(); i++)
	{
		ccStack.push(ccList[i]);
	}
	
	while(!ccStack.empty())
	{
		work = ccStack.top();
		ccStack.pop();
		
		visited.clear();
		st.clear();
		blockedSet.clear();
		blockedMap.clear();
		//cout<<"trigger ckt with: "<<*(work.begin())<<"\n";
		circuit(*(work.begin()),*(work.begin()));
		deletedVertices.insert(*(work.begin()));
		visited.clear();
		std::vector<std::set<size_t> > newCC = enumSCC();
		for(size_t i = 0; i<newCC.size(); i++)
		{
			ccStack.push(newCC[i]);
		}
		
	}
}

bool circuit(size_t u, size_t src)
{
	bool ret = false;
	
	if(u!=src)
		visited.insert(u);
	
	if(u == src && st.size() != 0)
	{
		//cout<<"************* cycle: *************\n";
		for(size_t k = 0; k < st.size(); k++)
		{
			//cout<<st[k]<<" ";
			loops[lctr].push_back(st[k]);
		}
		liveLoops[lctr] = true;
		lctr++;
		//cout<<u<<endl;
		//cout<<"**********************************\n";
		return true;		
	}
	
	st.push_back(u);
	//cout<<"stack push: "<<u<<"\n";
	if(blockedSet.find(u) == blockedSet.end())
	{
		blockedSet.insert(u);
		//cout<<"blockedSet push: "<<u<<"\n";
	}
	else 	// should not explore in that direction
	{
		st.pop_back();
		//cout<<"stack pop: "<<u<<"\n";
		visited.erase(u);
		return false;
	}
	for(size_t v = 0; v < adjList[u].size(); ++v)
	{
		size_t k = adjList[u][v];
		//cout<<"deciding edge: "<<u<<" -> "<<adjList[u][v]<<endl;
		if(work.find(adjList[u][v]) == work.end()) continue;

		if(deletedVertices.find(adjList[u][v]) == deletedVertices.end() && visited.find(adjList[u][v]) == visited.end())
		{
			bool f = circuit(adjList[u][v],src);
			//cout<<"exploring edge: "<<u<<" -> "<<adjList[u][v]<<endl;
			ret = ret || f; // becomes true iff atleast one cycle thru u is found
		}
	}
	
	// if all neighbours are explored and found a cycle means unblock
	if(ret)
	{
		unblock(u);
	}
	else
	{
		for(size_t v = 0; v < adjList[u].size(); ++v)
		{
			if(blockedSet.find(adjList[u][v]) != blockedSet.end() && adjList[u][v] != src)
			{
				//if(visited.find(adjList[u][v]) != visited.end()) continue;
				assert(blockedMap.find(adjList[u][v]) == blockedMap.end());
				blockedMap[adjList[u][v]] = u;
				//cout<<"BM: "<<adjList[u][v]<<" --> "<<u<<endl;
			}	
		}		
	}
	
	st.pop_back(); // delete from stack but not from blocked set
	//cout<<"stack pop: "<<u<<endl;
	if(u != src)
		visited.erase(u);
	return ret;
	
}

void unblock(size_t u)
{
	if(blockedMap.find(u) == blockedMap.end())
	{
		blockedSet.erase(u);
		//cout<<"erase from blockedSet: "<<u<<endl;
		return;
	}

//	if(blockedMap.find(blockedMap[u]) != blockedMap.end())
	unblock(blockedMap[u]);
	
//	if(blockedMap.find(u) != blockedMap.end())
//	{
		//cout<<"erase from blockedMap: "<<u<<" --> "<<blockedMap[u]<<endl;
		blockedMap.erase(u);
//	}
	blockedSet.erase(u);
	//cout<<"erase from blockedSet: "<<u<<endl;
}

void enumPaths(size_t src, size_t dst)
{
	for(std::map<size_t,std::vector<size_t> >::iterator u = adjList.begin(), e = adjList.end();
		u != e; ++u)
		{
			visitedNodes[u->first] = false;
		}
	
	pathDFS(src,dst);

}

void pathDFS(size_t& src, size_t& dst)
{
	//cout<<"vis: "<<src<<" dst: "<<dst<<endl;
	if(src == dst)
	{
		printPathStack(dst);
		return;
	}
	
	pathStack.push_back(src);
	visitedNodes[src] = true;

	for(size_t v = 0; v < adjList[src].size(); ++v)
	{
		if(visitedNodes[adjList[src][v]]) continue;
		
		pathDFS(adjList[src][v], dst);
	}
	
	pathStack.pop_back();
	visitedNodes[src] = false;
}

void printPathStack(size_t dst)
{
	for(size_t i = 0; i<pathStack.size(); i++)
	{
//		cout<<pathStack[i]<<" ";
		paths[pctr].push_back(pathStack[i]);
	}
	paths[pctr].push_back(dst);
	pctr++;
//	cout<<dst<<"\n";
}

void sortPaths()
{
	for(std::map<size_t,std::vector<size_t> >::iterator b = paths.begin(), e = paths.end(); b!=e; ++b)
	{
		std::sort((b->second).begin(),(b->second).end());
	}
}

void sortLoops()
{
	for(std::map<size_t,std::vector<size_t> >::iterator b = loops.begin(), e = loops.end(); b!=e; ++b)
		std::sort((b->second).begin(),(b->second).end());	
}

// primary function for checking disjointness of 2 sorted vectors
// true if not disjoint
bool merge(std::vector<size_t>& a, std::vector<size_t>& b, size_t beg1, size_t beg2)
{
	if(beg1 == a.size() || beg2 == b.size()) // all elements of an array got expired
		return false;
	if(a[beg1] == b[beg2])
		return true;
	if(a[beg1] < b[beg2])
		return merge(a,b,++beg1,beg2);
	else
		return merge(a,b,beg1,++beg2);	
}

std::string getNetWeight(std::vector<size_t> nodes)
{
	std::string netWt = "";
	for(size_t i = 0; i<nodes.size(); i++)
	{
		netWt+="("+weights[nodes[i]]+")*";
	}
	netWt.pop_back();
	return netWt;
}

// need to be called for every edge modification
void buildpathsNloops()
{
	for(std::map<size_t,std::vector<size_t> >::iterator p = paths.begin(), pe = paths.end(); p!=pe; ++p)
	{
		std::vector<bool> temp;
		for(std::map<size_t,std::vector<size_t> >::iterator l = loops.begin(), le = loops.end(); l!=le; ++l)
		{
			if(!liveLoops[l->first]) temp.push_back(false);
			else
				temp.push_back(merge(p->second, l->second));
		}
		pathsNloops.push_back(temp);
		std::string h = getNetWeight(p->second);
		pathWeights.push_back(h);
	}
}

// need to be called only once
void buildloopsNloops()
{
	for(std::map<size_t,std::vector<size_t> >::iterator p = loops.begin(), pe = loops.end(); p!=pe; ++p)
	{
		std::vector<bool> temp;
		for(std::map<size_t,std::vector<size_t> >::iterator l = loops.begin(), le = loops.end(); l!=le; ++l)
		{
			temp.push_back(merge(p->second, l->second));			
		}
		loopsNloops.push_back(temp);
		loopWeights.push_back(getNetWeight(p->second));
	}
}

std::string getProductTerms(size_t n, size_t r, int pathindex)
{
    std::vector<bool> v(n);
    std::string denom = "";
    std::fill(v.begin() + n - r, v.end(), true);
    do {
		size_t numTerms = 0;
		std::vector<size_t> temp;
		std::string netWt = "";
		bool stat = true;
        for (size_t i = 0; i < n; ++i)
        {
            if (v[i] && liveLoops[i])  // only need to consider live loops
            {
                for(size_t j = 0; j<temp.size(); j++)
                {
					//cout<<"considering loops: "<<i<<" and "<<temp[j]<<endl;
					if(loopsNloops[i][temp[j]])
					{
						stat = false;
						//cout<<"intersects"<<endl;
						break;
					}
				}
				if(!stat) break;
				if(pathindex!=-1 && pathsNloops[pathindex][i]) break; // if current loop intersects with given path
				else
				{
					temp.push_back(i);
					netWt+=loopWeights[i]+"*";
					numTerms++;
					//cout<<"lw: "<<loopWeights[i]<<endl;
				}
                //std::cout << (i+1) << " ";
            }
        }
        //cout<<"num Terms: "<<numTerms<<endl;
        if(numTerms >= r && netWt.size() != 0)
		{
			netWt.pop_back();
			denom+=netWt+"+";
		}
		
        //std::cout << "\n";
    } while (std::next_permutation(v.begin(), v.end()));
    
    if(denom.size() != 0)
		denom.pop_back();
	//cout<<"returning: "<<denom<<endl;
	return denom;
}

void buildDenominator()
{
	std::string dr = "1";
	size_t ctr = 0;
	while(ctr < loops.size())
	{		
		ctr++;
		std::string p = getProductTerms(loops.size(),ctr);
		if(p.size() && ctr%2)
			dr+="-("+p+")";
		else if(p.size() && !(ctr%2))
			dr+="+("+p+")";
	}
//	cout<<"denominator: "<<dr<<endl;
	Out<<"("<<dr<<")\n";
}

void buildNumerator()
{
	if(paths.size() == 0)
	{
		Out<<"(0)/";
		return;
	}
	
	std::string nr = "";
	for(size_t pathindex = 0; pathindex < paths.size(); ++pathindex)
	{
		nr+=pathWeights[pathindex]+"*(1";
		size_t ctr = 0;
		while(ctr < loops.size())
		{		
			ctr++;
			std::string p = getProductTerms(loops.size(),ctr,pathindex);
			if(p.size() && ctr%2)
				nr+="-("+p+")";
			else if(p.size() && !(ctr%2))
				nr+="+("+p+")";
			
		}
		nr+=")+";
	}
	nr.pop_back();
//	cout<<"numerator: "<<nr<<endl;
	Out<<"("<<nr<<")/";
}

void gBreak(size_t node)
{
	adjList_cpy = adjList;
	revadjList_cpy = revadjList;
	std::vector<size_t> outV = adjList[node];
	adjList[node].clear(); // setting out degree to zero
	adjList[node].push_back(numNodes);
	adjList[numNodes] = outV;
	
	// resetting globals for future paths exploration
	paths.clear();
	pathsNloops.clear();
	pathWeights.clear();
	pctr = 0;
	
/*	for(size_t i = 0; i<adjList.size(); i++)
	{
		cout<<"node "<<i<<": ";
		for(size_t j = 0; j<adjList[i].size(); j++)
			cout<<adjList[i][j]<<" ";
		cout<<"\n";
	}*/
	
/*	for(size_t u = 0; u<outV.size(); u++) 	
	{
		size_t v;
		for(v = 0; v<revadjList[outV[u]].size(); v++)
		{
			if(revadjList[outV[u]][v] == node) break;
		}
		revadjList[outV[u]].erase(revadjList[outV[u]].begin()+v);
		revadjList[outV[u]].push_back(numNodes);
	}
	std::vector<size_t> nullVector;
	revadjList[numNodes] = nullVector;*/
}

void gWane()
{
	adjList = adjList_cpy;
	revadjList = revadjList_cpy;
}

// should be called only after sortLoops
void selectLiveLoops(size_t removedNode)
{
	
	for(size_t i = 0; i<loops.size(); i++)
	{
		if(std::binary_search(loops[i].begin(), loops[i].end(),removedNode))
		{
			liveLoops[i] = false;
			toReset.insert(i);
		}
		else
			liveLoops[i] = true;
	}
	//cout<<"rem node: "<<removedNode<<endl;
	killNonReachableLoops(numNodes);
}

// checks reachability of loops from given node
void killNonReachableLoops(size_t node)
{
	std::set<size_t> cc;
	std::vector<size_t> temp;
	deletedVertices.clear();
	visited.clear();
	findSCC(node,cc,false);
	//cout<<"temp: "<<endl;
	for(std::set<size_t>::iterator b = cc.begin(), e = cc.end(); b!=e; ++b)
	{
		//cout<<*b<<" ";
		temp.push_back(*b);
	}
	//cout<<endl;
	for(size_t i = 0; i<loops.size(); i++)
	{
		//cout<<"curr loop: ";
		//for(size_t j = 0; j<loops[i].size(); j++)
			//cout<<loops[i][j]<<" ";
		//cout<<endl;
		if(!merge(temp,loops[i]))
		{
			//cout<<"not intersects"<<endl;
			assert(toReset.find(i) == toReset.end());
			liveLoops[i] = false;
		}
		//else
			//cout<<"intersects"<<endl;
	}
}

// takes a register index and fills the correct driver
void fillRegDrivers(size_t i)
{
	size_t i_cpy = i;
	
	
	while(weights[revadjList[i][0]] == "z")
	{
		i = revadjList[i][0];
	}
	regDrivers[revadjList[i][0]].push_back(i_cpy);
}

void createExtensions()
{
	depList.resize(hMean[0].size()+numMults);
	bool done = false;
	
	for(size_t i = 0; i<hMean.size(); i++)		// size of hMean = # of POs
	{
		std::vector<double> m,v;
		m.resize(hMean[i].size()+numMults);
		v.resize(hMean[i].size()+numMults);
		size_t visitedMuls = 0;		
		
		for(std::map<size_t,std::vector<size_t> >::iterator op = revadjList.begin(), e = revadjList.end(); op!=e; ++op)
		{
			m[op->first] = hMean[i][op->first];
			v[op->first] = hVar[i][op->first];
			if(weights[op->first] != "z" && weights[op->first] != "1")
			{
				//cout<<"adding "<<weights[op->first]<<"to "<<hMean[i].size()+visitedMuls<<endl;
				m[hMean[i].size()+visitedMuls] = hMean[i][op->first];
				v[hMean[i].size()+visitedMuls] = hVar[i][op->first];
				if(!done)
				{
					//cout<<"% "<<hMean[i].size()+visitedMuls<<"\n";
					constIndices.insert(hMean[i].size()+visitedMuls);
					depList[op->first].push_back(hMean[i].size()+visitedMuls);
				}
				visitedMuls++;
			}
			
				
			if(!done)
			{
				for(size_t i = 0; i<(op->second).size(); i++)
					depList[op->first].push_back((op->second)[i]);
			}
		}
		done = true;
		xtendedMean.push_back(m);
		xtendedVar.push_back(v);
	}
	// TODO: initial case of op->first == 0 can be peeled out
	for(size_t i = 0; i<depList.size(); i++)
	{
		cout<<i<<" : ";
		for(size_t j = 0; j<depList[i].size(); j++)
			cout<<depList[i][j]<<" ";
		cout<<endl;
	}
}

double calculateSNR(std::vector<int> WL)
{
/*	string lk = "";
	for(int i = 0; i<WL.size(); i++)
		lk+=std::to_string(WL[i]);

	if(cacheMap.find(lk) != cacheMap.end())
	{
		cout<<"H\n";
		return cacheMap[lk];
	}*/
	
	
	double noise = -99999999999999;
	double s;
	for(size_t t = 0; t<hMean.size(); t++)
	{
		double meanSum = 0, varSum = 0;
		
		for(size_t i = 0; i<WL.size(); i++)	
		{
			//cout<<"cur index: "<<i<<endl;
			if(i<numNodes && weights[i] == "z") 
			{
				if(tr) cout<<"skip "<<i<<" z"<<endl;
				continue; // skip registers
			}
			if(constIndices.find(i) != constIndices.end())
			{
				if(tr) cout<<"skip "<<i<<" c"<<endl;
				continue;
			}
			std::pair<double,double> stat = getOperatorStats(i,WL);
	//		cout<<"index: "<<i<<" mean: "<<stat.first<<" std: "<<stat.second<<endl;
			meanSum+=(xtendedMean[t][i]*stat.first);
			varSum+=(xtendedVar[t][i]*stat.second);
	//		cout<<"multiplied var: "<<xtendedVar[i]<<"\n";
		}
		
		s = 10*log10(varSum+(meanSum*meanSum));
		noise = (s > noise) ? s : noise;					// finds max noise in MIMO systems
		
	}
	//cout<<"non log: "<<varSum+(meanSum*meanSum)<<endl;
	//cout<<"noise power = "<<10*log10(varSum+(meanSum*meanSum))<<endl;
	//return 10*log10(sigPower/(varSum+(meanSum*meanSum)));
	//cacheMap[lk] = noise;
	//cout<<"M\n";
	return noise;
}

std::pair<double,double> getOperatorStats(size_t i, std::vector<int> WL) // BEWARE: i is index in WL
{
	
	double q = fastPower2[WL[i]];
	if(depList[i].size() == 0)	// natural quantizers
	{
//		cout<<"natural quantizer\n";
		return std::make_pair(0.5*q,(1.0/12)*q*q);
	}
	
	// after this point i<numNodes always
	assert(i<numNodes);
	unsigned long long unity = 1;
	size_t trueWL = 0;
	int k = 0, l =0;
	std::pair<double,double> mean_var;
	size_t constWL;
//	cout<<"inps: ";
	if(weights[i] != "1") // FIXME: while legup integration use operator types to detect mul/add
	{
		for(size_t t = 0; t<depList[i].size(); t++)
		{
//			cout<<WL[depList[i][t]]<<" ";
//			if(depList[depList[i][t]].size() == 0)
//			{
				//constWL = WL[depList[i][t]];
//				constIndices.insert(depList[i][t]);
//			}
			trueWL+=WL[depList[i][t]];
		}
	}
	else
	{
		for(size_t t = 0; t<depList[i].size(); t++)
		{
//			cout<<WL[depList[i][t]]<<" ";
			trueWL = (trueWL < WL[depList[i][t]])? WL[depList[i][t]] : trueWL;
		}
	}
//	cout<<"\n";
	
	if(trueWL <= WL[i])				// no quantization noise source added in this case
	{
		//cout<<"no error case\n";
		return std::make_pair(0,0);
	}
	else
	{
		k = trueWL - WL[i];
		//cout<<"k = "<<k<<"\n";
		if(weights[i] != "1") 
		{
			if(tr) cout<<"index: "<<i<<" mul: "<<multConstants[i]<<"\n";
			l = getNumTrailingZeroes(multConstants[i],CONSTWL);
		}
		mean_var = getStats(q,k,l);
		return mean_var;
	}
}

std::pair<double,double> getStats(double q, int k, int l)
{
	if(k < l) l = 0;
	double m = 0.5*q*(1-fastPower2[k-l]);
	double v = (1.0/12)*q*q*(1-(fastPower2[(k-l)]*fastPower2[(k-l)]));
	//cout<<"mean: "<<m<<" var: "<<v<<endl;
	return std::make_pair(m,v);
}

size_t getNumTrailingZeroes(float x, size_t f)
{
	float y = x * pow(2,f);
	double fractpart, intpart;
	fractpart = modf(y , &intpart);
	//cout<<"y: "<<y<<" int: "<<intpart<<" fract: "<<fractpart<<"\n";
	if(y<0 && fabs(fractpart)>0) intpart--;
	unsigned long long ip = fabs(intpart);
	size_t ctr = 0;
	if(ip == 0) return 0;
	while(ip%2 == 0)
	{
		ctr++;
		ip/=2;
	}
	if(tr)cout<<"**** trail for x= "<<x<<" is "<<ctr<<"\n";
	return ctr;
}

// maintains regwidths same as their drivers
void houseKeepRegWidths(std::vector<int>& WM, size_t j, int mode)
{
	assert(mode != -1);
	if(regDrivers.find(j) != regDrivers.end())
	{
		for(size_t i = 0; i<regDrivers[j].size(); i++)
		{
			//assert(WM[j] == WM[regDrivers[j][i]]);
			if(mode == 0)					// decr
				WM[regDrivers[j][i]]--;		
			else if(mode == 1)				// incr
				WM[regDrivers[j][i]]++;
			else 							// assignment
				WM[regDrivers[j][i]] = WM[j];	
		}
	}
	
}

std::vector<int> getMWC()
{
	std::vector<int> WMC;
	std::vector<int> WM = wMax;
	WMC.resize(wMax.size());
	double uu = calculateSNR(WM);
	//cout<<uu<<endl;
	//assert(0);
	for(int j=0;j<WM.size(); j++)
	{
		if(j<numNodes && weights[j] == "z") continue;	// skip registers
		if(constIndices.find(j) != constIndices.end())
		{
			WMC[j] = CONSTWL;
			continue;
		}
		cout<<"******************************** j = "<<j<<endl;
		while(WM[j] > 0 && calculateSNR(WM) < minNoise )
		//while(WM[j] > 0 && calculateSNR(WM) > minSNR )
		{
			houseKeepRegWidths(WM,j,0);
			WM[j]--;
			//cout<<"\t\tl = "<<WM[j]<<endl;
		}
		if(WM[j] != MAXOPWL)
		{
			++WM[j];
			WMC[j] = WM[j];
			houseKeepRegWidths(WMC,j,2);
			//cout<<"a j: "<<j<<" , "<<WMC[9]<<endl;
		}
		else
		{
			WMC[j] = MAXOPWL;
			houseKeepRegWidths(WMC,j,2);
			//cout<<"b j: "<<j<<" , "<<WMC[9]<<endl;
		}
		WM[j] = MAXOPWL;
		houseKeepRegWidths(WM,j,2);
		//cout<<"c j: "<<j<<" , "<<WMC[9]<<endl;
	}
	return WMC;
}

std::vector<int> greedyAscent(std::vector<int> WL)
{
	std::set<int> prohibitedIndices;
	
	double currSNR = calculateSNR(WL);
	while(currSNR > minNoise)
	//while(currSNR < minSNR)
	{
		//cout<<"currSNR: "<<currSNR<<" "<<prohibitedIndices.size()<<"\n";
		std::vector<double> gradient;
		gradient.resize(WL.size());
		
		double snr = currSNR;
		double c = getCost(WL);
		
		for(int i = 0; i < WL.size(); i++)
		{
			if(weights[i] == "z") continue;
			if(constIndices.find(i) != constIndices.end()) continue;
			if(prohibitedIndices.find(i) != prohibitedIndices.end())
			{
				gradient[i] = -999999998;
				continue;
			}
			std::vector<int> WL_delta = WL;
			WL_delta[i]++;
			houseKeepRegWidths(WL_delta,i,1);
			//double g = (calculateSNR(WL_delta)-calculateSNR(WL))/(getCost(WL_delta) -  getCost(WL));
			double c_delta = getCost(WL_delta);
			//double c = getCost(WL);
			double snr_delta = calculateSNR(WL_delta);
			//double snr = calculateSNR(WL);
			double g;
			if(c_delta ==  c)
			{
				if(snr_delta < snr)	g = 999999998;
				else 	g = -999999998;
			}
			else
			{
				//g = (calculateSNR(WL)-calculateSNR(WL_delta))/(getCost(WL_delta) -  getCost(WL));
				g = (snr-snr_delta)/(c_delta-c);
			}
			gradient[i] = g;
		}
		int index = findMaxIndex(gradient,prohibitedIndices);
		if(index == -1) break;
		WL[index] = WL[index] + 1;
		//cout<<"increasing index: "<<index<<" to "<<WL[index]<<"\n";
		houseKeepRegWidths(WL,index,2);
		if(WL[index] == MAXOPWL) prohibitedIndices.insert(index);
		currSNR = calculateSNR(WL);
	}
	return WL;
}

int findMaxIndex(std::vector<double> arr, std::set<int> prohibitedIndices)
{
	double y = -999999999;
	int index = -1;
	for(int i = 0; i<arr.size(); i++)
	{
		if(i<numNodes && weights[i] == "z") continue;
		if(constIndices.find(i) != constIndices.end()) continue;
		if(y < arr[i] && prohibitedIndices.find(i) == prohibitedIndices.end())
		{
			y = arr[i];
			index = i;
		}
	}
	return index;
}

double getCost(std::vector<int> WL)
{

	double sum = 0;
	for(size_t i = 0; i<adjList.size(); i++)
	{
		if(depList[i].size() == 0) continue;
		
		if(weights[i] == "z") sum+=WL[i];
		else if(weights[i] == "1")
		{
			if(depList[i].size() == 2)
				sum+=WL[i]+WL[depList[i][0]]+WL[depList[i][1]];
			else
				sum+=WL[i]+WL[depList[i][0]];
		}
		else
		{
			sum+=WL[i]+(WL[depList[i][0]]*WL[depList[i][1]]);
		}
	}
	return sum;

/*	return 
	WL[11]+WL[4]+
	WL[0]+WL[6]+
	WL[5]+WL[8]+
	WL[2]+WL[7]+
	WL[12]*WL[9]+
	WL[13]*WL[9]+
	WL[14]*WL[10]+
	WL[15]*WL[10]+
	WL[16]*WL[1]+
	WL[9]+
	WL[10];*/
	
	
/*	int sum = 0;
	for(int i = 0; i<17; i++)
		sum+=WL[i];
	return sum;*/

	// iir 2
/*	return 
	WL[1]+WL[0]+WL[5]+
	WL[2]+WL[1]+WL[7]+
	WL[3]+WL[6]+WL[9]+
	WL[4]+WL[3]+WL[8]+
	WL[5]+WL[12]*WL[10]+
	WL[6]+WL[13]*WL[10]+
	WL[7]+WL[14]*WL[11]+
	WL[8]+WL[15]*WL[11]+
	WL[9]+WL[16]*WL[2]+
	WL[10]+
	WL[11];*/
	
	
	// iir 4
/*	return
	WL[0]+WL[22]+WL[4]+
	WL[1]+WL[0]+WL[6]+
	WL[2]+WL[5]+WL[8]+
	WL[3]+WL[2]+WL[7]+
	WL[4]+WL[23]*WL[9]+
	WL[5]+WL[24]*WL[9]+
	WL[6]+WL[25]*WL[10]+
	WL[7]+WL[26]*WL[10]+
	WL[8]+WL[27]*WL[1]+
	WL[11]+WL[3]+WL[15]+
	WL[12]+WL[11]+WL[17]+
	WL[13]+WL[16]+WL[19]+
	WL[14]+WL[13]+WL[18]+
	WL[15]+WL[28]*WL[20]+
	WL[16]+WL[29]*WL[20]+
	WL[17]+WL[30]*WL[21]+
	WL[18]+WL[31]*WL[21]+
	WL[19]+WL[32]*WL[12]+
	WL[9]+
	WL[10]+
	WL[20]+
	WL[21];*/
	
	// fir
	/*return
	WL[2]+WL[0]+WL[3]+
	WL[6]+WL[2]+WL[5]+
	WL[3]+WL[7]*WL[1]+
	WL[5]+WL[8]*WL[4]+
	WL[1]+
	WL[4];*/
	
	// moving average
	/*return
	WL[1]+WL[0]*CONSTWL+
	WL[2]+WL[0]*CONSTWL+
	WL[3]+WL[0]*CONSTWL+
	WL[4]+WL[0]*CONSTWL+
	WL[5]+WL[0]*CONSTWL+
	WL[6]+WL[0]*CONSTWL+
	WL[7]+WL[0]*CONSTWL+
	WL[8]+WL[0]*CONSTWL+
	WL[9]+WL[1]+WL[2]+
	WL[10]+WL[9]+WL[3]+
	WL[11]+WL[10]+WL[4]+
	WL[12]+WL[11]+WL[5]+
	WL[13]+WL[12]+WL[6]+
	WL[14]+WL[13]+WL[7]+
	WL[15]+WL[14]+WL[8]+
	7*WL[0];*/
}

std::vector<int> tabuSearch(std::vector<int> WL)
{
	int iter = 1;
	double Copt = getCost(WL);
	std::vector<int> Wopt,Wmin;
	Wmin = WL;
	Wopt = WL;
	
	std::vector<int> T; // contains indices of Tabu operators
	std::map<int,bool> Tabu;
	for(int i =0; i<WL.size(); i++)
	{
		if(i < numNodes && weights[i] == "z")
			Tabu[i] = true;
		else if(constIndices.find(i) != constIndices.end())
		{
			Tabu[i] = true;
			T.push_back(i);
		}
		else
			Tabu[i] = false;
	}
		
	int d = (calculateSNR(WL) < minNoise) ? -1: 1;
	//int d = (calculateSNR(WL) > minSNR) ? -1: 1;
	
	//assert(d == -1);
	
	// registers are already Tabooed
	for(std::map<size_t,std::vector<size_t> >::iterator b = regDrivers.begin(), e = regDrivers.end(); b!=e; ++b)
	{
		for(int i = 0; i < (b->second).size(); i++)
			T.push_back((b->second)[i]);
	}
	
	size_t numRegs = T.size();
	
	int y = 0;
	while(T.size() < WL.size())
	{
		//cout<<"Tabu size: "<<T.size()<<"/"<<WL.size()<<endl;
		y++; 
		int ctr = -1;
		std::vector<double> gradient(WL.size(),-999999999); 
		double c = getCost(WL);
		double snr = calculateSNR(WL);
		for(std::map<int,bool> :: iterator op = Tabu.begin(), ope = Tabu.end(); op!=ope; op++)
		{
			
			ctr++;
			if(op->second) continue;
			
			//cout<<"iteration: "<<y<<" "<<ctr<<" "<<WL[op->first]<<endl;
			std::vector<int> WL_delta = WL;
			int index = op->first;
			// now we have to deal with non tabu operators
			if(d>0 && WL[index] < MAXOPWL)
			{
				WL_delta[index]++;
				houseKeepRegWidths(WL_delta,index,1);
			}
			else if(d<0 && WL[index] > MINOPWL)
			{
				WL_delta[index]--;
				houseKeepRegWidths(WL_delta,index,0);
			}
			else
			{
				T.push_back(index);
				Tabu[op->first] = true;
			}
			
			if(op->second) continue;
			
			// now we have to compute gradient
			double c_delta = getCost(WL_delta);
			//double c = getCost(WL);
			double snr_delta = calculateSNR(WL_delta);
			//double snr = calculateSNR(WL);
			double g;
			if(c_delta ==  c)
			{
				if(snr_delta < snr)	g = 999999998;
				else 	g = -999999998;
			}
			else
			{
				//g = (calculateSNR(WL)-calculateSNR(WL_delta))/(getCost(WL_delta) -  getCost(WL));
				g = (snr-snr_delta)/(c_delta-c);
			}
			//double g = (calculateSNR(WL_delta)-calculateSNR(WL))/(getCost(WL_delta) -  getCost(WL));
			//cout<<"# "<<((calculateSNR(WL)-calculateSNR(WL_delta)) > 0)<<" "<<((getCost(WL_delta) -  getCost(WL))>0)<<" "<<g<<"\n";
			gradient[ctr] = g;
			if(calculateSNR(WL_delta) < minNoise)
			//if(calculateSNR(WL_delta) > minSNR)
			{
				//cout<<"************* NOT VIOLATED by  ";
				//for(int m = 0; m<WL_delta.size(); m++)
					//cout<<WL_delta[m]<<" ";
				//cout<<"\t";
				if(getCost(WL_delta) < getCost(Wopt))
				{
					Wopt = WL_delta;
					Copt = getCost(WL_delta);
					//cout<<"CHANGED";
				}
				//cout<<"\n";
			}
		}
		if(T.size() == WL.size()) break;
		
		if(d > 0)
		{
			int index = findMaxIndex(gradient);
			assert(index != -1);
			WL[index]++;
			//cout<<"increase index: "<<index<<" to "<<WL[index]<<endl;
			houseKeepRegWidths(WL,index,1);
			if(calculateSNR(WL) < minNoise)
			//if(calculateSNR(WL) > minSNR)
			{
				d = -1; // change of direction as min is now fullfilled
				T.push_back(index);
				Tabu[index] = true;
				//cout<<"tabooed index: "<<index<<"\n";
			}
		}
		else
		{
			int index = findMinIndex(gradient);
			assert(index != -1);
			WL[index]--;
			//cout<<"decrease index: "<<index<<" to "<<WL[index]<<endl;
			houseKeepRegWidths(WL,index,0);
			if(calculateSNR(WL) > minNoise)
			//if(calculateSNR(WL) < minSNR)
			{
				//cout<<"violated at: ";
				//for(int t = 0; t<WL.size(); t++)
					//cout<<WL[t]<<" ";
				//cout<<" changing to ascent\n";
				d = 1;
			}
		}

	}
	return Wopt;
}

int findMaxIndex(std::vector<double> arr)
{
	//cout<<"size: "<<arr.size()<<endl;
	double y = -999999999;
	int index = -1;
	for(int i = 0; i<arr.size(); i++)
	{
		if(i < numNodes && weights[i] == "z") continue;
		if(constIndices.find(i) != constIndices.end()) continue;
		if(y < arr[i])
		{
			y = arr[i];
			index = i;
		}
	}
	return index;
}

int findMinIndex(std::vector<double> arr)
{
	//cout<<"size: "<<arr.size()<<endl;
	double y = 999999999;
	int index = -1;
	for(int i = 0; i<arr.size(); i++)
	{
		if(i < numNodes && weights[i] == "z") continue;
		if(constIndices.find(i) != constIndices.end()) continue;
		if(arr[i] != -999999999 && y > arr[i])
		{
			y = arr[i];
			index = i;
		}
	}
	return index;
}
