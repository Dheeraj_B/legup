// call_function.c - A sample of calling 
// python functions from C code
// 
#include <Python.h>
#include<iostream>

using namespace std;
int main(int argc, char *argv[])
{
    PyObject *pName, *pModule, *pDict, *pFunc, *pValue;

    if (argc < 3) 
    {
        printf("Usage: exe_name python_source function_name\n");
        return 1;
    }
	
	
    // Initialize the Python Interpreter
    Py_Initialize();
	PySys_SetArgv(argc, argv);
    // Build the name object
    pName = PyString_FromString(argv[1]);
    // Load the module object
    pModule = PyImport_Import(pName);
    if(pModule == NULL)    
    {
		PyErr_Print();
		return 1;
	}
    // pDict is a borrowed reference 
	pDict = PyModule_GetDict(pModule);
    // pFunc is also a borrowed reference 
    pFunc = PyDict_GetItemString(pDict, argv[2]);

    if (PyCallable_Check(pFunc)) 
    {
        PyObject_CallObject(pFunc, NULL);
    } else 
    {
        PyErr_Print();
    }
    // Clean up
    Py_DECREF(pModule);
    Py_DECREF(pName);

    // Finish the Python Interpreter
    Py_Finalize();

    return 0;
}
