#include<iostream>
#include<set>
#include<map>
#include<stack>
#include<vector>
#include<cassert>
#include<stack>
#include<string>

using namespace std;

std::map<size_t,std::vector<size_t> > adjList;
std::map<size_t,std::vector<size_t> > revadjList;
std::vector<size_t> pathStack;
std::map<size_t,bool> visitedNodes;
std::map<size_t,string> weights;

void enumPaths(size_t src, size_t dst);
void pathDFS(size_t& src, size_t& dst);
void printPathStack(size_t dst);

int main()
{
	size_t N, cpyN, u,v, degree,src,dst;
	cin>>N;
	while(N>0)
	{
		N--;
		cin>>u;
		cin>>degree;
		while(degree>0)
		{
			degree--;
			cin>>v;
			adjList[u].push_back(v);
			revadjList[v].push_back(u);
		}
		std::string wt;
		cin>>wt;
		weights[u] = wt;
	}
	cin>>src>>dst;
	cout<<"***********\n";
	enumPaths(src,dst);
	
}

void enumPaths(size_t src, size_t dst)
{
	for(std::map<size_t,std::vector<size_t> >::iterator u = adjList.begin(), e = adjList.end();
		u != e; ++u)
		{
			visitedNodes[u->first] = false;
		}
	
	pathDFS(src,dst);
}

void pathDFS(size_t& src, size_t& dst)
{
	if(src == dst)
	{
		printPathStack(dst);
		return;
	}
	
	pathStack.push_back(src);
	visitedNodes[src] = true;

	for(size_t v = 0; v < adjList[src].size(); ++v)
	{
		if(visitedNodes[adjList[src][v]]) continue;
		
		pathDFS(adjList[src][v], dst);
	}
	
	pathStack.pop_back();
	visitedNodes[src] = false;
}

void printPathStack(size_t dst)
{
	for(size_t i = 0; i<pathStack.size(); i++)
	{
		cout<<pathStack[i]<<" ";
	}
	cout<<dst<<"\n";
}
