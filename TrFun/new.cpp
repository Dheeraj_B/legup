#include<iostream>
#include<set>
#include<map>
#include<stack>
#include<vector>
#include<cassert>
#include<stack>
#include<string>

using namespace std;

size_t clk = 0;
std::map<size_t,std::vector<size_t> > adjList;
std::map<size_t,std::vector<size_t> > revadjList;
std::set<size_t> visited;
std::map<size_t,size_t,std::greater<size_t> >post2v;
std::map<size_t,size_t> v2post;
std::set<size_t> work;
std::set<size_t> deletedVertices;
std::map<size_t,string> weights;

std::vector<std::set<size_t> > enumSCC();
void explore(size_t u);
void DFS();
void findSCC(size_t u,std::set<size_t>& cc);
std::vector<std::set<size_t> > processDecrPost();
void johnson();
bool circuit(size_t u, size_t src);
void unblock(size_t u);

int main()
{
	size_t N, cpyN, u,v, degree;
	cin>>N;
	while(N>0)
	{
		N--;
		cin>>u;
		work.insert(u);
		cin>>degree;
		while(degree>0)
		{
			degree--;
			cin>>v;
			adjList[u].push_back(v);
			revadjList[v].push_back(u);
		}
		std::string wt;
		cin>>wt;
		weights[u] = wt;
	}
	johnson();
}

std::vector<std::set<size_t> > enumSCC()
{
	//cout<<"enumSCC\n";
	post2v.clear();
	v2post.clear();
	clk = 0;
	DFS();
//	for(std::map<size_t,size_t>::iterator h = v2post.begin(), g = v2post.end(); h!=g; ++h)
//		cout<<h->first<<" : "<<h->second<<endl;
//	assert(0);
	visited.clear();
	return processDecrPost();
}

void explore(size_t u)
{
	visited.insert(u);
	for(size_t v = 0; v < revadjList[u].size(); ++v)
	{
		if(work.find(revadjList[u][v]) == work.end()) continue;
		if(deletedVertices.find(revadjList[u][v]) == deletedVertices.end() && visited.find(revadjList[u][v]) == visited.end())
			explore(revadjList[u][v]);
	}
	v2post[u] = clk;
	post2v[clk++] = u;
}


void DFS()
{
	//cout<<"DFS\n";
	visited.clear();
	for(std::map<size_t,std::vector<size_t> >::iterator u = revadjList.begin(), e = revadjList.end();
		u != e; ++u)
		{
			if(work.find(u->first) == work.end()) continue;
			if(deletedVertices.find(u->first) == deletedVertices.end() && visited.find(u->first) == visited.end())
				explore(u->first);
		}
}

void findSCC(size_t u,std::set<size_t>& cc)
{
	visited.insert(u);
	cc.insert(u);
	//cout<<u<<" ";
	size_t post = v2post[u];
	v2post.erase(u);
	post2v.erase(post);
	
	for(size_t v = 0; v < adjList[u].size(); ++v)
	{
		if(work.find(adjList[u][v]) == work.end()) continue;
		if(deletedVertices.find(adjList[u][v]) == deletedVertices.end() && visited.find(adjList[u][v]) == visited.end())
			findSCC(adjList[u][v],cc);
	}
	
}

std::vector<std::set<size_t> > processDecrPost()
{
	//cout<<"processDecrPost\n";
	std::vector<std::set<size_t> > ccList;
	std::set<size_t> cc;
	//cout<<"components:\n";
	
	while(!post2v.empty())
	{
		//cout<<"src: "<<post2v.begin()->second<<endl;
		findSCC((post2v.begin()->second),cc);
		ccList.push_back(cc);
		//cout<<endl;
		cc.clear();
	}
	return ccList;
}

std::vector<size_t> st;
std::set<size_t> blockedSet;
std::map<size_t,size_t> blockedMap;


void johnson()
{
	//cout<<"johnson"<<endl;
	std::vector<std::set<size_t> >  ccList = enumSCC();
	std::stack<std::set<size_t> > ccStack;
	for(size_t i = 0; i<ccList.size(); i++)
	{
		ccStack.push(ccList[i]);
	}
	
	while(!ccStack.empty())
	{
		work = ccStack.top();
		ccStack.pop();
		
		visited.clear();
		st.clear();
		blockedSet.clear();
		blockedMap.clear();
		//cout<<"trigger ckt with: "<<*(work.begin())<<"\n";
		circuit(*(work.begin()),*(work.begin()));
		deletedVertices.insert(*(work.begin()));
		visited.clear();
		std::vector<std::set<size_t> > newCC = enumSCC();
		for(size_t i = 0; i<newCC.size(); i++)
		{
			ccStack.push(newCC[i]);
		}
		
	}
}

bool circuit(size_t u, size_t src)
{
	bool ret = false;
	
	if(u!=src)
		visited.insert(u);
	
	if(u == src && st.size() != 0)
	{
		cout<<"************* cycle: *************\n";
		for(size_t k = 0; k < st.size(); k++)
			cout<<st[k]<<" ";
		cout<<u<<endl;
		cout<<"**********************************\n";
		return true;		
	}
	
	st.push_back(u);
	//cout<<"stack push: "<<u<<"\n";
	if(blockedSet.find(u) == blockedSet.end())
	{
		blockedSet.insert(u);
		//cout<<"blockedSet push: "<<u<<"\n";
	}
	else 	// should not explore in that direction
	{
		st.pop_back();
		//cout<<"stack pop: "<<u<<"\n";
		visited.erase(u);
		return false;
	}
	for(size_t v = 0; v < adjList[u].size(); ++v)
	{
		size_t k = adjList[u][v];
		//cout<<"deciding edge: "<<u<<" -> "<<adjList[u][v]<<endl;
		if(work.find(adjList[u][v]) == work.end()) continue;

		if(deletedVertices.find(adjList[u][v]) == deletedVertices.end() && visited.find(adjList[u][v]) == visited.end())
		{
			bool f = circuit(adjList[u][v],src);
			//cout<<"exploring edge: "<<u<<" -> "<<adjList[u][v]<<endl;
			ret = ret || f; // becomes true iff atleast one cycle thru u is found
		}
	}
	
	// if all neighbours are explored and found a cycle means unblock
	if(ret)
	{
		unblock(u);
	}
	else
	{
		for(size_t v = 0; v < adjList[u].size(); ++v)
		{
			if(blockedSet.find(adjList[u][v]) != blockedSet.end() && adjList[u][v] != src)
			{
				//if(visited.find(adjList[u][v]) != visited.end()) continue;
				assert(blockedMap.find(adjList[u][v]) == blockedMap.end());
				blockedMap[adjList[u][v]] = u;
				//cout<<"BM: "<<adjList[u][v]<<" --> "<<u<<endl;
			}	
		}		
	}
	
	st.pop_back(); // delete from stack but not from blocked set
	//cout<<"stack pop: "<<u<<endl;
	if(u != src)
		visited.erase(u);
	return ret;
	
}

void unblock(size_t u)
{
	if(blockedMap.find(u) == blockedMap.end())
	{
		blockedSet.erase(u);
		//cout<<"erase from blockedSet: "<<u<<endl;
		return;
	}

//	if(blockedMap.find(blockedMap[u]) != blockedMap.end())
	unblock(blockedMap[u]);
	
//	if(blockedMap.find(u) != blockedMap.end())
//	{
		//cout<<"erase from blockedMap: "<<u<<" --> "<<blockedMap[u]<<endl;
		blockedMap.erase(u);
//	}
	blockedSet.erase(u);
	//cout<<"erase from blockedSet: "<<u<<endl;
}
