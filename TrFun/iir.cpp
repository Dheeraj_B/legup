#include<math.h>
#include<iostream>
#include<fstream>
#include<iomanip> 
#include<vector>
#include<random>
#include "ac_fixed.h"
#include "iir.h"
#include <Python.h>

using namespace std;

double a[] = {-1.710329,0.747274};
double b[] = {0.009236,0.018472,0.009236};

ofstream Out;
ofstream OutF;
ofstream Inp;



/*ac_fixed<31,20,true> A0 = -1.710329;
ac_fixed<28,20,true> A1 = 0.747274;
ac_fixed<34,20,true> B0 = 0.009236;
ac_fixed<34,20,true> B1 = 0.018472;
ac_fixed<34,20,true> B2 = 0.009236;


ac_fixed<31,20,true> iir(ac_fixed<26,20,true> x)
{
	ac_fixed<26,20,true> a0;
	ac_fixed<26,20,true> a1;
	ac_fixed<30,20,true> a2;
	ac_fixed<31,20,true> a3;
	ac_fixed<26,20,true> m0;
	ac_fixed<30,20,true> m1;
	ac_fixed<26,20,true> m2;
	ac_fixed<31,20,true> m3;
	ac_fixed<30,20,true> m4;

	static ac_fixed<26,20,true> reg1 = 0;
	static ac_fixed<26,20,true> reg2 = 0;
	a0 = a1 = a2 = a3= 0;
	m0 = m1 =m2 =m3 =m4 = 0;
	

	m0 = reg1 *A0;
	m2 = reg2 * A1;
	a0 = x - m0;
	a1 = a0 - m2;
	m4 = a1 * B0;
	m1 = B1 * reg1;
	m3 = B2 * reg2;
	a2 = m1 + m4;
	a3 = m3 + a2;
	reg2 = reg1;
	reg1 = a1;
	return a3;
}*/



double eqn(double x)
{
	static double w1=0,w2=0,w3 = 0,w4 = 0;
	double w0;
	w0 = x - a[0]*w1 - a[1]*w2;
	double y = b[0]*w0 + b[1]*w1 + b[2]*w2;
	w2 = w1;
	w1 = w0;
	/*double w5;
	w5 = y - a[0]*w3 - a[1]*w4;
	double z = b[0]*w5 + b[1]*w3 + b[2]*w4;
	w4 = w3;
	w3 = w5;
	
	return z;*/
	return y;
}

int main(int argc, char *argv[])
{
	std::vector<double> inputs;
	std::ifstream infile("inputs");
	double inp;
	
    std::random_device rd;
    std::mt19937 gen(rd());
    //std::uniform_real_distribution<> dis(-5, 5);
    std::normal_distribution<> dis(0,2);

	Out.open("outputNew.txt");
	OutF.open("outputFixpNew.txt");
	Inp.open("inputs.txt");
	double r,angle = 0;
	int cycles = 200;
	float step = 0.1;
	int ctr = 0;
	double sum = 0;
	
	for(int i = 0; i<cycles; i++)
	{
		ctr++;
		angle = 0;
		while(angle <= 180)
		{
			//inp = dis(gen);
			//r = eqn(2*sin(angle*M_PI/180));
			if(angle < 90)	
			{
				inp = 2;
				r = eqn(inp);
				t = iir(inp);
			}
			else
			{
				inp = -2;
				r = eqn(inp);
				t = iir(inp);
			}
			Inp <<std::setprecision(64) << inp <<"\n";
			Out << std::setprecision(64) << r <<"\n";
			OutF << t <<"\n";
			angle+=step;
		}		
	}

	/*for(int i = 0; i<cycles; i++)
	{
		angle = 0;
		while(angle<=90)
		{
			r = iir(5);
			Out << std::setprecision(32) << r <<"\n";
			angle+=step;
			
		}
		
		while(angle<=180)
		{
			r = iir(0);
			Out << std::setprecision(32) << r <<"\n";
			angle+=step;
			
		}
	}*/

	
	/*for(int i = 0; i<inputs.size();i++)
	{
		r = iir(inputs[i]);
		if(i>=200)
			Out << std::setprecision(32) << r <<"\n";
		
	}*/
	Out.close();
	OutF.close();
	Inp.close();
	
    PyObject *pName, *pModule, *pDict, *pFunc, *pValue;

    if (argc < 3) 
    {
        printf("Usage: exe_name python_source function_name\n");
        return 1;
    }
	
	
    // Initialize the Python Interpreter
    Py_Initialize();
	PySys_SetArgv(argc, argv);
    // Build the name object
    pName = PyString_FromString(argv[1]);
    // Load the module object
    pModule = PyImport_Import(pName);
    if(pModule == NULL)    
    {
		PyErr_Print();
		return 1;
	}
    // pDict is a borrowed reference 
	pDict = PyModule_GetDict(pModule);
    // pFunc is also a borrowed reference 
    pFunc = PyDict_GetItemString(pDict, argv[2]);

    if (PyCallable_Check(pFunc)) 
    {
        PyObject_CallObject(pFunc, NULL);
    } else 
    {
        PyErr_Print();
    }
    // Clean up
    Py_DECREF(pModule);
    Py_DECREF(pName);

    // Finish the Python Interpreter
    Py_Finalize();
	
}
