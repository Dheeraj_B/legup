#include "WLO.h"
#include "pow2.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FormattedStream.h"

using namespace llvm;
using namespace legup;

namespace legup
{
	
std::string exec(const char* cmd) {
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) return "ERROR";
    char buffer[128];
    std::string result = "";
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}

// finds all SCC in current working set of vertices and push each into a set. (refer #5)
std::vector<std::set<size_t> > WLO::enumSCC()
{
	//cout<<"enumSCC\n";
	post2v.clear();
	v2post.clear();
	clk = 0;
	DFS();
	visited.clear();
	return processDecrPost();
}

// (refer #5)
void  WLO::explore(size_t u)
{
	visited.insert(u);
	for(size_t v = 0; v < revadjList[u].size(); ++v)
	{
		if(work.find(revadjList[u][v]) == work.end()) continue;
		if(deletedVertices.find(revadjList[u][v]) == deletedVertices.end() && visited.find(revadjList[u][v]) == visited.end())
		{
			//errs()<<"from "<<u<<" to "<<revadjList[u][v]<<"\n";
			explore(revadjList[u][v]);
		}
	}
	v2post[u] = clk;
	post2v[clk++] = u;
}

// (refer #5)
void WLO::DFS()
{
	//cout<<"DFS\n";
	visited.clear();
	errs()<<"revadjlist before DFS\n";
/*	for(std::map<size_t,std::vector<size_t> >::iterator u = revadjList.begin(), e = revadjList.end();u != e; ++u)
	{
		errs()<<u->first<<" : ";
		for(size_t i = 0; i<(u->second).size(); i++)
			errs()<<u->second[i]<<" ";
		errs()<<"\n";
	}*/
	for(std::map<size_t,std::vector<size_t> >::iterator u = revadjList.begin(), e = revadjList.end();
		u != e; ++u)
		{
			errs()<<"in dfs "<<u->first<<"\n";
			if(work.find(u->first) == work.end())
			{
				errs()<<"skipped from work\n";
				continue;
			}
			if(deletedVertices.find(u->first) == deletedVertices.end() && visited.find(u->first) == visited.end())
				explore(u->first);
			else
			{
				errs()<<"del: "<<(deletedVertices.find(u->first) == deletedVertices.end())<<" "
				<<"visit: "<<(visited.find(u->first) == visited.end())<<"\n";
			}
		}
}

// this function is mainly a DFS from vertex u
// has two roles:
// if del is false, then this fills cc with set of reachable vertices from src w/o doing any graph modification
// if del is true, fills cc as above and 
// whenever it visits a vertex it deletes it from postvisit sequences (refer #5) so that after a SCC
// is found we can easily chose the start vertex for next SCC by picking the current highest postvisit numbered vertex
void WLO::findSCC(size_t u,std::set<size_t>& cc, bool del)
{
	visited.insert(u);
	cc.insert(u);
	//cout<<u<<" ";
	if(del)
	{
		size_t post = v2post[u];
		v2post.erase(u);
		post2v.erase(post);
	}
	
	for(size_t v = 0; v < adjList[u].size(); ++v)
	{
		if(del && work.find(adjList[u][v]) == work.end()) continue;
		if(deletedVertices.find(adjList[u][v]) == deletedVertices.end() && visited.find(adjList[u][v]) == visited.end())
			findSCC(adjList[u][v],cc,del);
	}
	
}

// processes vertices in decreasing order of postvisit (refer #5)
std::vector<std::set<size_t> > WLO::processDecrPost()
{
	//cout<<"processDecrPost\n";
	std::vector<std::set<size_t> > ccList;
	std::set<size_t> cc;
	//cout<<"components:\n";
	
	while(!post2v.empty())
	{
		//cout<<"src: "<<post2v.begin()->second<<endl;
		//errs()<<"new scc: "<<"\n";
		findSCC((post2v.begin()->second),cc);
		/*for(std::set<size_t>::iterator ab = cc.begin(), ae = cc.end(); ab!=ae; ++ab)
			errs()<<*ab<<"\n";*/
		ccList.push_back(cc);
		//cout<<endl;
		cc.clear();
	}
	return ccList;
}

// for a more interactive session on how johnson's algo works refer:
// https://www.youtube.com/watch?v=johyrWospv0
void WLO::johnson()
{
	// johnson's algo takes a Strongly Connected Component (SCC) as input. so we need to first identify all SCCs in the graph
	std::vector<std::set<size_t> >  ccList = enumSCC();
	std::stack<std::set<size_t> > ccStack;
	for(size_t i = 0; i<ccList.size(); i++)
	{
		ccStack.push(ccList[i]);
	}
	
	// for each of the SCC found thusly, johnson's routine is run
	while(!ccStack.empty())
	{
		work = ccStack.top();
		ccStack.pop();
		
		// clearing appropriate datatstructures to prepare for next invocation of circuit routine
		visited.clear();
		st.clear();
		blockedSet.clear();
		blockedMap.clear();

		circuit(*(work.begin()),*(work.begin()));
		deletedVertices.insert(*(work.begin()));
		visited.clear();
		
		// finds new SCCs after the graph modification done by circuit routine
		std::vector<std::set<size_t> > newCC = enumSCC();
		for(size_t i = 0; i<newCC.size(); i++)
		{
			ccStack.push(newCC[i]);
		}
		
	}
}

// refer #2 for psuedocode
bool WLO::circuit(size_t u, size_t src)
{
	bool ret = false;
	
	if(u!=src)
		visited.insert(u);
	
	// found a cycle
	if(u == src && st.size() != 0)
	{
		//errs()<<"************* cycle: *************\n";
		for(size_t k = 0; k < st.size(); k++)
		{
			//errs()<<st[k]<<" ";
			loops[lctr].push_back(st[k]);
		}
		liveLoops[lctr] = true;
		lctr++;
		//errs()<<u<<"\n";
		//errs()<<"**********************************\n";
		return true;		
	}
	
	st.push_back(u);
	//cout<<"stack push: "<<u<<"\n";
	if(blockedSet.find(u) == blockedSet.end())
	{
		blockedSet.insert(u);
		//cout<<"blockedSet push: "<<u<<"\n";
	}
	else 	// should not explore in that direction
	{
		st.pop_back();
		//cout<<"stack pop: "<<u<<"\n";
		visited.erase(u);
		return false;
	}
	for(size_t v = 0; v < adjList[u].size(); ++v)
	{
		size_t k = adjList[u][v];
		//cout<<"deciding edge: "<<u<<" -> "<<adjList[u][v]<<endl;
		if(work.find(adjList[u][v]) == work.end()) continue;

		if(deletedVertices.find(adjList[u][v]) == deletedVertices.end() && visited.find(adjList[u][v]) == visited.end())
		{
			bool f = circuit(adjList[u][v],src);
			//cout<<"exploring edge: "<<u<<" -> "<<adjList[u][v]<<endl;
			ret = ret || f; // becomes true iff atleast one cycle thru u is found
		}
	}
	
	// if all neighbours are explored and found a cycle means unblock
	if(ret)
	{
		unblock(u);
	}
	else
	{
		for(size_t v = 0; v < adjList[u].size(); ++v)
		{
			if(blockedSet.find(adjList[u][v]) != blockedSet.end() && adjList[u][v] != src)
			{
				//if(visited.find(adjList[u][v]) != visited.end()) continue;
				assert(blockedMap.find(adjList[u][v]) == blockedMap.end());
				blockedMap[adjList[u][v]] = u;
				//cout<<"BM: "<<adjList[u][v]<<" --> "<<u<<endl;
			}	
		}		
	}
	
	st.pop_back(); // delete from stack but not from blocked set
	//cout<<"stack pop: "<<u<<endl;
	if(u != src)
		visited.erase(u);
	return ret;
	
}

// refer #2 for psuedocode
void WLO::unblock(size_t u)
{
	if(blockedMap.find(u) == blockedMap.end())
	{
		blockedSet.erase(u);
		//cout<<"erase from blockedSet: "<<u<<endl;
		return;
	}

//	if(blockedMap.find(blockedMap[u]) != blockedMap.end())
	unblock(blockedMap[u]);
	
//	if(blockedMap.find(u) != blockedMap.end())
//	{
		//cout<<"erase from blockedMap: "<<u<<" --> "<<blockedMap[u]<<endl;
		blockedMap.erase(u);
//	}
	blockedSet.erase(u);
	//cout<<"erase from blockedSet: "<<u<<endl;
}

// acts as a wrapper for triggering pathDFS
void WLO::enumPaths(size_t src, size_t dst)
{
	for(std::map<size_t,std::vector<size_t> >::iterator u = adjList.begin(), e = adjList.end();
		u != e; ++u)
		{
			visitedNodes[u->first] = false;
		}
	
	pathDFS(src,dst);

}

// DFS to find all paths from src to dst
// refer: http://www.geeksforgeeks.org/find-paths-given-source-destination/
void WLO::pathDFS(size_t& src, size_t& dst)
{
	//cout<<"vis: "<<src<<" dst: "<<dst<<endl;
	if(src == dst)
	{
		printPathStack(dst);
		return;
	}
	
	pathStack.push_back(src);
	visitedNodes[src] = true;

	for(size_t v = 0; v < adjList[src].size(); ++v)
	{
		if(visitedNodes[adjList[src][v]]) continue;
		
		pathDFS(adjList[src][v], dst);
	}
	
	pathStack.pop_back();
	visitedNodes[src] = false;
}

// updates the paths LUT once a path is found
void WLO::printPathStack(size_t dst)
{
	for(size_t i = 0; i<pathStack.size(); i++)
	{
		//errs()<<pathStack[i]<<" ";
		paths[pctr].push_back(pathStack[i]);
	}
	paths[pctr].push_back(dst);
	pctr++;
	//errs()<<dst<<"\n";
}

// sorts the nodes in a path. This is done so that we can safely input to merge() for checking intersections
// at no point in the workflow we need the actual sequence of vertices in a path!
void WLO::sortPaths()
{
	for(std::map<size_t,std::vector<size_t> >::iterator b = paths.begin(), e = paths.end(); b!=e; ++b)
	{
		std::sort((b->second).begin(),(b->second).end());
	}
}

// similar to above function
void WLO::sortLoops()
{
	for(std::map<size_t,std::vector<size_t> >::iterator b = loops.begin(), e = loops.end(); b!=e; ++b)
		std::sort((b->second).begin(),(b->second).end());	
}

// primary function for checking disjointness of 2 sorted vectors
// true if not disjoint
bool WLO::merge(std::vector<size_t>& a, std::vector<size_t>& b, size_t beg1, size_t beg2)
{
	if(beg1 == a.size() || beg2 == b.size()) // all elements of an array got expired
		return false;
	if(a[beg1] == b[beg2])
		return true;
	if(a[beg1] < b[beg2])
		return merge(a,b,++beg1,beg2);
	else
		return merge(a,b,beg1,++beg2);	
}

// gets netweight of a path or a loop. (refer #3)
std::string WLO::getNetWeight(std::vector<size_t> nodes)
{
	std::string netWt = "";
	for(size_t i = 0; i<nodes.size(); i++)
	{
		netWt+="("+weights[nodes[i]]+")*";
	}
	netWt.pop_back();
	return netWt;
}

// need to be called for every graph modification
// builds the pathsNloops matrix. It has dimension NUM_PATHS X NUM_LOOPS
void WLO::buildpathsNloops()
{
	for(std::map<size_t,std::vector<size_t> >::iterator p = paths.begin(), pe = paths.end(); p!=pe; ++p)
	{
		std::vector<bool> temp;
		for(std::map<size_t,std::vector<size_t> >::iterator l = loops.begin(), le = loops.end(); l!=le; ++l)
		{
			if(!liveLoops[l->first]) temp.push_back(false);
			else
				temp.push_back(merge(p->second, l->second));
		}
		pathsNloops.push_back(temp);
		std::string h = getNetWeight(p->second);
		pathWeights.push_back(h);
	}
}

// need to be called only once since no new loops are created by graph modifications
// similar to above function
void WLO::buildloopsNloops()
{
	for(std::map<size_t,std::vector<size_t> >::iterator p = loops.begin(), pe = loops.end(); p!=pe; ++p)
	{
		std::vector<bool> temp;
		for(std::map<size_t,std::vector<size_t> >::iterator l = loops.begin(), le = loops.end(); l!=le; ++l)
		{
			temp.push_back(merge(p->second, l->second));			
		}
		loopsNloops.push_back(temp);
		loopWeights.push_back(getNetWeight(p->second));
	}
}

// different combinations of loops must be considered for multiplication while creating the transfer function (refer #3)
// this function returns the algebraic expression generated by legal combinations out of 
// "n choose r" possibilities of loop multiplications.
// paths-loop intersections has to be considered for creating numerator polynomial, hence pathindex != -1
// no such consideration while creating Dr, hence pathindex will be set as -1
// also refer: http://stackoverflow.com/questions/9430568/generating-combinations-in-c
std::string WLO::getProductTerms(size_t n, size_t r, int pathindex)
{
    std::vector<bool> v(n);
    std::string denom = "";
    std::fill(v.begin() + n - r, v.end(), true);
    do {
		size_t numTerms = 0;
		std::vector<size_t> temp;
		std::string netWt = "";
		bool stat = true;
        for (size_t i = 0; i < n; ++i)
        {
            if (v[i] && liveLoops[i])  // only need to consider live loops
            {
                for(size_t j = 0; j<temp.size(); j++)
                {
					//cout<<"considering loops: "<<i<<" and "<<temp[j]<<endl;
					if(loopsNloops[i][temp[j]])
					{
						stat = false;
						//cout<<"intersects"<<endl;
						break;
					}
				}
				if(!stat) break;
				if(pathindex!=-1 && pathsNloops[pathindex][i]) break; // if current loop intersects with given path
				else
				{
					temp.push_back(i);
					netWt+=loopWeights[i]+"*";
					numTerms++;
					//cout<<"lw: "<<loopWeights[i]<<endl;
				}
                //std::cout << (i+1) << " ";
            }
        }
        //cout<<"num Terms: "<<numTerms<<endl;
        if(numTerms >= r && netWt.size() != 0)
		{
			netWt.pop_back();
			denom+=netWt+"+";
		}
		
        //std::cout << "\n";
    } while (std::next_permutation(v.begin(), v.end()));
    
    if(denom.size() != 0)
		denom.pop_back();
	//cout<<"returning: "<<denom<<endl;
	return denom;
}

// builds denominator polynomial according to reference #3
void WLO::buildDenominator()
{
	std::string dr = "1";
	size_t ctr = 0;
	while(ctr < loops.size())
	{		
		ctr++;
		std::string p = getProductTerms(loops.size(),ctr);
		if(p.size() && ctr%2)
			dr+="-("+p+")";
		else if(p.size() && !(ctr%2))
			dr+="+("+p+")";
	}
//	cout<<"denominator: "<<dr<<endl;
	Out<<"("<<dr<<")\n";
}

// builds nuemerator polynomial according to reference #3
void WLO::buildNumerator()
{
	std::string nr = "";
	for(size_t pathindex = 0; pathindex < paths.size(); ++pathindex)
	{
		nr+=pathWeights[pathindex]+"*(1";
		size_t ctr = 0;
		while(ctr < loops.size())
		{		
			ctr++;
			std::string p = getProductTerms(loops.size(),ctr,pathindex);
			if(p.size() && ctr%2)
				nr+="-("+p+")";
			else if(p.size() && !(ctr%2))
				nr+="+("+p+")";
			
		}
		nr+=")+";
	}
	nr.pop_back();
//	cout<<"numerator: "<<nr<<endl;
	Out<<"("<<nr<<")/";
}
 
// gbreak does the following transform on the graph:
// insert an adder just after the output of the vertex "node"
// the newly inserted adder has id=numNodes
void WLO::gBreak(size_t node)
{
	adjList_cpy = adjList;
	revadjList_cpy = revadjList;
	std::vector<size_t> outV = adjList[node];
	adjList[node].clear(); // setting out degree to zero
	adjList[node].push_back(numNodes);
	adjList[numNodes] = outV;
	
	// resetting globals for future paths exploration
	paths.clear();
	pathsNloops.clear();
	pathWeights.clear();
	pctr = 0;
	
/*	for(size_t i = 0; i<adjList.size(); i++)
	{
		cout<<"node "<<i<<": ";
		for(size_t j = 0; j<adjList[i].size(); j++)
			cout<<adjList[i][j]<<" ";
		cout<<"\n";
	}*/
	
/*	for(size_t u = 0; u<outV.size(); u++) 	
	{
		size_t v;
		for(v = 0; v<revadjList[outV[u]].size(); v++)
		{
			if(revadjList[outV[u]][v] == node) break;
		}
		revadjList[outV[u]].erase(revadjList[outV[u]].begin()+v);
		revadjList[outV[u]].push_back(numNodes);
	}
	std::vector<size_t> nullVector;
	revadjList[numNodes] = nullVector;*/
}

// undo the change made by gbreak
void WLO::gWane()
{
	adjList = adjList_cpy;
	revadjList = revadjList_cpy;
}

// should be called only after sortLoops
// in the graph the node removedNode is NOT actually removed. An adder is inserted just after the output of
// removedNode. Refer to my thesis for more details
void WLO::selectLiveLoops(size_t removedNode)
{
	
	for(size_t i = 0; i<loops.size(); i++)
	{
		if(std::binary_search(loops[i].begin(), loops[i].end(),removedNode))
		{
			// all loops touching the current removedNode do not contribute to Numerator polynomial
			// hence their liveliness is set false
			// but irresepective of intersections with the removedNode, those loops can contribute to 
			// denominator polynomial and hence their liveliness must be restored while computing Dr.
			// Hence they are inserted into set toReset
			liveLoops[i] = false;
			toReset.insert(i);
		}
		else
			liveLoops[i] = true;
	}
	//cout<<"rem node: "<<removedNode<<endl;
	// kill nonreachable loops from the adder that is newly inserted
	// the newly inserted adder has always an id = numNodes
	killNonReachableLoops(numNodes);
}

// checks reachability of loops from given node
void WLO::killNonReachableLoops(size_t node)
{
	std::set<size_t> cc;
	std::vector<size_t> temp;
	deletedVertices.clear();
	visited.clear();
	findSCC(node,cc,false);
	//cout<<"temp: "<<endl;
	for(std::set<size_t>::iterator b = cc.begin(), e = cc.end(); b!=e; ++b)
	{
		//cout<<*b<<" ";
		temp.push_back(*b);
	}
	//cout<<endl;
	for(size_t i = 0; i<loops.size(); i++)
	{
		//cout<<"curr loop: ";
		//for(size_t j = 0; j<loops[i].size(); j++)
			//cout<<loops[i][j]<<" ";
		//cout<<endl;
		if(!merge(temp,loops[i]))
		{
			//cout<<"not intersects"<<endl;
			assert(toReset.find(i) == toReset.end());	// because all loops in the toReset set is defenitely reachable from the adder
			liveLoops[i] = false;
		}
		//else
			//cout<<"intersects"<<endl;
	}
}

// takes a register index and fills first non register driver
// example: consdier the below graph
// [adder1]--->[reg1]--->[reg2]
// if fillRegDrivers is called with index of reg2 then that index is pushed into regDrivers[index of adder1 in wordlength vector].
// wordlengths of adder1 and all entries in regDrivers[index of adder1 in wordlength vector] must be same
void WLO::fillRegDrivers(size_t i)
{
	size_t i_cpy = i;
	
	
	while(weights[revadjList[i][0]] == "z")
	{
		i = revadjList[i][0];
	}
	regDrivers[revadjList[i][0]].push_back(i_cpy);
}

void WLO::createExtensions()
{
	depList.resize(hMean[0].size()+numMults);
	bool done = false;
	
	for(size_t i = 0; i<hMean.size(); i++)		// size of hMean = # of POs
	{
		std::vector<double> m,v;
		m.resize(hMean[i].size()+numMults);
		v.resize(hMean[i].size()+numMults);
		size_t visitedMuls = 0;		
		
		for(std::map<size_t,std::vector<size_t> >::iterator op = revadjList.begin(), e = revadjList.end(); op!=e; ++op)
		{
			m[op->first] = hMean[i][op->first];
			v[op->first] = hVar[i][op->first];
			if(weights[op->first] != "z" && weights[op->first] != "1")
			{
				//cout<<"adding "<<weights[op->first]<<"to "<<hMean[i].size()+visitedMuls<<endl;
				m[hMean[i].size()+visitedMuls] = hMean[i][op->first];
				v[hMean[i].size()+visitedMuls] = hVar[i][op->first];
				if(!done)
				{
					//cout<<"% "<<hMean[i].size()+visitedMuls<<"\n";
					constIndices.insert(hMean[i].size()+visitedMuls);
					depList[op->first].push_back(hMean[i].size()+visitedMuls);
				}
				visitedMuls++;
			}
			
				
			if(!done)
			{
				for(size_t i = 0; i<(op->second).size(); i++)
					depList[op->first].push_back((op->second)[i]);
			}
		}
		done = true;
		xtendedMean.push_back(m);
		xtendedVar.push_back(v);
	}
	// TODO: initial case of op->first == 0 can be peeled out
	for(size_t i = 0; i<depList.size(); i++)
	{
		errs()<<i<<" : ";
		for(size_t j = 0; j<depList[i].size(); j++)
			errs()<<depList[i][j]<<" ";
		errs()<<"\n";
	}
}

// calculates noise power for the given vector WL. (refer #1)
double WLO::calculateSNR(std::vector<int> WL)
{
	double noise = -99999999999999;
	double s;
	for(size_t t = 0; t<hMean.size(); t++)		// for all POs
	{
		double meanSum = 0, varSum = 0;
		
		for(size_t i = 0; i<WL.size(); i++)	
		{
			// no noise contribution from registers as they have same width as their drivers
			if(i<numNodes && weights[i] == "z") 
			{
				if(tr) errs()<<"skip "<<i<<" z"<<"\n";
				continue; // skip registers
			}
			// multiplier constant width is already fixed and the model does not take into consideration
			// the quantization noise from the multiplier constants
			if(constIndices.find(i) != constIndices.end())
			{
				if(tr) errs()<<"skip "<<i<<" c"<<"\n";
				continue;
			}
			std::pair<double,double> stat = getOperatorStats(i,WL);
	//		cout<<"index: "<<i<<" mean: "<<stat.first<<" std: "<<stat.second<<endl;
			meanSum+=(xtendedMean[t][i]*stat.first);
			varSum+=(xtendedVar[t][i]*stat.second);
	//		cout<<"multiplied var: "<<xtendedVar[i]<<"\n";
		}
		
		s = 10*log10(varSum+(meanSum*meanSum));
		noise = (s > noise) ? s : noise;					// finds max noise in MIMO systems
		
	}
	return noise;
}

// returns the mean and variance from the given operator according to the truncation noise model 
// presented in reference #1
std::pair<double,double> WLO::getOperatorStats(size_t i, std::vector<int> WL) // BEWARE: i is index in WL
{
	
	double q = fastPower2[WL[i]];
	if(depList[i].size() == 0)	// natural quantizers
	{
//		cout<<"natural quantizer\n";
		return std::make_pair(0.5*q,(1.0/12)*q*q);
	}
	
	// after this point i<numNodes always
	assert(i<numNodes);
	unsigned long long unity = 1;
	size_t trueWL = 0;
	int k = 0, l =0;
	std::pair<double,double> mean_var;
	size_t constWL;
//	cout<<"inps: ";
	if(weights[i] != "1")
	{
		for(size_t t = 0; t<depList[i].size(); t++)
		{
//			cout<<WL[depList[i][t]]<<" ";
//			if(depList[depList[i][t]].size() == 0)
//			{
				//constWL = WL[depList[i][t]];
//				constIndices.insert(depList[i][t]);
//			}
			trueWL+=WL[depList[i][t]];
		}
	}
	else
	{
		for(size_t t = 0; t<depList[i].size(); t++)
		{
//			cout<<WL[depList[i][t]]<<" ";
			trueWL = (trueWL < WL[depList[i][t]])? WL[depList[i][t]] : trueWL;
		}
	}
//	cout<<"\n";
	
	if(trueWL <= WL[i])				// no quantization noise source added in this case
	{
		//cout<<"no error case\n";
		return std::make_pair(0,0);
	}
	else
	{
		k = trueWL - WL[i];
		//cout<<"k = "<<k<<"\n";
		if(weights[i] != "1") 
		{
			l = getNumTrailingZeroes(multConstants[i],CONSTWL);
		}
		mean_var = getStats(q,k,l);
		return mean_var;
	}
}

std::pair<double,double> WLO::getStats(double q, int k, int l)
{
	if(k < l) l = 0;
	double m = 0.5*q*(1-fastPower2[k-l]);
	double v = (1.0/12)*q*q*(1-(fastPower2[(k-l)]*fastPower2[(k-l)]));
	//cout<<"mean: "<<m<<" var: "<<v<<endl;
	return std::make_pair(m,v);
}

// gets number of trailing zeroes in the multiplier constant
size_t WLO::getNumTrailingZeroes(float x, size_t f)
{
	float y = x * pow(2,f);
	double fractpart, intpart;
	fractpart = modf(y , &intpart);
	//cout<<"y: "<<y<<" int: "<<intpart<<" fract: "<<fractpart<<"\n";
	if(y<0 && fabs(fractpart)>0) intpart--;
	unsigned long long ip = fabs(intpart);
	size_t ctr = 0;
	if(ip == 0) return 0;
	while(ip%2 == 0)
	{
		ctr++;
		ip/=2;
	}
	return ctr;
}

// maintains regwidths same as their drivers
void WLO::houseKeepRegWidths(std::vector<int>& WM, size_t j, int mode)
{
	assert(mode != -1);
	if(regDrivers.find(j) != regDrivers.end())
	{
		for(size_t i = 0; i<regDrivers[j].size(); i++)
		{
			//assert(WM[j] == WM[regDrivers[j][i]]);
			if(mode == 0)					// decr
				WM[regDrivers[j][i]]--;		
			else if(mode == 1)				// incr
				WM[regDrivers[j][i]]++;
			else 							// assignment
				WM[regDrivers[j][i]] = WM[j];	
		}
	}
	
}

// verbatim implementation of algo 1 in reference #4
std::vector<int> WLO::getMWC()
{
	std::vector<int> WMC;
	std::vector<int> WM = wMax;
	WMC.resize(wMax.size());
	calculateSNR(WM);
	for(int j=0;j<WM.size(); j++)
	{
		if(j<numNodes && weights[j] == "z") continue;	// skip registers
		if(constIndices.find(j) != constIndices.end())
		{
			WMC[j] = CONSTWL;
			continue;
		}
		//cout<<"******************************** j = "<<j<<endl;
		while(WM[j] > 0 && calculateSNR(WM) < minNoise )
		//while(WM[j] > 0 && calculateSNR(WM) > minSNR )
		{
			houseKeepRegWidths(WM,j,0);
			WM[j]--;
			//cout<<"\t\tl = "<<WM[j]<<endl;
		}
		if(WM[j] != MAXOPWL)
		{
			++WM[j];
			WMC[j] = WM[j];
			houseKeepRegWidths(WMC,j,2);
			//cout<<"a j: "<<j<<" , "<<WMC[9]<<endl;
		}
		else
		{
			WMC[j] = MAXOPWL;
			houseKeepRegWidths(WMC,j,2);
			//cout<<"b j: "<<j<<" , "<<WMC[9]<<endl;
		}
		WM[j] = MAXOPWL;
		houseKeepRegWidths(WM,j,2);
		//cout<<"c j: "<<j<<" , "<<WMC[9]<<endl;
	}
	return WMC;
}

// verbatim implementation of algo 2 in reference #4
std::vector<int> WLO::greedyAscent(std::vector<int> WL)
{
	std::set<int> prohibitedIndices;
	
	double currSNR = calculateSNR(WL);
	while(currSNR > minNoise)
	//while(currSNR < minSNR)
	{
		std::vector<double> gradient;
		gradient.resize(WL.size());

		double snr = currSNR;
		double c = getCost(WL);

		for(int i = 0; i < WL.size(); i++)
		{
			if(weights[i] == "z") continue;
			if(constIndices.find(i) != constIndices.end()) continue;
			if(prohibitedIndices.find(i) != prohibitedIndices.end())
			{
				gradient[i] = -999999998;
				continue;
			}
			std::vector<int> WL_delta = WL;
			WL_delta[i]++;
			houseKeepRegWidths(WL_delta,i,1);
			//double g = (calculateSNR(WL_delta)-calculateSNR(WL))/(getCost(WL_delta) -  getCost(WL));
			double c_delta = getCost(WL_delta);
			double snr_delta = calculateSNR(WL_delta);
			double g;
			if(c_delta ==  c)
			{
				if(snr_delta < snr)	g = 999999998;
				else 	g = -999999998;
			}
			else
			{
				//g = (calculateSNR(WL)-calculateSNR(WL_delta))/(getCost(WL_delta) -  getCost(WL));
				g = (snr-snr_delta)/(c_delta-c);
			}
			gradient[i] = g;
		}
		int index = findMaxIndex(gradient,prohibitedIndices);
		if(index == -1) break;
		WL[index] = WL[index] + 1;
		houseKeepRegWidths(WL,index,2);
		if(WL[index] == MAXOPWL) prohibitedIndices.insert(index);
		currSNR = calculateSNR(WL);
	}
	return WL;
}

// finds non prohibited index having maximum gradient
int WLO::findMaxIndex(std::vector<double> arr, std::set<int> prohibitedIndices)
{
	double y = -999999999;
	int index = -1;
	for(int i = 0; i<arr.size(); i++)
	{
		if(i<numNodes && weights[i] == "z") continue;
		if(constIndices.find(i) != constIndices.end()) continue;
		if(y < arr[i] && prohibitedIndices.find(i) == prohibitedIndices.end())
		{
			y = arr[i];
			index = i;
		}
	}
	return index;
}

// cost function evaluation
// refer to my thesis for how cost is ascertained
double WLO::getCost(std::vector<int> WL)
{

	double sum = 0;
	size_t ctr = 0;
	for(size_t i = 1; i<adjList.size(); i++)
	{
		ctr++;
		if(weights[i] == "z") sum+=WL[i];
		else if(weights[i] == "1")
		{
			sum+=WL[i]+WL[depList[i][0]]+WL[depList[i][1]];
		}
		else
		{
			sum+=WL[i]+(WL[depList[i][0]]*WL[depList[i][1]]);
		}
	}
	return sum;

/*	return 
	WL[11]+WL[4]+
	WL[0]+WL[6]+
	WL[5]+WL[8]+
	WL[2]+WL[7]+
	WL[12]*WL[9]+
	WL[13]*WL[9]+
	WL[14]*WL[10]+
	WL[15]*WL[10]+
	WL[16]*WL[1]+
	WL[9]+
	WL[10];*/
	
	
/*	int sum = 0;
	for(int i = 0; i<17; i++)
		sum+=WL[i];
	return sum;*/

	// iir 2
/*	return 
	WL[1]+WL[0]+WL[5]+
	WL[2]+WL[1]+WL[7]+
	WL[3]+WL[6]+WL[9]+
	WL[4]+WL[3]+WL[8]+
	WL[5]+WL[12]*WL[10]+
	WL[6]+WL[13]*WL[10]+
	WL[7]+WL[14]*WL[11]+
	WL[8]+WL[15]*WL[11]+
	WL[9]+WL[16]*WL[2]+
	WL[10]+
	WL[11];*/
	
	
	// iir 4
/*	return
	WL[0]+WL[22]+WL[4]+
	WL[1]+WL[0]+WL[6]+
	WL[2]+WL[5]+WL[8]+
	WL[3]+WL[2]+WL[7]+
	WL[4]+WL[23]*WL[9]+
	WL[5]+WL[24]*WL[9]+
	WL[6]+WL[25]*WL[10]+
	WL[7]+WL[26]*WL[10]+
	WL[8]+WL[27]*WL[1]+
	WL[11]+WL[3]+WL[15]+
	WL[12]+WL[11]+WL[17]+
	WL[13]+WL[16]+WL[19]+
	WL[14]+WL[13]+WL[18]+
	WL[15]+WL[28]*WL[20]+
	WL[16]+WL[29]*WL[20]+
	WL[17]+WL[30]*WL[21]+
	WL[18]+WL[31]*WL[21]+
	WL[19]+WL[32]*WL[12]+
	WL[9]+
	WL[10]+
	WL[20]+
	WL[21];*/
	
	// fir
	/*return
	WL[2]+WL[0]+WL[3]+
	WL[6]+WL[2]+WL[5]+
	WL[3]+WL[7]*WL[1]+
	WL[5]+WL[8]*WL[4]+
	WL[1]+
	WL[4];*/
	
	// moving average
/*	return
	WL[1]+WL[0]*CONSTWL+
	WL[2]+WL[0]*CONSTWL+
	WL[3]+WL[0]*CONSTWL+
	WL[4]+WL[0]*CONSTWL+
	WL[5]+WL[0]*CONSTWL+
	WL[6]+WL[0]*CONSTWL+
	WL[7]+WL[0]*CONSTWL+
	WL[8]+WL[0]*CONSTWL+
	WL[9]+WL[1]+WL[2]+
	WL[10]+WL[9]+WL[3]+
	WL[11]+WL[10]+WL[4]+
	WL[12]+WL[11]+WL[5]+
	WL[13]+WL[12]+WL[6]+
	WL[14]+WL[13]+WL[7]+
	WL[15]+WL[14]+WL[8]+
	7*WL[0];*/
}

// verbatim implementation of algo 3 in reference #4
std::vector<int> WLO::tabuSearch(std::vector<int> WL)
{
	int iter = 1;
	double Copt = getCost(WL);
	std::vector<int> Wopt,Wmin;
	Wmin = WL;
	Wopt = WL;
	
	std::vector<int> T; // contains indices of Tabu operators
	std::map<int,bool> Tabu;
	for(int i =0; i<WL.size(); i++)
	{
		if(i < numNodes && weights[i] == "z")
			Tabu[i] = true;
		else if(constIndices.find(i) != constIndices.end())
		{
			Tabu[i] = true;
			//errs()<<"tabooed index a: "<<i<<"\n";
			T.push_back(i);
		}
		else
			Tabu[i] = false;
	}
		
	int d = (calculateSNR(WL) < minNoise) ? -1: 1;
	//int d = (calculateSNR(WL) > minSNR) ? -1: 1;
	
	//assert(d == -1);
	
	// registers are already Tabooed
	for(std::map<size_t,std::vector<size_t> >::iterator b = regDrivers.begin(), e = regDrivers.end(); b!=e; ++b)
	{
		for(int i = 0; i < (b->second).size(); i++)
		{
			T.push_back((b->second)[i]);
			//errs()<<"tabooed index b: "<<(b->second)[i]<<"\n";
		}
	}
	
	size_t numRegs = T.size();
	
	int y = 0;
	while(T.size() < WL.size())
	{
		y++; 
		int ctr = -1;
		std::vector<double> gradient(WL.size(),-999999999); 

		double c = getCost(WL);
		double snr = calculateSNR(WL);

		for(std::map<int,bool> :: iterator op = Tabu.begin(), ope = Tabu.end(); op!=ope; op++)
		{
			
			ctr++;
			if(op->second) continue;
			
			//cout<<"iteration: "<<y<<" "<<ctr<<" "<<WL[op->first]<<endl;
			std::vector<int> WL_delta = WL;
			int index = op->first;
			// now we have to deal with non tabu operators
			if(d>0 && WL[index] < MAXOPWL)
			{
				WL_delta[index]++;
				houseKeepRegWidths(WL_delta,index,1);
			}
			else if(d<0 && WL[index] > MINOPWL)
			{
				WL_delta[index]--;
				houseKeepRegWidths(WL_delta,index,0);
			}
			else
			{
				T.push_back(index);
				//errs()<<"tabooed index c: "<<index<<"\n";
				Tabu[op->first] = true;
			}
			
			if(op->second) continue;
			
			// now we have to compute gradient
			double c_delta = getCost(WL_delta);
			//double c = getCost(WL);
			double snr_delta = calculateSNR(WL_delta);
			//double snr = calculateSNR(WL);
			double g;
			if(c_delta ==  c)
			{
				if(snr_delta < snr)	g = 999999998;
				else 	g = -999999998;
			}
			else
			{
				//g = (calculateSNR(WL)-calculateSNR(WL_delta))/(getCost(WL_delta) -  getCost(WL));
				g = (snr-snr_delta)/(c_delta-c);
			}
			//double g = (calculateSNR(WL_delta)-calculateSNR(WL))/(getCost(WL_delta) -  getCost(WL));
			//cout<<"# "<<((calculateSNR(WL)-calculateSNR(WL_delta)) > 0)<<" "<<((getCost(WL_delta) -  getCost(WL))>0)<<" "<<g<<"\n";
			gradient[ctr] = g;
			if(calculateSNR(WL_delta) < minNoise)
			//if(calculateSNR(WL_delta) > minSNR)
			{
				//cout<<"************* NOT VIOLATED by  ";
				//for(int m = 0; m<WL_delta.size(); m++)
					//cout<<WL_delta[m]<<" ";
				//cout<<"\t";
				if(getCost(WL_delta) < getCost(Wopt))
				{
					Wopt = WL_delta;
					Copt = getCost(WL_delta);
					//cout<<"CHANGED";
				}
				//cout<<"\n";
			}
		}
		if(T.size() == WL.size()) break;
		
		if(d > 0)
		{
			int index = findMaxIndex(gradient);
			assert(index != -1);
			WL[index]++;
			houseKeepRegWidths(WL,index,1);
			if(calculateSNR(WL) < minNoise)
			//if(calculateSNR(WL) > minSNR)
			{
				d = -1; // change of direction as min is now fullfilled
				T.push_back(index);
				//errs()<<"tabooed index d: "<<index<<"\n";
				Tabu[index] = true;
			}
		}
		else
		{
			int index = findMinIndex(gradient);
			assert(index != -1);
			WL[index]--;
			houseKeepRegWidths(WL,index,0);
			if(calculateSNR(WL) > minNoise)
			//if(calculateSNR(WL) < minSNR)
			{
				//cout<<"violated at: ";
				//for(int t = 0; t<WL.size(); t++)
					//cout<<WL[t]<<" ";
				//cout<<" changing to ascent\n";
				d = 1;
			}
		}

	}
	return Wopt;
}

// finds index having maximum gradient
int WLO::findMaxIndex(std::vector<double> arr)
{
	//cout<<"size: "<<arr.size()<<endl;
	double y = -999999999;
	int index = -1;
	for(int i = 0; i<arr.size(); i++)
	{
		if(i < numNodes && weights[i] == "z") continue;
		if(constIndices.find(i) != constIndices.end()) continue;
		if(y < arr[i])
		{
			y = arr[i];
			index = i;
		}
	}
	return index;
}

// finds index having minimum gradient
int WLO::findMinIndex(std::vector<double> arr)
{
	//cout<<"size: "<<arr.size()<<endl;
	double y = 999999999;
	int index = -1;
	for(int i = 0; i<arr.size(); i++)
	{
		if(i < numNodes && weights[i] == "z") continue;
		if(constIndices.find(i) != constIndices.end()) continue;
		if(arr[i] != -999999999 && y > arr[i])
		{
			y = arr[i];
			index = i;
		}
	}
	return index;
}

// initialized the graph for the workflow
void WLO::createGraph(std::map<size_t,std::vector<size_t> > xadjList,
std::map<size_t,std::vector<size_t> > xrevadjList,
std::map<size_t,std::string> xweights,
std::set<size_t> xwork)
{
	adjList = xadjList;
	revadjList = xrevadjList;
	weights = xweights;
	work = xwork;
}

// reads and sets the maximum noise constraint presented in decibels
void WLO::setMinNoise()
{
	std::ifstream infile("AccuracyConstraints/MaxNoise.txt");
	assert(infile.is_open());
	std::string line;
	while(getline (infile,line))
	{
		minNoise = std::stod(line);
	}
	infile.close();
}

// the main function for triggering WLO engine
// src is input and dst is output of the SISO system
std::vector<int> WLO::generateWordLengths(size_t src, size_t dst)
{
	for(size_t i = 0; i<regIndices.size(); i++)									
	{
		fillRegDrivers(regIndices[i]);
	}
	
	std::vector<size_t> nv;
	// out_neighbours of PO is null set
	adjList[dst] = nv;
	revadjList[src] = nv;
	
	PO.push_back(dst);
	// finds all cycles in the graph
	johnson();
	
	bool init = true;
	for(std::vector<size_t>::iterator ib = PO.begin(), ie = PO.end(); ib!=ie; ib++)
	{
		Out.open("h_of_z");
		dst = *ib;
		size_t ctr = 0;
		weights[numNodes] = "1";
		while(ctr < numNodes)
		{
			if(ctr == dst) // no path from dst to dst since no self loops are present 
			{	
				//cout<<"detect "<<ctr<<"\n";
				ctr++;
				Out<<"(1/1)\n";
				continue; 
			}
	//		cout<<"node: ------------------------------------------ "<<ctr<<endl;
			gBreak(ctr);
			//cout<<"******* paths ***********\n";
			enumPaths(numNodes,dst);
			sortPaths();
			
			// sorting loops need to be done only once
			if(init)
			{
				sortLoops();		
			}
			toReset.clear();
			
			selectLiveLoops(ctr);
			buildpathsNloops();
			if(init)
			{
				buildloopsNloops();
				init = false;
			}
			buildNumerator();
			
			// reset liveliness before Dr computaion, see selectliveloops()
			for(std::set<size_t>::iterator beg = toReset.begin(), en = toReset.end(); beg != en; ++beg)
				liveLoops[*beg] = true;
			buildDenominator();
			gWane();
			//if(ctr == 9)	assert(0);
			ctr++;
		}
		
		
		Out.close();
		
		// invoking a python script to compute H[0] and energy content in the transfer functions (refer #1)
		// the actual invocation occurs through an external file "callback.cpp" so that we do not have to change the
		// linking parameters in the original Legup compilation workflow
		exec("g++ -I/usr/include/python2.7 -L/usr/lib/python2.7/config/ -o py callback.cpp -lpython2.7 -ldl -lm -lutil -lz -pthread");
		std::string out = exec("./py pyTrans computePower");
		
		// saving the result from the python script
		std::stringstream ss;
		ss<<out;
		double power;
		double avg;
	//	cout<<"printing powers:\n";
		std::vector<double> m,v;
		while(ss>>power)
		{
			ss>>avg;
			errs()<<"power: "<<power<<" avg: "<<avg<<"\n";
			m.push_back(avg);
			v.push_back(power);
			//cout<<power<<"\n";
		}
		hMean.push_back(m);
		hVar.push_back(v);

	}
	
	errs()<<"printing hmean\n";
	for(size_t i = 0; i<hMean.size(); i++)
	{
		for(size_t j = 0; j<hMean[i].size(); j++)
			errs()<<hMean[i][j]<<"\n";
	}
		
	createExtensions();
//	cout<<"const index size: "<<constIndices.size()<<endl;
	wMax.clear();
	wMax.resize(xtendedMean[0].size());
	std::vector<int> mwc;
	double fin, cpy;
	cpy = minNoise;

	// initializing wMax
	for(size_t i = 0; i<wMax.size(); i++)
	{
		if(constIndices.find(i) != constIndices.end())
			wMax[i] = CONSTWL;
		else
			wMax[i] = MAXOPWL;
	}
	errs()<<"minNoise: "<<minNoise<<"\n";
	errs()<<"getting MWC....\n";
	mwc = getMWC();
		for(size_t i = 0; i<mwc.size(); i++)
			errs()<<mwc[i]<<" ";
		errs()<<"\n";
		mwc = greedyAscent(mwc);
		errs()<<"WLs after ascent..."<<"\n";
		for(int i = 0; i<mwc.size();i++)
			errs()<<mwc[i]<<" ";
		errs()<<"\n";
//		errs()<<"SNR after algo 2: "<<calculateSNR(mwc)<<"\n";	

	mwc = tabuSearch(mwc);
//	errs()<<"WLs after tabu search..."<<"\n";
//		for(int i = 0; i<mwc.size();i++)
//			errs()<<"("<<i<<","<<mwc[i]<<") ";
//		errs()<<"\n";
	errs()<<"SNR after tabu search: "<<calculateSNR(mwc)<<"\n";	

	// returns optimal world length combination
	return mwc;
}

// set some essential params for triggering the workflow
void WLO::setParams(std::map<size_t,double> mulc,std::vector<size_t> regi,size_t nm,size_t nn)
{
	multConstants = mulc;
	regIndices = regi;
	numNodes = nn;
	numMults = nm;
}

} // end namespace legup
