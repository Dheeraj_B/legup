//.cpp -----------------------------------*- C++ -*-===//
//
// This file is distributed under the LegUp license. See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the data structures needed for scheduling.
//
//===----------------------------------------------------------------------===//

#include "FiniteStateMachine.h"
#include "SchedulerDAG.h"
#include "Scheduler.h"
#include "State.h"
#include "LegupPass.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/ErrorHandling.h"
#include "utils.h"
#include "LegupConfig.h"
#include "llvm/Pass.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "WLO.h"

#include<cstdio>
#include<memory>
using namespace llvm;
using namespace legup;

namespace legup {
//extern std::vector<std::vector<Instruction*> > aliasList;
extern std::map<Value*, Value*> aliasList; // call ac_int_add(%1,..) --> as (%1,call_ac_int_add)
extern std::map<std::string, Instruction*> aliasListbyName;
extern bool isInside(std::string parent, std::string sub);
extern std::vector<int> getMDString(Instruction* instr);
extern std::vector<int> extractTemplates(std::string fname);
extern std::map<Value*,int> opSize;
extern std::set<std::string> FunSet;
std::map<Function*, std::map<Value*,int> > opSizeLookup;	// LUT for a function and OWLs of values in it
std::map<std::string,MDNode*> MDataLookup;
std::map<Value*,std::string> constDict;	// constDict used in updating weights of multiplier nodes in SFG before triggering WLO
std::map<Value*,size_t> val2Num;	// LUT for SFG nodes to a unique integer val
std::map<size_t,Value*> num2Val;	// inverse of above LUT
int inpRamWidth = -1;	// stores OWL of input RAM. Assumption: only one global input array in the i/p src code
Value* inLoad = NULL;

std::pair<std::string,std::string> modifyNameToFixed(std::string fname)
{
	size_t b;
	b = fname.find("i");
	bool isOdd = true;
	std::string newfname = "";

	bool found = false;
	std::string yy;
	for(std::set<std::string>::iterator ib = FunSet.begin(), e = FunSet.end(); ib!=e; ++ib)
	{
		found = (fname.find(*ib) != std::string::npos);
		if(found)
		{
			yy = *ib;
			break;
		}
	}
	
	if(found) newfname += yy;
	newfname += "ac_int_";
	std::string metadata = "";
	std::string op = "";
	
	if(isInside(fname,"ops_with_other_types")) op = "_ops_with_other_types_";
	if(isInside(fname,"plus")) op += "_plus_";
	else if(isInside(fname,"minus")) op += "_minus_";
	else if(isInside(fname,"mult")) op += "_mult_";
	else if(isInside(fname,"EEERKS_IXT_EXT0")) op+="_EEERKS_IXT_EXT0_";
	
	errs()<<"fname: "<<fname<<"\n";
	
	while(b!=std::string::npos && b+1!=std::string::npos)
	{
		std::string num = "";
		errs()<<"@ "<<fname[b+1]<<"\n";
		while(fname[b+1]-48 >= 0 && fname[b+1]-48 <= 9)
		{
			num+=std::string(1,fname[b+1]);
			++b;
		}

		if(num!="")
		{
			if(!isOdd) metadata+=num + " ";
			else newfname+="ILi" + num + "ELb0";
			isOdd = !isOdd;
		}
		
		b = fname.find("i",b+1);
	}
	assert(newfname!="ac_int");
	assert(metadata!="");
	newfname+=op;
	if(isInside(fname,"EEC2Ei") || isInside(fname,"EEC2Ed")) newfname+="EEC2Ei";
	if(isInside(fname,"EEC2ERKS2_")) newfname+="EEC2ERKS2_";
	newfname+="_EE";
	std::pair<std::string,std::string> ret = std::make_pair(newfname,metadata);
	errs()<<"newfname: "<<newfname<<"\n";
	errs()<<"metadata: "<<metadata<<"\n";
	return ret;
}


// Add dependencies for all the operands of iNode
// ie. %3 = add %1, %2
// %3 is dependent on %1 and %2
// %1 is used by %3
// %2 is used by %3
void SchedulerDAG::regDataDeps(InstructionNode *iNode) {
    Instruction *inst = iNode->getInst();
    static InstructionNode *buf;
    // these instructions are not scheduled
    if (isa<AllocaInst>(inst) || isa<PHINode>(inst))
        return;
    errs()<<"computing data dependencies for: "<<*inst<<"\n";
    
    // create dependancies for return instruction
    // TODO: need to add dependancies from every instruction of the BB to return instruction
    if(isa<ReturnInst>(inst) && inst->getType()->getTypeID() == Type::VoidTyID)
    {
		Instruction *bufInst = buf->getInst();
		if(bufInst->getParent() != inst->getParent()) return;
		assert(buf);
		errs()<<"adding return dependancy to "<<*(buf->getInst())<<" for "<<*inst<<"\n";
        iNode->addDepInst(buf);
        buf->addUseInst(iNode);
		return;
	}
	
	if(isa<CallInst>(inst) && inst->getNumOperands() == 4 && inst->getOperand(3)->getName().str() == "legup_memcpy_4")
	{
		// adding dependancy from all preceeding instructions to legup_memcpy
		BasicBlock *b = inst->getParent();
		for (BasicBlock::iterator depb = b->begin(), ie = b->end(); depb != ie;
			 ++depb) {
			Instruction *I1 = depb;

			// If we reach inst then there are no more candidates for I1
			if (I1 == inst)
				break;

			if (!isa<LoadInst>(I1) && !isa<StoreInst>(I1) && !isa<CallInst>(I1))
				{errs()<<"skip 1\n";continue;}

			InstructionNode *I1Node = getInstructionNode(I1);
			iNode->addDepInst(I1Node);
			I1Node->addUseInst(iNode);
			errs()<<"added dependency for call\n";
			errs()<<"-->"<<*I1<<"\n";
		}
		aliasList[inst->getOperand(0)] = inst;
		Instruction *op = dyn_cast<Instruction>(inst->getOperand(0));
		assert(op);
		assert(isa<BitCastInst>(op));
		errs()<<"bitcast: "<<*op<<"\n";
		std::string iname = op->getOperand(0)->getName().str();
		// for calls with result as named vars
		if(iname != "")
		{
			errs() << "inserting "<<iname<<" into aliasListbyName\n";
			aliasListbyName[iname] = inst;
		}
		else
		{
			Instruction *t = dyn_cast<Instruction>(op->getOperand(0));
			assert(t);
			errs() << "inserting "<<*t<<" into aliasList\n";
			
			aliasList[op->getOperand(0)] = inst;			
		}
		return;
	}
	
	// these instrs are ignored
	if(isa<BitCastInst>(inst))
	{
		return;
	}
	
    int uu = 0;
    bool printed = false;
    // iterate through all the inputs to the call, ignoring the result instruction
    for (User::op_iterator i = inst->op_begin(), e = inst->op_end(); i != e;
         ++i) {
			 uu++;
        errs()<<"before dyncast: "<<(*i)->getName().str()<<"\n";
        // we only care about operands that are created by other instructions
        Instruction *dep = dyn_cast<Instruction>(*i);
        Instruction *prev;
		bool insert = false;
        // also ignore if the dependency is an alloca
		if(isa<CallInst>(inst) && (*i)->getType()->getTypeID() != Type::IntegerTyID) // hence function input args and integer constants are filtered out
		{
			int len = inst->getOperand(inst->getNumOperands()-1)->getName().str().size();
			len--;
			std::string temp = inst->getOperand(inst->getNumOperands()-1)->getName().str();
			bool found = false;
			for(std::set<std::string>::iterator ib = FunSet.begin(), e = FunSet.end(); ib!=e; ++ib)
			{
				found = (temp.find(*ib) != std::string::npos);
				if(found) break;
			}

			
			if(!printed) {errs()<<"inst string: "<<temp<<" size: "<<len<<" "<<temp[len-2]<<temp[len-1]<<temp[len]<<"\n";}
			// TODO: need to sort out fixed point calls by checking types of the arguments
			if((temp[len] == 'E' && temp[len-1] == 'E' && temp[len-2] == '_') || found
			|| isInside(temp,"ops_with_other_types") || isInside(temp,"EEC2Ei"))
			{
				
				//if(dyn_cast<ConstantInt>(*i)) continue; //skipping integer args
				
				// check for comparison calls
				bool cmp = isInside(temp,"ac_int") && 
				(isInside(temp,"ltILi") || isInside(temp,"gtILi") || isInside(temp,"eqILi") || isInside(temp,"neILi"));
				
				if(uu == 1 && !cmp)
				{
					aliasList[(*i)] = inst; // zeroth arg of a call
					continue;
				}
				
				if((*i)->getType()->getTypeID() != Type::IntegerTyID) // integer arguments dealt as usual at the end of code
				{
					// keeps track of last fixedpoint call
					// FIXME: fails if last call is a memcpy. hence write code so that there will be a bitadjust at the end
					buf = iNode;
					// update the dependancies by checking the aliasListbyName and aliasList
					if((*i)->getName().str() != "")
					{
						if(aliasListbyName.find((*i)->getName().str()) != aliasListbyName.end())
						{
							Instruction *depIn = aliasListbyName[(*i)->getName().str()];
							assert(depIn);
							if(depIn->getParent() == inst->getParent())
							{
								InstructionNode *depNode = getInstructionNode(depIn);
								iNode->addDepInst(depNode);
								errs()<<"adding dependent inst: "<<*(depNode->getInst())<<"\n";
								depNode->addUseInst(iNode);
							}
						}
					}
					if(aliasList.find(dep) != aliasList.end())
					{
						Instruction *depIn = dyn_cast<Instruction>(aliasList[dep]);
						if(depIn)
						{
							if(depIn->getParent() != inst->getParent()) continue;
						
							InstructionNode *depNode = getInstructionNode(depIn);
							iNode->addDepInst(depNode);
							errs()<<"adding dependent inst: "<<*(depNode->getInst())<<"\n";
							depNode->addUseInst(iNode);
						}
					}
				}
			}
			
		}
        if (!dep || isa<AllocaInst>(dep))
            {errs()<<"skip a"<<"\n";continue;}
        // ignore operands from other basic blocks, these are
        // guaranteed to be in another state
        if (dep->getParent() != inst->getParent())
            {errs()<<"skip b"<<"\n";continue;}

        InstructionNode *depNode = getInstructionNode(dep);
        iNode->addDepInst(depNode);
        errs()<<"adding dependent inst: "<<*(depNode->getInst())<<"\n";
        depNode->addUseInst(iNode);
        buf = iNode;
    }
    errs()<<"loop ctr = "<<uu<<"\n";
}

// returns true if there is a dependency from I1 -> I2
// based on alias analysis
bool SchedulerDAG::hasDependencyAA(Instruction *I1, Instruction *I2) {
    AliasAnalysis::Location Loc1, Loc2;

    if (isa<CallInst>(I1)) {
        // assume that any loads/stores after a call must indeed execute
        // AFTER the call
        return true;
    }

    // bool store = false;
    if (LoadInst *lInst = dyn_cast<LoadInst>(I1)) {
        Loc1 = AliasA->getLocation(lInst);
    } else if (StoreInst *sInst = dyn_cast<StoreInst>(I1)) {
        Loc1 = AliasA->getLocation(sInst);
        // store = true;
    } else {
        assert(0 && "Unexpected input");
    }

    if (isa<StoreInst>(I1) && isa<LoadInst>(I2)) {
        // RAW dependency:
        // I1 is a store and I2 is a load from potentially the same address
        LoadInst *lInst = dyn_cast<LoadInst>(I2);
        Loc2 = AliasA->getLocation(lInst);
        if (!AliasA->isNoAlias(Loc1, Loc2)) {
            return true;
        }

    } else if (isa<StoreInst>(I2)) {
        // WAW or WAR dependency:
        // I1 is a store OR a load and I2 is a store to
        // potentially same address
        StoreInst *sInst = dyn_cast<StoreInst>(I2);
        Loc2 = AliasA->getLocation(sInst);
        if (!AliasA->isNoAlias(Loc1, Loc2)) {
            return true;
        }
    } else {
        // RAR: no dependency
        assert(isa<LoadInst>(I1) && isa<LoadInst>(I2));
    }

    return false;
}

// find all memory dependencies: I1 -> I2 (where I2 is given)
void SchedulerDAG::memDataDeps(InstructionNode *I2Node) {
    Instruction *I2 = I2Node->getInst();
    BasicBlock *bb = I2->getParent();
	errs()<<"In SchedulerDAG::memDataDeps\n";
	
    // loop over all candidates for I1 in the BB for dependencies I1 -> I2
    for (BasicBlock::iterator dep = bb->begin(), ie = bb->end(); dep != ie;
         ++dep) {
        Instruction *I1 = dep;

        // If we reach I2 then there are no more candidates for I1
        if (I1 == I2)
            break;

        if (!isa<LoadInst>(I1) && !isa<StoreInst>(I1) && !isa<CallInst>(I1))
            {errs()<<"skip 1\n";continue;}

        if (isaDummyCall(I1))
            {errs()<<"skip 2\n";continue;}

        if (hasDependencyAA(I1, I2)) {
             errs() << "memDataDeps: I1 -> I2\n" <<
                "I1: " << *I1 << "\n" <<
                "I2: " << *I2 << "\n";
            InstructionNode *I1Node = getInstructionNode(I1);
            I2Node->addMemDepInst(I1Node);
            I1Node->addMemUseInst(I2Node);
        }
    }
}

// find all memory dependencies: I1 -> I2 (where I2 is given)
void SchedulerDAG::callDataDeps(InstructionNode *I2Node) {
    Instruction *I2 = I2Node->getInst();
    BasicBlock *b = I2->getParent();
	
	errs()<<"In SchedulerDAG::callDataDeps "<<*I2<<"\n";
	if(isa<CallInst>(I2) && I2->getOperand(I2->getNumOperands()-1)->getName().str().find("legup_memcpy_4") != std::string::npos)
		return;
    
    if (isaDummyCall(I2) && !isaPrintCall(I2))
        return;

    RAM *localMem = NULL;
    if (LEGUP_CONFIG->getParameterInt("LOCAL_RAMS") &&
        (isa<LoadInst>(I2) || isa<StoreInst>(I2))) {
        assert(alloc);
        localMem = alloc->getLocalRamFromInst(I2);
    }

    // loop over all candidates for I1 in the BB for dependencies I1 -> I2
    for (BasicBlock::iterator dep = b->begin(), ie = b->end(); dep != ie;
         ++dep) {
        Instruction *I1 = dep;

        // If we reach I2 then there are no more candidates for I1
        if (I1 == I2)
            break;

        if (!isa<LoadInst>(I1) && !isa<StoreInst>(I1) && !isa<CallInst>(I1))
            {errs()<<"skip 1\n";continue;}

        if (isaDummyCall(I1) && !isaPrintCall(I1))
            {errs()<<"skip 2\n";continue;}

        if (isa<LoadInst>(I1) || isa<StoreInst>(I1)) {
            RAM *depMem = alloc->getLocalRamFromInst(I1);
            if (localMem && depMem) {
                // only make them dependent if they point to the same memory
                if (localMem == depMem) {
                    if (LEGUP_CONFIG->getParameterInt("ALIAS_ANALYSIS")) {
                        if (!hasDependencyAA(I1, I2))
                            continue;
                    }
                    InstructionNode *I1Node = getInstructionNode(I1);
                    I2Node->addMemDepInst(I1Node);
                    I1Node->addMemUseInst(I2Node);
                }
                continue;
            }
        }

        InstructionNode *I1Node = getInstructionNode(I1);
        I2Node->addMemDepInst(I1Node);
        I1Node->addMemUseInst(I2Node);
        errs()<<"added dependency for call\n";
        errs()<<"-->"<<*I1<<"\n";
    }
}

SchedulerDAG::~SchedulerDAG() {
    errs()<<"in dag destructor\n";
    for (DenseMap<Instruction *, InstructionNode *>::iterator
             i = nodeLookup.begin(),
             e = nodeLookup.end();
         i != e; ++i) {
        if(i->second)
			delete i->second;
    }
}

// hasNoDelay - detect instructions which have no delay. For example a shift by
// constant will just be turned into a wire by Quartus
bool hasNoDelay(Instruction *instr) {
    if (instr->isShift()) {
        return (isa<ConstantInt>(instr->getOperand(1)));
    }
    if (isa<GetElementPtrInst>(instr)) {
        if (LEGUP_CONFIG->getParameterInt("DONT_CHAIN_GET_ELEM_PTR")) {
            return false;
        }
        for (unsigned i = 1; i < instr->getNumOperands(); i++) {
            if (!isa<ConstantInt>(instr->getOperand(i))) {
                return false;
            }
        }
        return true;
    }

    switch (instr->getOpcode()) {
    case (Instruction::And):
    case (Instruction::Or):
        return (isa<ConstantInt>(instr->getOperand(1)) ||
                getBitWidth(instr->getType()) == 1);
    }
    return (instr->isCast() || isa<PHINode>(instr) || isa<AllocaInst>(instr) ||
            instr->isTerminator() || isa<LoadInst>(instr) ||
            isa<CallInst>(instr));
}

void SchedulerDAG::updateDAGwithInst(Instruction *instr) {
    // generate Instruction to InstructionNode mapping
    InstructionNode *iNode = new InstructionNode(instr);
    nodeLookup[instr] = iNode;

	

    // set delay
    std::string opName = LEGUP_CONFIG->getOpNameFromInst(instr, alloc);
    
    //errs()<<"current inst: "<<*instr<<"\n";
    
    if(!opName.empty())
    {
		errs()<<opName<<"\n";
	}
    else errs()<<"opName is empty \n";
    
    if (opName.empty() || isMem(instr)) {
        if (isa<GetElementPtrInst>(instr)) {
            if (LEGUP_CONFIG->getParameterInt("DONT_CHAIN_GET_ELEM_PTR")) {
                iNode->setAtMaxDelay();
            } else if (hasNoDelay(instr)) {
                iNode->setDelay(0);
            } else {
                iNode->setAtMaxDelay();
            }
        } else if (hasNoDelay(instr)) {
            errs() << "In SchedulerDAG::updateDAGwithInst found no delay inst: " << *instr << "\n";
            if(isa<CallInst>(instr))
            {
				// this wont be entered anymore since legup_memcpy_4 is mapped to dummy_memcpy as operator name
				if(isInside(instr->getOperand(instr->getNumOperands()-1)->getName().str(),"legup_memcpy_4"))
				{
					iNode->setAtMaxDelay();
					return;
				}
			}
            iNode->setDelay(0);
        } else {
             errs() << "In SchedulerDAG::updateDAGwithInst Empty: " << *instr << "\n";
            // assert(hasNoDelay(instr));
            iNode->setAtMaxDelay();
        }
    } else {
        Operation *Op = LEGUP_CONFIG->getOperationRef(opName);
        assert(Op);
        float critDelay = Op->getCritDelay();
		errs()<<"critDelay= "<<critDelay<<"\n";
        if (critDelay > InstructionNode::getMaxDelay()) {
            // errs() << "Warning delay " << critDelay <<
            //    "ns is greater than clock constraint of " <<
            //    InstructionNode::getMaxDelay() << "ns for instruction: " <<
            //    *instr << "\n";
            iNode->setAtMaxDelay();
        } else if (isMul(instr) &&
                   LEGUP_CONFIG->getParameterInt("MULTIPLIER_NO_CHAIN")) {
            iNode->setAtMaxDelay();
        } else {
            iNode->setDelay(critDelay);
        }
    }
}

void SchedulerDAG::generateDependencies(Instruction *instr) {
    InstructionNode *iNode = getInstructionNode(instr);

    // generate dependencies
    regDataDeps(iNode);

    if (isa<LoadInst>(*instr) || isa<StoreInst>(*instr)) {
        if (LEGUP_CONFIG->getParameterInt("ALIAS_ANALYSIS")) {
            memDataDeps(
                iNode); // create dependencies based on LLVM alias analysis
        } else {
            callDataDeps(iNode); // create dependences in same order as in IR
                                 // [LegUp 1.0 & 2.0 functionality]
        }
    } else if (isa<CallInst>(instr)) {
        std::string fname = instr->getOperand(instr->getNumOperands()-1)->getName().str();
        std::string ref = "ac_int";
        std::size_t pos = fname.find(ref);
        // skip all ac_int calls because their dependancies has been considered in regDataDeps()
        if(pos == std::string::npos)
			callDataDeps(iNode);
		else
			errs()<<"skip callDataDeps for: "<<*instr<<"\n";
    }
}

bool SchedulerDAG::runOnFunction(Function &F, Allocation *_alloc) {
    assert(_alloc);
    alloc = _alloc;
    AliasAnalysis *AA = alloc->getAA();
    assert(AA);
    AliasA = AA;
	int inst_ctr = 100;
	
	Value* otherLoad = NULL;
	
	std::vector<Instruction*> instToDelete;
	std::queue<std::pair<Instruction*,Instruction*> > instToInsert;
	if(isInside(F.getName().str(),"ac_fixed"))
	{
		std::string temp = modifyNameToFixed(F.getName().str()).first;
		F.setName(temp);
	}
	std::string name = F.getName();

    // before scheduling we need to do some IR modifications
    // this is because since AC datatypes are not a fundamental datatype built into LLVM type system,
    // the operations involving them will be code gen as calls to library functions. In case of ac_fixed,
    // we change the name of the function calls to represent total WL and attach a metadata node 
    // to represent fractional WLs of the operands
    for (Function::iterator b = F.begin(), be = F.end(); (b != be); b++) {
		bool loadCreated = false;
        for (BasicBlock::iterator instr = b->begin(), ie = b->end();
             instr != ie; ++instr) {
			errs()<<"in modification: "<<*instr<<"\n";
            if(isa<BitCastInst>(instr))
            {
/*				bool push = false;
				for(std::map<Value*,std::vector<Value*> >::iterator st = lut.begin(), sp = lut.end();
					st != sp; ++st)
				{
					for(size_t j = 0; j<st->second.size(); j++)
					{
						if(st->second[j] == instr->getOperand(0))
						{
							errs()<<"bitcast equlizer\n";
							errs()<<" op0: "<<*(instr->getOperand(0))<<"\n";
							errs()<<" rep inst: "<<*(st->first)<<"\n";
							st->second.push_back(instr);
							push = true;
							break;
						}
					}
					if(push) break;
				}
				assert(push);*/
				continue;
			}
			if(isa<AllocaInst>(instr))
			{
				continue;
			}
			// handle legup_memcpy instructions
			if(isa<CallInst>(instr) && instr->getOperand(instr->getNumOperands()-1)->getName().str().find("legup_memcpy_4") != 
				std::string::npos)
			{
				bool found = false;
				Value* op0 = instr->getOperand(0);
				Value* op1 = instr->getOperand(1);
				
				// the if condition below is added because if you call stripPointerCasts on a load
				// inst, then it causes error.
				Instruction *i1 = dyn_cast<Instruction>(op1);
				if(i1)
				{
					if(isa<BitCastInst>(i1))
					{
						i1 = dyn_cast<Instruction>(i1->getOperand(0));
						if(i1 && isa<LoadInst>(i1)) continue;						
					}
				}
				
				std::vector<Value*> tmp;
				Value* rep;
				Value* v = op1->stripPointerCasts();
				//errs()<<" v: "<<*v<<" isGlobal: "<<isa<GlobalVariable>(v)<<"\n";
				
				// code to handle calls of form: legup_memcpy_4(i8* %3, %class.ac_int* dereferenceable(4) @InpA)
				// ie, seocnd argument is a global of non array type
				// we create:
				//	1) a load instruction to load global inpA
				//	2) a bitcast instruction to bitcast the global
				//	3) change the second argument of current legup_memcpy to the new bitcast
				if((isa<GlobalVariable>(v)))
				{
					errs()<<" v: "<<*v<<" isGlobal: "<<isa<GlobalVariable>(v)<<"\n";
					Instruction *bc = dyn_cast<BitCastInst>(op0);
					assert(bc);
					Value *eq = bc->getOperand(0);
					
					Instruction *load = new LoadInst(v,(eq->getName().str()+"_new_" + utostr(inst_ctr--)).c_str());
					errs()<<"new load: "<<*load<<"\n";
					//instToInsert[instr] = load;
					//instToDelete.push_back(instr);
					std::string oldName = eq->getName().str();
					
					
					//Instruction *bc1 = new BitCastInst(eq,Type::getInt8PtrTy(load->getContext()));
					std::string bcName = "BC_" + load->getName().str();
					Instruction *bc2 = bc->clone();
					bc2->setOperand(0,load);
					bc2->setName(bcName);
					errs()<<"new bitcast: "<<*bc2<<"\n";
					instr->setOperand(1,bc2);

					instToInsert.push(std::make_pair(instr,bc2));		// means bc2 must be inserted before instr
					instToInsert.push(std::make_pair(bc2,load));		// means load must be inseretd before bc2
					// the above insertions must be processed in FIFO mode. Hence instToInsert is declared as a queue
					
					// set metadata to new load to identify as a newly introduced instruction
					LLVMContext& C = load->getContext();
					MDNode* N = MDNode::get(C, MDString::get(C, "fixedPointLoad"));
					load->setMetadata("fixpLoad", N);
					//assert(0);
					
				}
			
				continue;
			}
			// handle call instructions
			// call instructions can have args of form:
			// _EXT0_EE(%class.ac_int.0* %xyb, %class.ac_int* dereferenceable(4) @InpB) 
			// global vars must be loaded before the call
			// create new load instructions and insert it before the call
			// modify the args of call inst appropriately
			if(isa<CallInst>(instr) && instr->getOperand(instr->getNumOperands()-1)->getName().str().find("legup_memcpy_4") == 
				std::string::npos && (isInside(instr->getOperand(instr->getNumOperands()-1)->getName().str(),"ac_int") 
				|| isInside(instr->getOperand(instr->getNumOperands()-1)->getName().str(),"ac_fixed")))
			{
				
				std::string temp = instr->getOperand(instr->getNumOperands()-1)->getName().str();
				
				// similar to code in leguppass.cpp
				//	the metadta string for ac_fixed binary op calls do not have integer WL of the result associated with it
				// 	they can be deduced from the operand integer WLs
				if(isInside(temp,"ac_fixed"))
				{
					std::pair<std::string,std::string> p = modifyNameToFixed(temp);
					temp = p.first;
					std::string metadata = p.second;
					instr->getOperand(instr->getNumOperands()-1)->setName(temp);
			
					
					LLVMContext& C = instr->getContext();
					MDNode* N = MDNode::get(C, MDString::get(C, metadata));
					instr->setMetadata("psuedo", N);		// psuedo ac_fixed instructions
					errs()<<"set metadata for:- "<<*instr<<"\n";
					MDataLookup[instr->getOperand(instr->getNumOperands()-1)->getName().str()] = N;					
					
				}
				else if(MDataLookup.find(temp) != MDataLookup.end())
				{
					instr->setMetadata("psuedo", MDataLookup[temp]);
					errs()<<"set metadata for:- "<<*instr<<"\n";
				}
				temp = instr->getOperand(instr->getNumOperands()-1)->getName().str();
				for(int j =1; j<instr->getNumOperands()-1;j++) //FIXME: what if op0 of a cmp call is global?
				{
					errs()<<"arg no.[0 based index] "<<j<<"\n";
					ConstantFP *Flp = dyn_cast<ConstantFP>(instr->getOperand(1));
					if(isInside(temp,"EEC2Ei") && Flp)
					{
						APFloat f = Flp->getValueAPF();
						const double val = f.convertToDouble();
						std::ostringstream strs;
						strs << val;
						constDict[instr->getOperand(0)] = strs.str();
						errs()<<"adding const: "<<*(instr->getOperand(0))<<" to dict as "<<strs.str()<<"\n";						
						break;
					}
					else if(isa<GlobalVariable>(instr->getOperand(j)))
					{
						errs()<<"found global var\n";
						// creation of a load instruction and insertion before the call
						Instruction* load = new LoadInst(instr->getOperand(j),"l"+utostr(inst_ctr--));
						errs()<<"new load: "<<*load<<"\n";
						instToInsert.push(std::make_pair(instr,load));
						// adjust the operands of the call instruction
						instr->setOperand(j,load);
						errs()<<*instr<<"\n";
					}
					// handle GEP instructions that load from a global array
					// craete a load before the call
					// replace the GEP argument in the call with new load
					// PS: GEP can appear as an argument to a call :)
					else
					{
							Value *v = instr->getOperand(j);
							Value *base = v->stripInBoundsConstantOffsets();
							if(isa<GlobalVariable>(base))
							{
								
								// this will create a load that fetches the correct index in the array
								Instruction *load = new LoadInst(v,"l"+utostr(inst_ctr--));
								errs()<<"new load: "<<*load<<"\n";
								instToInsert.push(std::make_pair(instr,load));
								instr->setOperand(j,load);
							}
							//continue;
					}
				}
				continue;
			}
			
			// GEP inst of AC type uses must have a new load insruction loading the data fetched by GEP 
			if(isa<GetElementPtrInst>(instr))
			{
				// ex: getelementptr inbounds [3 x %class.ac_fixed]* @inputstream, i32 0, i32 1
				StructType *STy;
				Value *op0;
				
				op0 = instr->getOperand(0);
				Type *T = op0->getType();
				// T --> [3 x %class.ac_fixed]*
				PointerType *PTy = dyn_cast<PointerType>(T);
				assert(PTy);

				Type *ETy = PTy->getElementType();
				// ETy --> [3 x %class.ac_fixed]
				
				errs()<<"op0: "<<*op0<<"\n";
				errs()<<*PTy<<"\n";
				 
				 ArrayType *ATy = dyn_cast<ArrayType>(ETy);
				 ETy = ATy->getElementType();
				 // ETy --> class.ac_fixed  and every such class is of LLVM struct type
				 STy = dyn_cast<StructType>(ETy);
				 if(STy) errs()<<"type: "<<STy->getName()<<"\n";
			
				// only consider ac_int and ac_fixed types for IR modifictaion
				if(isa<GlobalVariable>(op0) && STy && (STy->getName().str() == "class.ac_int" || STy->getName().str() == "class.ac_fixed"))
				{
					// creation of new load that loads the current GEP into a new variable
					Instruction *load = new LoadInst(instr,"l"+utostr(inst_ctr--));
					errs()<<"new load: "<<*load<<"\n";
					
					// assumption is that we consider filters with fanout atmost 2
					// the otherLoad is used to denote the shifting of the input to a regsiter in case of filters
					// the first load will be an input to an adder
					// the otherLoad node wont be generated in the SFG, instead will be merged with the node corresponding
					// to the first load. So inorder to fix the OWL of the 2nd load, we need to save the pointer to 2nd load
					// and at the end, OWLs of both loads are equalized. see the part of code where we set OWLs of instructions after WLO
					if(loadCreated) otherLoad = load;
					loadCreated = true;
					
					Instruction *curr = dyn_cast<Instruction>(instr++);
					Instruction *next = dyn_cast<Instruction>(instr--);
					// insert the new load just after the current GEP
					instToInsert.push(std::make_pair(next,load));					
					
					// replacing all the uses of gep with new load
					for(User *u : instr->users())
					{
						Instruction *Inst = dyn_cast<Instruction>(u);
						if(Inst && (isa<CallInst>(Inst) || isa<BitCastInst>(Inst)))
						{
							errs() << " is used in : ";
							errs()<< *Inst <<"\n";
							unsigned numArgs = Inst->getNumOperands();
							for(unsigned j = 0; j<numArgs; j++)
							{
								Instruction *argInst = dyn_cast<Instruction>(Inst->getOperand(j));
								if(argInst == curr)
								{
									Inst->setOperand(j,load);
									errs()<<"modified as: "<<*Inst<<"\n";
								}
							}
							
						}
					}
				}
			}
        }
    }
    
    errs()<<"processing instruction insertions\n";
    while(!instToInsert.empty())
	{
		std::pair<Instruction*,Instruction*> b = instToInsert.front();
		instToInsert.pop();
		errs()<<" first: "<<*(b.second)<<"\n";
		errs()<<" second: "<<*(b.first)<<"\n";
		assert(!(b.second->getParent()));
		// getParent() --> returns BB where the instr resides, getInstList() --> returns list if inst in that BB
		b.first->getParent()->getInstList().insert(b.first,b.second);
		// set metadata for new load instructions which are fixed point generated
		if(isa<LoadInst>(b.second))
		{
			Instruction *Inst = b.second;
			LLVMContext& C = Inst->getContext();
			MDNode* N = MDNode::get(C, MDString::get(C, "fixedPointLoad"));
			Inst->setMetadata("fixpLoad", N);		
			errs()<<"set metadata for:- "<<*Inst<<"\n";
			errs()<<"  "<<cast<MDString>(Inst->getMetadata("fixpLoad")->getOperand(0))->getString()<<"\n";	
		}
	}

	// can be deleted since we do no more deletions
	errs()<<"processing instruction deletions\n";
	for(int j = 0; j<instToDelete.size();j++)
	{
		errs()<<" candidate: "<<*(instToDelete[j])<<"\n";
		instToDelete[j]->eraseFromParent();
	}
	
	errs()<<"begin printing the modified IR for function: "<<F.getName()<<"\n";
    for (Function::iterator b = F.begin(), be = F.end(); (b != be); b++) {
        for (BasicBlock::iterator instr = b->begin(), ie = b->end();
             instr != ie; ++instr) {
            errs()<<*instr<<"\n";
        }
    }
	errs()<<"end printing the modified IR for function: "<<F.getName()<<"\n";

	bool found = false;
	for(std::set<std::string>::iterator ib = FunSet.begin(), e = FunSet.end(); ib!=e; ++ib)
	{
		found = (F.getName().str().find(*ib) != std::string::npos);
		if(found) break;
	}

	if(found && !(F.getName().str() == "main") && isInside(F.getName().str(),"WLO"))		// FIXME: the other functions are always assumed to be run by WLO
	{
		createSFG(F);
		//writeSimulationSSA();
		std::map<size_t,std::vector<size_t> > adjList;
		std::map<size_t,std::vector<size_t> > revadjList;
		std::map<size_t,std::string> weights;
		std::set<size_t> work;
		
		std::pair<size_t,size_t> sNt = createAdjLists(adjList, revadjList, weights, work);
		// IMP: from this point onwards I refer SFG to the graph represented by numbered adjacency list created by the above call
		errs()<<"printing adjlist------\n";
		for(std::map<size_t,std::vector<size_t> >::iterator ib = adjList.begin(), e = adjList.end(); ib!=e; ++ib)
		{
			errs()<<(ib->first)<<" : "<<*(num2Val[ib->first])<<"\n";
			for(size_t y = 0; y<(ib->second).size(); y++)
				errs()<<(ib->second)[y]<<" ";
			errs()<<"\t-> "<<weights[ib->first]<<"\n";
		}
		
		errs()<<"printing revadjlist------\n";
		for(std::map<size_t,std::vector<size_t> >::iterator ib = revadjList.begin(), e = revadjList.end(); ib!=e; ++ib)
		{
			errs()<<(ib->first)<<" : ";
			for(size_t y = 0; y<(ib->second).size(); y++)
				errs()<<(ib->second)[y]<<" ";
			errs()<<" -> "<<weights[ib->first]<<"\n";
		}
		WLO *wopt = new WLO();
		wopt->createGraph(adjList,revadjList,weights,work);
		std::map<size_t,double> multConstants;
		size_t numMults = 0;
		size_t numNodes = adjList.size()+1;
		std::vector<size_t> regIndices;
		
		// finding registers and multipliers in the SFG
		for(std::map<size_t,std::string>::iterator ib = weights.begin(), e = weights.end(); ib!=e; ++ib)
		{
			if(ib->second == "z")
			{
				regIndices.push_back(ib->first);
				continue;
			}

			if(ib->second == "1")
				continue;
			
			multConstants[ib->first] = std::stod(ib->second);
			numMults++;
		}
		wopt->setParams(multConstants,regIndices,numMults,numNodes);
		wopt->setMinNoise();
		std::vector<int> mwc = wopt->generateWordLengths(sNt.first,sNt.second);
		for(size_t y = 0; y<mwc.size(); y++)
			errs()<<"("<<y<<","<<mwc[y]<<") ";
		errs()<<"\n";
		
		delete wopt;
		
		//errs()<<"size of rootRegs: "<<rootRegs.size()<<"\n";
		//exec("g++ -o AccuracyConstraints/tabu -fopenmp AccuracyConstraints/script.cpp");
		
		errs()<<"tag values\n";
		std::map<Value*,int> tag = sfg->tag;
		for(std::map<Value*,int>::iterator ib = tag.begin(), e = tag.end(); ib!=e; ++ib)
			errs()<<*(ib->first)<<" : "<<(ib->second)<<"\n";
		errs()<<"num mults: "<<numMults<<"\n";
		// sets the OWL for different operators
		for(size_t i = 0; i<mwc.size()-numMults; i++)
		{
			Value *op = num2Val[i];
			errs()<<"mwc "<<*op<<"\n";
			assert(sfg->tag.find(op) != sfg->tag.end());
			int id_tag = sfg->tag[op];
			assert(sfg->connectedInstrs.find(id_tag) != sfg->connectedInstrs.end());
			std::vector<Value*> cont = sfg->connectedInstrs[id_tag];
			for(size_t y = 0; y < cont.size(); y++)
			{
				//assert(opSize.find(cont[y]) != opSize.end());
				opSize[cont[y]] = mwc[i];
				errs()<<"setting width of: "<<*cont[y]<<" as "<<mwc[i]<<"\n";
			}
		}
		errs()<<"set inpRamWidth as: "<<mwc[0]<<"\n";
		inpRamWidth = mwc[0];
		if(otherLoad)	// the secondary load case that shifts data to a register
		{
			errs()<<"set otherload: "<<*otherLoad<<" width as above\n";
			opSize[otherLoad] = mwc[0];
		}
		
		opSizeLookup[&F] = opSize;

	}
	else
	{
		opSize.clear();
		idList.clear();
		index2op.clear();
		rootRegs.clear();
	}
	
	
    //assert(0);
    for (Function::iterator b = F.begin(), be = F.end(); b != be; b++) {
        for (BasicBlock::iterator instr = b->begin(), ie = b->end();
             instr != ie; ++instr) {
            updateDAGwithInst(instr);
        }
    }
	


    for (Function::iterator b = F.begin(), be = F.end(); b != be; b++) {
        for (BasicBlock::iterator instr = b->begin(), ie = b->end();
             instr != ie; ++instr) {
            generateDependencies(instr);
        }
    }

    return false;
}

/// createStates - create new states for an FSM.
void createStates(unsigned begin, unsigned end,
                  std::map<unsigned, State *> &orderStates,
                  FiniteStateMachine *fsm) {
    assert(orderStates.find(begin - 1) != orderStates.end());
    for (; begin <= end; begin++) {
        orderStates[begin] = fsm->newState(orderStates[begin - 1]);
    }
}

/// setStateTransitions - determine the state transitions given a terminating
/// instruction
void setStateTransitions(State *lastState, const TerminatorInst *TI,
                         State *waitState,
                         std::map<BasicBlock *, State *> bbFirstState) {

    lastState->setTerminating(true);

    // unreachable could occur in a basic block like:
    // bb:                                               ; preds = %entry
    //   %2 = tail call i32 (i8*, ...)* @printf(i8* noalias getelementptr
    //    inbounds ([32 x i8]* @.str1, i32 0, i32 0)) nounwind ; <i32> [#uses=0]
    //   tail call void @exit(i32 1) noreturn nounwind
    //   unreachable
    if (isa<UnreachableInst>(TI) || TI->getOpcode() == Instruction::Ret) {
        lastState->setDefaultTransition(waitState);
        return;
    }

    lastState->setTransitionVariable(TI->getOperand(0));

    BasicBlock *Default;
    if (const SwitchInst *SI = dyn_cast<SwitchInst>(TI)) {

        for (unsigned i = 2, e = SI->getNumOperands(); i != e; i += 2) {
            Value *value = SI->getOperand(i);
            assert(value);
            BasicBlock *Succ = dyn_cast<BasicBlock>(SI->getOperand(i + 1));
            State *state = bbFirstState[Succ];

            lastState->addTransition(state, value);
        }

        Default = dyn_cast<BasicBlock>(SI->getDefaultDest());

    } else if (const BranchInst *B = dyn_cast<BranchInst>(TI)) {

        if (B->isConditional()) {
            Default = dyn_cast<BasicBlock>(TI->getSuccessor(1));

            BasicBlock *Succ = dyn_cast<BasicBlock>(B->getSuccessor(0));
            State *state = bbFirstState[Succ];

            lastState->addTransition(state);
        } else {
            Default = dyn_cast<BasicBlock>(TI->getSuccessor(0));
        }

    } else {
		errs()<<"Unreachable in FSM setStateTransitions()\n";
        llvm_unreachable(0);
    }

    State *state = bbFirstState[Default];
    lastState->setDefaultTransition(state);
}

/// createFSM - create a Finite State Machine object from the scheduler mapping.
FiniteStateMachine *SchedulerMapping::createFSM(Function *F,
                                                SchedulerDAG *dag) {
    // create FSM
    FiniteStateMachine *fsm = new FiniteStateMachine();

    // very first state
    State *waitState = fsm->newState();
    waitState->setDefaultTransition(waitState);

    // first state in each basic block
    std::map<BasicBlock *, State *> bbFirstState;
    // this map is used for labeling the states
    // each state name will have an index, from this map, as well as being
    // labeled with a function name, and a basic block name.
    std::map<BasicBlock *, unsigned> sCount;

    // create a FSM for the function where each basic block is assigned
    // an empty state as a placeholder
    unsigned bbNum = 0;
    for (Function::iterator b = F->begin(), be = F->end(); b != be; ++b) {
        if (isEmptyFirstBB(b))
            continue;

        State *s = fsm->newState();
        s->setBasicBlock(b);
        bbFirstState[b] = s;
        sCount[b] = 0;

        bbNum++;
    }

    // setup wait state transitions
    // setTransitionSignal(rtl->find("start"))
    // will be done later when we have access
    // to the RTL module
    Function::iterator firstBB = F->begin();
    if (BasicBlock *succ = isEmptyFirstBB(firstBB)) {
        // first BB was empty with just an unconditional branch,
        // so the waitstate should point to the next state, for instance:
        // BB_0:
        //   br label %BB_2
        // also need to set the basic block so phi's are still handled properly
        // for the empty BB
        waitState->setTerminating(true);
        waitState->setBasicBlock(firstBB);
        assert(bbFirstState.find(succ) != bbFirstState.end());
        waitState->addTransition(bbFirstState[succ]);
    } else {
        assert(bbFirstState.find(firstBB) != bbFirstState.end());
        waitState->addTransition(bbFirstState[firstBB]);
    }
	int bbCTr = 0;
	
    for (Function::iterator B = F->begin(), BE = F->end(); B != BE; ++B) {
        std::map<unsigned, State *> orderStates;
        if (isEmptyFirstBB(B))
            continue;
		
		bbCTr++;
		
        orderStates[0] = bbFirstState[B];
        unsigned lastState = getNumStates(B);

        // int pipelined = getMetadataInt(B->getTerminator(),
        // "legup.pipelined");

        // errs() << "BB: " << getLabel(B) << " lastState: " << lastState <<
        // "\n";
        createStates(1, lastState, orderStates, fsm);

        for (BasicBlock::iterator instr = B->begin(), ie = B->end();
             instr != ie; ++instr) {
            Instruction *I = instr;
            unsigned order = getState(dag->getInstructionNode(I));

            orderStates[order]->push_back(I);
            errs()<<"In FiniteStateMachine ::createFSM Inst: "<<*I<<" assigned in state "<<order<<" of BB "<<bbCTr<<" !might be changed later\n";

            // need to ensure multi-cycle instructions finish in the basic block
            unsigned delayState = Scheduler::getNumInstructionCycles(I);

            // Normally, loads take two cycles and the loaded values are stored
            // in shared memory controller output registers (port A or port B).
            //
            // In some flows however, we want each load to be stored in a
            // separate
            // register (e.g. to enable multi-cycle paths). But storing each
            // load
            // in a unique register and keeping the register on the output of
            // the
            // memory controller would make loads have a latency of 3, which is
            // not needed. Instead, when a separate register is created for each
            // load, make the FSM "think" that loads take 1 cycle (this is done
            // below). Then the second register is placed at the output of each
            // load (this is done in GenerateRTL.cpp, visitLoadInst);
            if (isa<LoadInst>(I) && LEGUP_CONFIG->duplicate_load_reg()) {
                delayState = 1; // Instead of normal 2 for loads
            }

            if (delayState == 0) {
                fsm->setEndState(I, orderStates[order]);
                continue;
            }

            delayState += order;
            if (delayState > lastState) {

                /*
                // assume iterative module scheduler has already handled
                // multi-cycle instructions
                // can't insert a new state - assume its ready in the first
                // state of next basic block
                if (pipelined) {
                    // this doesn't work for the kernel
                    assert(isa<LoadInst>(I));
                    //++B;
                    //assert(B != BE);
                    //fsm->setEndState(I, bbFirstState[B]);
                    //--B;

                    // all loads are assumed to be wires
                    fsm->setEndState(I, orderStates[order]);

                    continue;
                }
                */

                createStates(lastState + 1, delayState, orderStates, fsm);
                lastState = delayState;
            }

            fsm->setEndState(I, orderStates[delayState]);
        }

        setStateTransitions(orderStates[lastState], B->getTerminator(),
                            waitState, bbFirstState);
        orderStates[lastState]->setBasicBlock(B);

        for (unsigned i = 0; i < lastState; i++) {
            assert(orderStates.find(i) != orderStates.end());

            State *s = orderStates[i];
            s->setBasicBlock(B);
            s->setDefaultTransition(orderStates[i + 1]);
        }
    }

    FiniteStateMachine::iterator stateIter = fsm->begin();
    for (; stateIter != fsm->end(); stateIter++) {
        State *state = stateIter;
        if (!state->getBasicBlock()) {
            assert(state == waitState);
            continue;
        }

        sCount[state->getBasicBlock()] += 1;
        std::string newStateName = std::string("LEGUP_F_") +
                                   F->getName().str().data() + "_BB_" +
                                   getLabelStripped(state->getBasicBlock());

        stripInvalidCharacters(newStateName);
        state->setName(newStateName);
    }

    if (fsm->begin() != fsm->end())
        fsm->begin()->setName("LEGUP");

    return fsm;
}

void printNodeLabel(raw_ostream &out, InstructionNode *I) {
    out << *I->getInst();
}

// print a dot graph representing the dependency information (both normal and
// memory) for a basic block
void SchedulerDAG::printDFGDot(formatted_raw_ostream &out, BasicBlock *BB) {

    dotGraph<InstructionNode> graph(out, printNodeLabel);
    graph.setLabelLimit(40);

    bool ignoreDummyCalls =
        !LEGUP_CONFIG->getParameterInt("DFG_SHOW_DUMMY_CALLS");
    for (BasicBlock::iterator I = BB->begin(), ie = BB->end(); I != ie; ++I) {
        InstructionNode *op = getInstructionNode(I);
        if (ignoreDummyCalls && isaDummyCall(I))
            continue;

        std::string label = "label=\"D:" + ftostr(op->getDelay()) + "ns L:" +
                            utostr(Scheduler::getNumInstructionCycles(I)) +
                            "\",";
        for (Value::use_iterator use = I->use_begin(), e = I->use_end();
             use != e; ++use) {
            if (Instruction *child = dyn_cast<Instruction>(*use)) {
                if (ignoreDummyCalls && isaDummyCall(child))
                    continue;
                graph.connectDot(out, op, getInstructionNode(child),
                                 label + "color=blue");
            }
        }

        for (InstructionNode::iterator use = op->mem_use_begin(),
                                       e = op->mem_use_end();
             use != e; ++use) {
            if (ignoreDummyCalls && isaDummyCall((*use)->getInst()))
                continue;
            graph.connectDot(out, op, *use, label + "color=red");
        }
    }
}

void SFG::addEdge(Value* from, Value* to)
{
	adjList[from].first.insert(to);
	adjList[to].second.insert(from);
}

std::set<Value*>::iterator SFG::getInEdgeBegin(Value* vert)
{
	assert(adjList.find(vert) != adjList.end());
	return adjList[vert].second.begin();
}

std::set<Value*>::iterator SFG::getInEdgeEnd(Value* vert)
{
	assert(adjList.find(vert) != adjList.end());
	return adjList[vert].second.end();
}

std::set<Value*>::iterator SFG::getOutEdgeBegin(Value* vert)
{
	assert(adjList.find(vert) != adjList.end());
	return adjList[vert].first.begin();
}

std::set<Value*>::iterator SFG::getOutEdgeEnd(Value* vert)
{
	assert(adjList.find(vert) != adjList.end());
	return adjList[vert].first.end();
}

// clone oldNode to create a newNode, updates connectedInstrs correctly
void SFG::replaceNode(Value* oldNode, Value* newNode)
{
	assert(adjList.find(oldNode)!=adjList.end());
	std::pair<std::set<Value*>, std::set<Value*> > temp = adjList[oldNode];

	for(eit b = getInEdgeBegin(oldNode), e = getInEdgeEnd(oldNode); b!=e; ++b)
	{
		assert(adjList.find(*b) != adjList.end());
		adjList[*b].first.erase(oldNode);
		adjList[*b].first.insert(newNode);
		
	}
		
	
	for(eit b = getOutEdgeBegin(oldNode), e = getOutEdgeEnd(oldNode); b!=e; ++b)
	{
		assert(adjList.find(*b) != adjList.end());
		adjList[*b].second.erase(oldNode);
		adjList[*b].second.insert(newNode);
		
	}
	adjList.erase(oldNode);
	assert(adjList.find(newNode) == adjList.end());
	adjList[newNode] = temp;
	assert(tag.find(oldNode) != tag.end());
	int id_tag = tag[oldNode];
	tag[newNode] = id_tag;
	connectedInstrs[id_tag].push_back(newNode);
	opWidths[newNode] = -1;
	
	if(newNode->getName().str() != "")
		errs()<<"inserted new node: "<<newNode->getName()<<"\n";

	operatorType[newNode] = operatorType[oldNode];
	operatorType.erase(oldNode);
	errs()<<"operator type: " <<*(newNode)<<" as "<<operatorType[newNode]<<"\n";
	
	
}

size_t SFG::getInDegree(Value* vert)
{
	assert(adjList.find(vert) != adjList.end());
	return adjList[vert].second.size();
}

size_t SFG::getOutDegree(Value* vert)
{
	assert(adjList.find(vert) != adjList.end());
	return adjList[vert].first.size();
}

void SFG::addVertex(Value* op, Value* a, Value* b)
{

}


// call this function only while processing the add_integer function
void SchedulerDAG::createSFG(Function &F)
{
	sfg = new SFG();
	std::set<Value*> callDst;	// contains values that forms result of a function call of arithmetic operators and bitadjusts
	for (Function::iterator b = F.begin(), be = F.end(); (b != be); b++) {
		for (BasicBlock::iterator instr = b->begin(), ie = b->end();
			 instr != ie; ++instr) {
			
			// the instruction that this loop first consdiers will be a load because of the way IR is modified
			if(isa<LoadInst>(instr))	// inputs assumed to be a load inst
			{
				sfg->opWidths[instr] = -1; 
				sfg->tag[instr] = sfg->tagCtr;
				sfg->connectedInstrs[sfg->tagCtr++].push_back(instr);				
				continue;
			}

			if(!isa<CallInst>(instr)) continue;
			
			std::string iname = instr->getOperand(instr->getNumOperands()-1)->getName().str();
			
			errs()<<"current sfg inst: "<<*instr<<"\n";
			
			// FIXME: no subs expected in the cpp code. Otherwise transfer function computation
			// wont work. This can be fixed by modifying the code in WLO engine
			if(isInside(iname,"plus") || isInside(iname,"minus") || isInside(iname,"mult"))
			{
				// adding edge from operands to the output
				sfg->addEdge(instr->getOperand(1),instr->getOperand(0));
				sfg->addEdge(instr->getOperand(2),instr->getOperand(0));
				
				// finding the primary input load that goes to an adder
				if(!inLoad)			// assumes all shift operations to be done after the computations takes place
				{
					Instruction *i1 = dyn_cast<Instruction>(instr->getOperand(1));
					Instruction *i2 = dyn_cast<Instruction>(instr->getOperand(2));
				
					if(i1 && isa<LoadInst>(i1))
						inLoad = instr->getOperand(1);
					else if(i1 && isa<LoadInst>(i2))
						inLoad = instr->getOperand(2);
				}
				
				sfg->opWidths[instr->getOperand(0)] = -1; // will be changed later
				sfg->tag[instr->getOperand(0)] = sfg->tagCtr;
				sfg->connectedInstrs[sfg->tagCtr++].push_back(instr->getOperand(0));
				
				if(isInside(iname,"plus"))
				{
					sfg->operatorType[instr->getOperand(0)] = 0;
				}
				else if(isInside(iname,"minus"))
					sfg->operatorType[instr->getOperand(0)] = 1;
				else
					sfg->operatorType[instr->getOperand(0)] = 2;					
				errs()<<"operator type: " <<*(instr->getOperand(0))<<" as "<<sfg->operatorType[instr->getOperand(0)]<<"\n";
				callDst.insert(instr->getOperand(0));
				std::vector<int> WL = extractTemplates(iname);
				std::vector<int> IL = getMDString(instr);
				// setting theoretical fractional widths
				if(isInside(iname,"mult"))
					sfg->operatorWidth[instr->getOperand(0)] = WL[0] + WL[1] - IL[0] - IL[1];
				else
					sfg->operatorWidth[instr->getOperand(0)] = max(WL[0]-IL[0],WL[1]-IL[1]);
				sfg->operatorWidth[instr->getOperand(1)] = WL[0] - IL[0];
				sfg->operatorWidth[instr->getOperand(2)] = WL[1] - IL[1];
			}
			else if(isInside(iname,"legup_memcpy"))
			{
				// get the 2nd operand of memcpy and fetch that bitcast's operand
				Instruction *BC1 = dyn_cast<Instruction>(instr->getOperand(0));
				Instruction *BC2 = dyn_cast<Instruction>(instr->getOperand(1));
				assert(BC1 && BC2);
				Value *op1 = BC1->getOperand(0);
				Value *op2 = BC2->getOperand(0);
				
				// now there are 2 decisions. Either aliasing and thus replacing or adding a register
				if(callDst.find(op2) != callDst.end()) // case 1
				{
					sfg->replaceNode(op2,op1);
				}
				// case where we detect a register
				else
				{
					// to handle shifting from an input to a reg
					// assumes input and the reg has same datatypes to avoid extra bitcast
					// BC2 in this case has the form bitcast %scvgep to i8*
					Instruction * in_check = dyn_cast<Instruction>(op2);
					if(in_check && (isa<GetElementPtrInst>(in_check) || isa<LoadInst>(in_check)))
					{
						assert(inLoad);
						// treat op2 as primary load
						op2 = inLoad;
					}
					sfg->addEdge(op2,op1);
					sfg->operatorType[op1] = 3;
					sfg->opWidths[op1] = -1;
					

					sfg->tag[op1] = sfg->tagCtr;
					sfg->connectedInstrs[sfg->tagCtr++].push_back(op1);
					errs()<<"operator type: " <<*(op1)<<" as "<<sfg->operatorType[op1]<<"\n";
					// regs are always named variables in the input cpp
					assert(op1->getName().str() != "");
					//sfg->nodeLabels[op1] = op1->getName().str();
				}
				assert(sfg->operatorWidth.find(op2) != sfg->operatorWidth.end());
				// regs and aliases has same width as its drivers or node it clones respectively
				sfg->operatorWidth[op1] = sfg->operatorWidth[op2];
			}
			// when we reach a bitadjust, we replace the node getting casted by node it getting casted into
			else if(isInside(iname,"EERKS_IXT_EXT"))
			{
				Value *op0 = instr->getOperand(0);
				Value *op1 = instr->getOperand(1);
				sfg->replaceNode(op1,op0);
				callDst.insert(op0);
				assert(sfg->operatorWidth.find(op1) != sfg->operatorWidth.end());
				sfg->operatorWidth[op0] = sfg->operatorWidth[op1];
			}
			// handle inits. This will also set the fractional widths of register initializations. But those will
			// be eventually overwrited. It sets the widths for multiplier constants too
			else if(isInside(iname,"EEC2Ei") && instr->getMetadata("psuedo"))
			{
				Value *op0 = instr->getOperand(0);
				errs()<<"setting fractional width for named variable "<<*op0<<"\n";
				int w,iw;
				w = extractTemplates(iname)[0];
				iw = getMDString(instr)[0];
				sfg->opWidths[op0] = w - iw;
			}
		}
	}

	// crtical in finalizing the types of nodes. DO NOT DELETE
	errs()<<"printing sfg connectivity info\n";
	for(std::map<Value*, std::pair<std::set<Value*>, std::set<Value*> > >::iterator
	b = sfg->adjList.begin(), e = sfg->adjList.end(); b!=e; ++b)
	{
		if(b->first->getName().str() != "")
		{
			errs()<<"node: "<<b->first->getName()<<"\n";
			opSize[b->first] = 32; // CONSTOPWL
		}
		else
			errs()<<"node: "<<*(b->first)<<"\n";
		if(sfg->operatorType.find(b->first) != sfg->operatorType.end())
			errs()<<"type of: "<<(*b->first)<<" "<<sfg->operatorType[b->first]<<"\n";
		else
		{
			sfg->operatorType[b->first] = -1;
			errs()<<"added type as -1\n";
		}
			
		
		errs()<<"width: "<<sfg->operatorWidth[b->first]<<"\n";
	}
	//assert(0);
}

void SchedulerDAG::writeSimulationSSA()
{
	ssa.open("AccuracyConstraints/ssa");
	
	std::queue<Value*> L;
	typedef std::map<Value*, std::pair<std::set<Value*>, std::set<Value*> > >::iterator nit;
	std::map<Value*,int> visitCount;
	int regCtr = 0;
	int addCtr = 0;
	int mulCtr = 0;
	typedef std::set<Value*>::iterator eit;
	Value *primaryInput;
	const int offset = 4;
	out<<"float iir(float l100, std::vector<int> WL)\n";
	out<<"{\n";
	out<<"\tfloat a1 = -1.7103424072265625;\n"	;			//FIXME: need to get this from input cpp
	out<<"\tfloat a2 = 0.7472686767578125;\n";
	out<<"\tfloat b0 = 0.0092315673828125;\n";
	out<<"\tfloat b1 = 0.018463134765625;\n";
	out<<"\tfloat b2 = 0.0092315673828125;\n";

	for(nit b = sfg->adjList.begin(), e = sfg->adjList.end(); b!=e; ++b)
	{
		if(sfg->operatorType[b->first] == 3)
		{
			std::set<Value*> inputs = sfg->adjList[b->first].second;
			assert(inputs.size()==1);
			for(eit ib = inputs.begin(), ie = inputs.end(); ib!=ie; ++ib)
			{
				if(sfg->operatorType[*ib] != 3)
				{
					errs()<<"inserting "<<*(b->first)<<" to rootRegs\n";
					rootRegs.insert(b->first);
					srcRegs.insert(*ib);
				}
			}
			L.push(b->first);
			visitCount[b->first] = 1;
			idList[b->first] = "reg"+utostr(regCtr++);
			out<<"\tstatic float "<<idList[b->first]<<" = 0;\n";
			continue;
		}
		
		Instruction *Inst = dyn_cast<Instruction>(b->first);
		if(Inst && isa<LoadInst>(Inst)) // filter the inputs
		{
			errs()<<"detected load: "<<*Inst<<"\n";
			sfg->opWidths[b->first] = 16; // FIXME: this is not generalized
			std::set<Value*> outEdges = sfg->adjList[b->first].first;
			for(eit eb = outEdges.begin(), en = outEdges.end(); eb!=en; ++eb)
			{
				errs()<<"setting visit count of: "<<**eb<<"\n";
				primaryInput = *eb;
				visitCount[*eb] = 1;
			}
		}
		if(sfg->operatorType[b->first] == -1)
		{
			std::string u = b->first->getName().str();
			if(u != "")
				idList[b->first] = u;
		}
	}
	
	std::string outputId;
	std::map<Value*,int> WLIndex;
	int wli = 0;
	std::string costEqn;
	bool start = true;
	
	
	
	
	while(!L.empty())
	{
		Value *op = L.front(); // op can only be reg/add/sub/mul
		L.pop();
		errs()<<"consider: "<<*op<<" "<<sfg->operatorType[op]<<"\n";
		if(sfg->operatorType[op] == 0 || sfg->operatorType[op] == 1)
		{
			if(visitCount[op] != 2)
			{
				L.push(op);
				continue;
			}
		}
		
		// completed adders,subs
		// muls and regs
		for(eit b = sfg->adjList[op].first.begin() ,e = sfg->adjList[op].first.end(); b!=e; ++b)
		{
			Value *outE = *b;
			if(sfg->operatorType[outE] == 3) 
			{
				continue;
			}
			
			errs()<<"visiting: "<<*outE<<" "<<sfg->operatorType[outE]<<"\n";
			
			if(visitCount.find(outE) == visitCount.end())
				visitCount[outE] = 1;
			else
				visitCount[outE]++;
			
			if((sfg->operatorType[outE] != 3 && visitCount[outE] == 1) || outE == primaryInput) L.push(outE); // push only once
			
			
		}
		// writing instructions
		if(sfg->operatorType[op] == 3) continue;
		std::vector<std::string> opList;
		errs()<<"writing "<<*op<<" "<<sfg->operatorType[op]<<"\n";
		if(!(sfg->operatorType[op] == 1 || sfg->operatorType[op] == 0))
		{
			for(eit b = sfg->adjList[op].second.begin() ,e = sfg->adjList[op].second.end(); b!=e; ++b)
			{
				Value *in = *b;
				errs()<<"\tinp name "<<*in<<" "<<sfg->operatorType[in]<<"\n";
				assert(idList.find(in) != idList.end());
				opList.push_back(idList[in]);
			}
			
		}
		
		std::string id;
		
		int ty = sfg->operatorType[op];
		if(ty == 0)
		{
			id = "A"+utostr(addCtr++);
			ssa<<id<<" = "<<opList[0]<<" + "<<opList[1]<<";\n";
			out<<"\tfloat "<<id<<" = "<<opList[0]<<" + "<<opList[1]<<";\n";
			if(start)
			{
				start = false;
				costEqn+="WL["+utostr(offset + wli)+"]";
			}
			else
			{
				costEqn+="+WL["+utostr(offset + wli)+"]";
			}
		}
		else if(ty == 1)
		{
			id = "A"+utostr(addCtr++);
			ssa<<id<<" = "<<opList[0]<<" - "<<opList[1]<<";\n";
			out<<"\tfloat "<<id<<" = "<<opList[0]<<" - "<<opList[1]<<";\n";
			if(start)
			{
				start = false;
				costEqn+="WL["+utostr(offset + wli)+"]";
			}
			else
			{
				costEqn+="+WL["+utostr(offset + wli)+"]";
			}
		}
		else if(ty == 2)
		{
			id = "M"+utostr(mulCtr++);
			ssa<<id<<" = "<<opList[0]<<" * "<<opList[1]<<";\n";
			out<<"\tfloat "<<id<<" = "<<opList[0]<<" * "<<opList[1]<<";\n";
			if(start)
			{
				start = false;
				costEqn+="WL["+utostr(offset + wli)+"]*WL["+utostr(offset + wli)+"]";
			}
			else
			{
				costEqn+="+WL["+utostr(offset + wli)+"]*WL["+utostr(offset + wli)+"]";
			}
		}
		out<<"\t"<<id<<" = "<<"get_qauntized_signal("<<id<<", WL["<<offset + wli<<"]);\n";
		
		idList[op] = id;
		if(op->getName().str()=="agg.result")
			outputId = id;
			
		WLIndex[op] = wli;
		index2op[wli++] = op;
	}
	
	for(std::set<Value*>::iterator ib = rootRegs.begin(), ie = rootRegs.end(); ib!=ie; ++ib)
		writeShiftSeq(*ib);
	
	ssa<<"return "<<outputId<<";\n";
	ssa.close();

	out<<"\treturn "<<outputId<<";\n";
	out<<"}\n";
	out<<"int getCost(std::vector<int> WL)\n";
	out<<"{\n";
	out<<"\treturn "<<costEqn<<";\n";
	out<<"}\n";
	
	std::ifstream infile;
	infile.open("AccuracyConstraints/simulate.cpp");
	std::string line;
	std::ofstream outfile;
	outfile.open("AccuracyConstraints/script.cpp");
	outfile<<"#define NUMOPS "<<wli<<"\n";
	outfile<<"int wMax[] = {16,16,16,16,";
	for(int i = 0; i<wli; i++)
		outfile<<"32,";
	outfile<<"};\n";
	for(line;std::getline(infile,line);)
	{
		outfile<<line<<"\n";
	}
	infile.close();
	outfile<<out.str();
	outfile.close();
}

// annotate SFG nodes with integer values, so as to give input to Johnson's algo for transfer func computation
// creates adjacency list based on outgoing edges, reverese adjacency list cased on incoming edges, 
// weigths of edges viz: 1) "1" for adder 2) str(multiplier_const) for multipliers 3) "z" for register nodes
// creates a working set of nodes which is set of active SFG vertices
// returns input node and output node numbers as a pair
std::pair<size_t,size_t> SchedulerDAG::createAdjLists(
std::map<size_t,std::vector<size_t> >& adjList,
std::map<size_t,std::vector<size_t> >& revadjList,
std::map<size_t,std::string>& weights,
std::set<size_t>& work)
{
	size_t src,dst;
	val2Num.clear();
	num2Val.clear();
	typedef std::map<Value*, std::pair<std::set<Value*>, std::set<Value*> > >::iterator nit;
	typedef std::set<Value*>::iterator eit;
	size_t vIndex = 1; // so that input is indexed as zero
	for(nit b = sfg->adjList.begin(), e = sfg->adjList.end(); b!=e; ++b)
	{
		Instruction *Inst = dyn_cast<Instruction>(b->first);
		if(sfg->operatorType[b->first] == -1 && !(Inst && isa<LoadInst>(Inst))) continue;
		
		errs()<<"inspect: "<<*(b->first)<<"\n";
		size_t index;
		if(Inst && isa<LoadInst>(Inst))
		{
			index = 0;
			val2Num[b->first] = 0;
			num2Val[0] = b->first;
		}
		else if(val2Num.find(b->first) != val2Num.end())
			index = val2Num[b->first];
		else
		{
			index = vIndex;
			val2Num[b->first] = vIndex;
			num2Val[vIndex++] = b->first;
		}
		work.insert(index);
		
		std::set<Value*> outputs = sfg->adjList[b->first].first;
		cout<<"out degree: "<<outputs.size()<<"\n";
		if(outputs.size() == 0) dst = index;
		if(sfg->adjList[b->first].second.size() == 0) src = index;
//		std::vector<size_t> nullVector;
//		adjList[index] = nullVector;
		for(eit ib = outputs.begin(), ie = outputs.end(); ib!=ie; ++ib)
		{
			size_t e;
			if(val2Num.find(*ib) != val2Num.end())
				e = val2Num[*ib];
			else
			{
				e = vIndex;
				val2Num[*ib] = vIndex;
				num2Val[vIndex++] = *ib;
			}
			errs()<<"\tedge to: "<<*(*ib)<<"\n";
			adjList[index].push_back(e);
			revadjList[e].push_back(index);
		}
		
		assert(sfg->operatorType[b->first] != 1);
		// register weight update
		if(sfg->operatorType[b->first] == 3)
		{
			weights[index] = "z";
		}
		// adder weight update
		else if(sfg->operatorType[b->first] == 0)
		{
			weights[index] = "1";
		}
		// multiplier weight update
		else if(sfg->operatorType[b->first] == 2)
		{
			// getting the value of multiplier constant from incoming edges
			for(eit ib = sfg->adjList[b->first].second.begin() ,ie = sfg->adjList[b->first].second.end(); ib!=ie; ++ib)
			{
				// if current input node is a constant
				if(constDict.find(*ib) != constDict.end())
				{
					weights[index] = constDict[*ib];
					break;
				}
			}
		}
		else 	// input handling
		{
			assert(Inst && isa<LoadInst>(Inst));
			weights[index] = "1";
//			std::vector<size_t> nullVector;
//			revadjList[index] = nullVector;					
		}
	}
	errs()<<"src: "<<src<<" dst: "<<dst<<"\n";
	return std::make_pair(src,dst);
}

void SchedulerDAG::writeShiftSeq(Value *reg)
{
	typedef std::set<Value*>::iterator eit;
	std::set<Value*> outE = sfg->adjList[reg].first;
	for(eit r = outE.begin(), e = outE.end(); r!=e; ++r)
	{
		if(sfg->operatorType[*r] == 3)
			writeShiftSeq(*r);
	}
	std::set<Value*> in = sfg->adjList[reg].second;
	assert(in.size()==1);
	for(eit r = in.begin(), e = in.end(); r!=e; ++r)
	{
		ssa<<idList[reg]<<" = "<<idList[*r]<<";\n";
		out<<"\t"<<idList[reg]<<" = "<<idList[*r]<<";\n";
	}
}

void SchedulerDAG::fixRegWidths(Value *reg, int f)
{
	typedef std::set<Value*>::iterator eit;
	std::set<Value*> outE = sfg->adjList[reg].first;
	for(eit r = outE.begin(), e = outE.end(); r!=e; ++r)
	{
		if(sfg->operatorType[*r] == 3)
			fixRegWidths(*r,f);
	}
	errs()<<"set width for reg: "<<*reg<<" as "<<f<<"\n";
	opSize[reg] = f;
}

} // End legup namespace

