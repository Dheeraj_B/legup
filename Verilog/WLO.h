/************************************************************************************************************************************
 * header file for implmenting Word Length Optimization (WLO) engine
 * For determining analytical noise power expression, implements concepts from the following references:
 * 		1) Analytical Fixed-Point Accuracy Evaluation in Linear Time-Invariant Systems 
 * 				- D. Menard ; IRISA/INRIA Lab., Rennes I Univ., Lannion ; R. Rocher ; O. Sentieys
 * 				- IEEE Transactions on Circuits and Systems I: Regular Papers  (Volume:55 ,  Issue: 10 ) , Nov 2008
 * 				- http://ieeexplore.ieee.org/xpl/abstractAuthors.jsp?tp=&arnumber=4490316&url=http%3A%2F%2Fieeexplore.ieee.org%2Fiel5%2F8919%2F4358591%2F04490316.pdf%3Farnumber%3D4490316
 *		2) Finding All the Elementary Circuits of a Directed Graph
 * 				- Donald B. Johnson
 * 				- SIAM Journal on Computing 4, no. 1, 77-84, 1975.
 * 				- http://dx.doi.org/10.1137/0204007
 * 		3) Electronic circuits, signals, and systems [by] Samuel J. Mason [and] Henry J. Zimmermann. [book]
 * 				- chapter 4 , subsections 7 and 8 (pages 100 to 105)
 * 				- http://babel.hathitrust.org/cgi/pt?id=mdp.39015000971252;view=1up;seq=20
 * For doing WLO the following reference is used:
 * 		4) High-Level Synthesis under Fixed-Point Accuracy Constraint
 * 				- Daniel Menard, Nicolas Herve, Olivier Sentieys, and Hai-Nam Nguyen
 * 				- Journal of Electrical and Computer Engineering Volume 2012 (2012), Article ID 906350, 14 pages, Jan 2012.
 * 				- http://dx.doi.org/10.1155/2012/906350
 * For finding Strongly connected components in a directed Graph
 * 		5) Algorithms [by] S. Dasgupta, C. H. Papadimitriou, [and] U. V. Vazirani [book]
 * 				- chapter 3 , subsection 4
 * ***********************************************************************************************************************************/
#ifndef LEGUP_WLO_H
#define LEGUP_WLO_H

#include<iostream>
#include<set>
#include<map>
#include<vector>
#include<cassert>
#include<stack>
#include<string>
#include<algorithm>
#include<fstream>
#include<cstdio>
#include<memory>
#include<sstream>
#include<utility>
#include<cmath>
#include<iomanip>


#define MINOPWL 1		// Minimum operator WL
#define MAXOPWL 64		// Maximum operator WL
#define CONSTWL 32		// Multiplier constant WL assumed to be fixed

// we are trying to compute the optimum wordlength of each operator
// let w_opt denote the vector where w_opt[i] has the optimum WL for the ith node
// w_opt also contains WLs of multiplier constants also

namespace legup {
	
class WLO
{
	
private:
	bool tr = false;
	double minNoise;										// noise constraint, represents maximum noise allowed as constrainted.
															// name minNoise is a misnomer :(
	//double minSNR = 75;
	std::ofstream Out;										
	size_t clk;												// clk for performing DFS (refer #5)
	size_t pctr;											// to enumerate paths
	size_t lctr;											// to enumerate loops
	size_t numNodes;										// number of nodes in the graph
	std::map<size_t,std::vector<size_t> > adjList;			// adjacency list based on outgoing edges
	std::map<size_t,std::vector<size_t> > revadjList;		// adjacency list based on incoming edges
	std::map<size_t,std::vector<size_t> > adjList_cpy;
	std::map<size_t,std::vector<size_t> > revadjList_cpy;
	std::set<size_t> constIndices;							// indices in w_opt that stores WL of a constant
	std::set<size_t> toReset;								//  see selectLiveLoops()	

	std::set<size_t> visited;								// visited vertices
	std::map<size_t,size_t,std::greater<size_t> >post2v;	// LUT of postvisit number to vertex [reference #5]
	std::map<size_t,size_t> v2post;							// LUT of vertex to postvisit number
	std::set<size_t> work;									// currently active vertices for various iterations of johnson's cicruit routine
	std::set<size_t> deletedVertices;						// vertices removed by johnson's algo progressively
	std::map<size_t,std::string> weights;					// weights of each operator; see schedularDAG.cpp, SFG generation
	std::vector<size_t> pathStack;							// stores paths from input(src) to output(dst)
	std::map<size_t,bool> visitedNodes;						// visit flag to be used by function pathDFS

	std::map<size_t,std::vector<size_t> > paths;			// container that store: (pctr, set of nodes in the path)
	std::map<size_t,std::vector<size_t> > loops;			// container that store: (lctr, set of nodes in the loop)
	std::vector<std::vector<bool> > pathsNloops;			// entry (i,j) is true iff path having id pctr=i and loop having id lctr = j intersects
	std::vector<std::vector<bool> > loopsNloops;			// similar to above
	std::vector<std::string> pathWeights;					// contain path weights(refer #3) arranged in increasing order of pctr 
	std::vector<std::string> loopWeights;					// similar to above
	std::map<size_t,bool> liveLoops;						// liveloops[i] is true iff loop having lctr=i is active in current transfer computation


	size_t numMults;										// number of multipliers
	std::vector<int> wMax;									// initial wordlength vector where WL of each operator is set to its maximum value. This contains WLs of multiplier constants too
	std::map<size_t,double> multConstants;					// multConstants[i] stores the constant for multiplier having id i
	std::vector<size_t> WL;									
	// hMean[i] represents the attributes {H[0]} with i-th output under considertaion in MIMO systems
	// but in the current integration only SISO systems are considered
	// in standalone WLO tool MIMO systems are also incorporated
	std::vector<std::vector<double> >hMean; 				// contains H[0] value for operators + output + input (refer #1)
	std::vector<std::vector<double> >hVar;					// similar to above
	std::vector<std::vector<double> >xtendedMean;			// see createExtensions(), this data structure is obselate and can be removed
	std::vector<std::vector<double> >xtendedVar;			
	// depList[i] contains indices of all operators in the w_opt that the ith operator in w_opt depends on
	std::vector<std::vector<size_t> > depList;				
	std::map<size_t,std::vector<size_t> > regDrivers;		// see fillRegDrivers()
	std::vector<size_t> regIndices;							// indices in wordlength vector that represent registers
	
	// datastructures for johnson's algo (refer #2)
	std::vector<size_t> st;
	std::set<size_t> blockedSet;
	std::map<size_t,size_t> blockedMap;
	
	// vector of POs. useful only in MIMO systems
	std::vector<size_t> PO;

public:
	void createExtensions();
	std::pair<double,double> getOperatorStats(size_t i, std::vector<int> WL);
	std::pair<double,double> getStats(double q, int k, int l);
	double calculateSNR(std::vector<int> WL);
	size_t getNumTrailingZeroes(float x, size_t f);
	void houseKeepRegWidths(std::vector<int>& WM, size_t j, int mode = -1);
	std::vector<int> getMWC();
	void fillRegDrivers(size_t i);
	std::vector<int> greedyAscent(std::vector<int> WL);
	int findMaxIndex(std::vector<double> arr, std::set<int> prohibitedIndices);
	double getCost(std::vector<int> WL);
	std::vector<int> tabuSearch(std::vector<int> WL);
	int findMaxIndex(std::vector<double> arr);
	int findMinIndex(std::vector<double> arr);
	
	void setMinNoise();
	void createGraph(std::map<size_t,std::vector<size_t> > xadjList,
	std::map<size_t,std::vector<size_t> > xrevadjList,
	std::map<size_t,std::string> xweights,
	std::set<size_t> xwork);
	void setParams(std::map<size_t,double>,std::vector<size_t>,size_t,size_t);
	void setsrcNdst(size_t s, size_t t);
	std::vector<int> generateWordLengths(size_t src, size_t dst);
	
	void enumPaths(size_t src, size_t dst);
	void pathDFS(size_t& src, size_t& dst);
	void printPathStack(size_t dst);

	std::vector<std::set<size_t> > enumSCC();
	void explore(size_t u);
	void DFS();
	void findSCC(size_t u,std::set<size_t>& cc, bool del = true);
	std::vector<std::set<size_t> > processDecrPost();
	void johnson();
	bool circuit(size_t u, size_t src);
	void unblock(size_t u);

	void sortPaths();
	void sortLoops();
	bool merge(std::vector<size_t>& a, std::vector<size_t>& b, size_t beg1 = 0, size_t beg2 = 0);
	std::string getNetWeight(std::vector<size_t> nodes);
	void buildpathsNloops();
	void buildloopsNloops();
	std::string getProductTerms(size_t n, size_t r, int pathindex = -1);
	void buildDenominator();
	void buildNumerator();
	void gBreak(size_t node);
	void gWane();
	void selectLiveLoops(size_t removedNode);
	void killNonReachableLoops(size_t node);

public:
	WLO()
	{
		clk = 0;
		pctr = 0;
		lctr = 0;
		//Out.open("h_of_z");
	}

};	
}

#endif
