//===-- LegupPass.cpp -----------------------------------------*- C++ -*-===//
//
// This file is distributed under the LegUp license. See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the LegupPass object
//
//===----------------------------------------------------------------------===//

#include "Allocation.h"
#include "LegupPass.h"
#include "VerilogWriter.h"
#include "utils.h"
#include "Binding.h"
#include "RTL.h"
#include "GenerateRTL.h"
#include "LegupConfig.h"
#include "Scheduler.h"
#include "ResourceEstimator.h"
//#include "llvm/Analysis/ProfileInfo.h"
#include "Debug.h"
#include <fstream>
//NC changes
#include "Debugging.h"

#include "llvm/Support/FileSystem.h"

#define DEBUG_TYPE "LegUp:LegupPass"

using namespace llvm;
using namespace legup;

namespace legup {
Debugging dbger;
std::map<const Value*,std::pair<APInt,int> > globalValInits; // (value*,init,width)	used to store init vals for creating MIFs for RAM of AC datatypes later
std::map<const Value*,std::pair<std::vector<APInt>,int> > globalArrayInits; // (value*,init,width) used to store init vals for creating MIFs for RAM of arrays of AC datatypes later
std::set<std::string> FunSet;
std::vector<std::pair<std::string,std::vector<int> > > ARGS;
extern const RTLSignal *glSig;
extern std::set<std::string> pendingFus;
extern bool isInside(std::string parent, std::string sub);
extern std::map<std::string,MDNode*> MDataLookup;
extern std::pair<std::string,std::string> modifyNameToFixed(std::string fname);
extern std::vector<int> getMDString(Instruction* instr);
extern unsigned long long getFxConstant(double inp, int fr);
extern int inpRamWidth;

std::vector<std::pair<int,int> > getFxFuTemplates(std::string str)
{
	errs()<<"in getFxFuTemplates for str: "<<str<<"\n";
	size_t f = str.find("xdot",9);
	size_t l = str.find("_",f);
	std::vector<std::pair<int,int> > ret;
	std::string pre = "";
	while(f!=std::string::npos && l!=std::string::npos)
	{
		size_t i = f - 1;
		while(str[i] != '_')
		{
			pre+=std::string(1,str[i]);
			i--;
		}
		
		std::reverse(pre.begin(),pre.end());
		std::string post = str.substr(f+4,l-f-4);
		std::pair<int,int> duplet;
		duplet = std::make_pair(atoi(pre.c_str()), atoi(post.c_str()));
		errs()<<duplet.first<<" "<<duplet.second<<"\n";
		ret.push_back(duplet);
		pre = "";
		f = str.find("xdot",l);
		l = str.find("_",f);
	}
	return ret;
}

class FunctionSortingWrapper;

class FunctionSortingWrapper {
  public:
    FunctionSortingWrapper() {
		sortNumber = 0;
	}
	;
	FunctionSortingWrapper(unsigned _sortNumber) {
		sortNumber = _sortNumber;
	}
	;
    bool operator<(const FunctionSortingWrapper &rhs) const {
      return sortNumber < rhs.sortNumber;
	}
	;
	llvm::Function *getFunction() const {
		return function;
	}
	;
	void setFunction(Function *f) {
		function = f;
	}
	;

  private:
    unsigned sortNumber;
    Function *function;
  };

  // Returns a set of functions ordered such that the   
std::vector<Function*> LegupPass::getDepthFirstSortedFunctions(Module &M) {
  // TODO: Use a more efficient algorithm for this.
  size_t previousSetSize = 0;
  std::vector<Function*> sortedSet;
  std::vector<std::string> addedFunctionNames;
  size_t addedFunctionCount = 0;
  do {
    previousSetSize = sortedSet.size();
    for (Module::iterator f = M.begin(), FE = M.end(); f != FE; ++f) {
      Function &F = *f;
			if (std::find(addedFunctionNames.begin(), addedFunctionNames.end(),
					F.getName().str()) != addedFunctionNames.end())
	continue;
      bool allCalledFunctionsAddedToSet = true;
      
      for (Function::iterator b = F.begin(), be = F.end(); b != be; ++b) {
	
				for (BasicBlock::iterator instr = b->begin(), ie = b->end();
						instr != ie; ++instr) {
					if (isaDummyCall(instr))
						continue;
	  
	  if (CallInst *CI = dyn_cast<CallInst>(instr)) {	
	    llvm::Function *called = getCalledFunction(CI);
						if (std::find(addedFunctionNames.begin(),
								addedFunctionNames.end(),
								called->getName().str())
								== addedFunctionNames.end()) {
	      allCalledFunctionsAddedToSet = false;
	      break;
	    }
	  }
	} // end Block Iterator
				if (!allCalledFunctionsAddedToSet)
					break;
      } // end Function Iterator
      if (allCalledFunctionsAddedToSet) {
	  sortedSet.push_back(&F);
	  addedFunctionNames.push_back(F.getName().str());
	  addedFunctionCount ++;
      }
    } // end Module iterator
	} while (previousSetSize != sortedSet.size()
			&& addedFunctionCount <= M.getFunctionList().size());
  assert(addedFunctionCount == M.getFunctionList().size());
  return sortedSet;
}

bool LegupPass::doInitialization(Module &M) {
	errs()<<"in LegupPass::doInitialization\n";
    allocation = new Allocation(&M);
    Scheduler::alloc = allocation;
	errs()<<"returning from LegupPass::doInitialization\n";
    // no modification
    return false;
}

// print out some statistics:
// number of instructions and basic blocks in each loop/function
void LegupPass::printBBStats(Function &F) {
	if (!LEGUP_CONFIG->getParameterInt("PRINT_BB_STATS"))
		return;

    errs() << "Statistics\n";
    // note: the loop stats will double count some basic blocks that are
    // part of multiple loops. For instance, both the inner and the outer
    // body of a loop nest will count. This may affect the calculated
    // median/average
    errs() << "Function: " << F.getName() << "\n";
    LoopInfo &LI = getAnalysis<LoopInfo>(F);
    int totalBBs = 0;
    int totalInst = 0;
    int totalMemInst = 0;
    int numLoops = 0;
    for (LoopInfo::iterator li = LI.begin(), le = LI.end(); li != le; ++li) {
        numLoops++;
        Loop *loop = *li;
        BasicBlock *loopPreheader;
        loopPreheader = loop->getLoopPreheader();
		errs() << "Loop " << numLoops << " preheader name: "
				<< getLabel(loopPreheader) << "\n";
        int numBBs = 0;
        int numInstLoop = 0;
        int numMemInstLoop = 0;
        for (Loop::block_iterator bb = loop->block_begin(), eb =
                loop->block_end(); bb != eb; ++bb) {
            BasicBlock *BB = *bb;
            int numInst = 0;
            int numMemInst = 0;
			for (BasicBlock::iterator I = BB->begin(), ie = BB->end(); I != ie;
					++I) {
                numInst++;
				if (isMem(I))
					numMemInst++;
            }
            errs() << "Number of Instructions (Loop,BB): " << numInst << "\n";
			errs() << "Number of Mem Instructions (Loop,BB): " << numMemInst
					<< "\n";

            numInstLoop += numInst;
            numMemInstLoop += numMemInst;
            numBBs++;
        }
        errs() << "Number of Instructions (Loop): " << numInstLoop << "\n";
		errs() << "Number of Mem Instructions (Loop): " << numMemInstLoop
				<< "\n";
        errs() << "Number of Basic Blocks (Loop): " << numBBs << "\n";
        totalInst += numInstLoop;
        totalMemInst += numMemInstLoop;
        totalBBs += numBBs;
    }
    errs() << "Number of Loops: " << numLoops << "\n";
    errs() << "Number of Mem Instructions (Func): " << totalMemInst << "\n";
    errs() << "Number of Basic Blocks (Func): " << totalBBs << "\n";
	if (totalBBs > 0)
		errs() << "Inst/Basic Blocks: " << totalInst / totalBBs << "\n";

    errs() << "Function: " << F.getName() << "\n";
    totalBBs = 0;
    totalInst = 0;
    totalMemInst = 0;
    for (Function::iterator BB = F.begin(), be = F.end(); BB != be; ++BB) {
        int numInst = 0;
        int numMemInst = 0;
		for (BasicBlock::iterator I = BB->begin(), ie = BB->end(); I != ie;
				++I) {
            numInst++;
			if (isMem(I))
				numMemInst++;
        }
        errs() << "Number of Instructions (BB): " << numInst << "\n";
        errs() << "Number of Mem Instructions (BB): " << numMemInst << "\n";

        totalInst += numInst;
        totalMemInst += numMemInst;
        totalBBs++;
    }
    errs() << "Number of Instructions (Func): " << totalInst << "\n";
    errs() << "Number of Mem Instructions (Func): " << totalMemInst << "\n";
    errs() << "Number of Basic Blocks (Func): " << totalBBs << "\n";
    errs() << "Inst/Basic Blocks: " << totalInst/totalBBs << "\n";
}

bool LegupPass::runOnModule(Module &M) {

    ifstream synFuncs("functions.txt");
    assert(synFuncs.is_open());
    std::string line;
	while ( getline (synFuncs,line) )
	{
		std::stringstream ss;
		ss<<line;
		std::string h,nam;
		bool in = true;
		std::vector<int> nv;
		while(ss>>h)
		{
			if(in)
			{
				FunSet.insert(h);
				in = false;
				nam = h;
			}
			else
			{
				nv.push_back(atoi(h.c_str()));
			}
		}
		ARGS.push_back(std::make_pair(nam,nv));
		
	}
	synFuncs.close();    
    
    // NC changes
    // Debugging dbger;
    DEBUG(dbgs() << "********************* runOnModule ******************************************\n");
    if (LEGUP_CONFIG->getParameterInt("INSPECT_DEBUG") ||
        LEGUP_CONFIG->getParameterInt("INSPECT_ONCHIP_BUG_DETECT_DEBUG")) {
        std::cout << "***LegUp Inspect-Debug mode is selected***" << std::endl;
        std::cout << "it is assumed that you already set NO_OPT, NO_INLINE and "
                     "DEBUG_G_FLAG Makefile variables to 1.";
        std::cout << " If not, the inspect debug behavior will be unknown."
				<< std::endl;
		if (LEGUP_CONFIG->getParameterInt("LOCAL_RAMS") != 0
				|| LEGUP_CONFIG->getParameterInt("GROUP_RAMS") != 0
				|| LEGUP_CONFIG->getParameterInt("NO_ROMS") == 0) {
			std::cout << "LOCAL_RAMS: "
					<< LEGUP_CONFIG->getParameterInt("LOCAL_RAMS") << std::endl;
			std::cout << "GROUP_RAMS: "
					<< LEGUP_CONFIG->getParameterInt("GROUP_RAMS") << std::endl;
			std::cout << "NO_ROMS: " << LEGUP_CONFIG->getParameterInt("NO_ROMS")
					<< std::endl;
			std::cout
					<< "LegUp Configuration for Inspect-Debug mode is not correct."
					<< " In order to run LegUp on Inspect-Debug mode set LOCAL_RAMS = 0; GROUP_RAMS = 0; and NO_ROMS = 1; in legup.tcl file"
					<< std::endl;
            exit(1);
        }
        
        /*Timer timer;
        timer.init(StringRef("timer"));
        timer.startTimer();*/
        dbger.initializeDatabase();
        dbger.initialize();
        /*timer.stopTimer();
        std::cout << "inspect debug database initialized: " << std::endl;*/
    }

	pipelineLabelSanityCheck(M);

	std::vector<Function *> sortedFunctionSet =
			this->getDepthFirstSortedFunctions(M);

	for (std::vector<Function *>::iterator fw = sortedFunctionSet.begin(), FWE =
			sortedFunctionSet.end(); fw != FWE; ++fw) {
	  Function &F = *(*fw);

        // can't call a function analysis pass on a function declaration
        // without a body
		if (F.isDeclaration() || LEGUP_CONFIG->isCustomVerilog(F))
			continue;

        DEBUG(errs() << "Entering function: " << F.getName() << "\n");

        // debugging: view a dot graph of function control flow graph:
        //F->viewCFG();
        
        //NC changes
		if (LEGUP_CONFIG->getParameterInt("INSPECT_DEBUG")
				|| LEGUP_CONFIG->getParameterInt(
						"INSPECT_ONCHIP_BUG_DETECT_DEBUG")) {
            dbger.fillDebugDB(&F);
        }

        // Do not codegen any 'extern' functions at all, they have
        // definitions outside the translation unit.
        if (F.hasAvailableExternallyLinkage()) {
            DEBUG(errs() << "Skipping function (extern)\n");
            continue;
        }

        // Create an RTL Generator for this function
        allocation->createGenerateRTL(&F);

        printBBStats(F);
    }
    DEBUG(errs() << "Before AA\n");
    allocation->addAA(&getAnalysis<AliasAnalysis>());	
  	DEBUG(errs() << "After AA\n");
  	
    // If software profiling was done in this compilation, read in the  
    // llvmprof.out file and cache some info about the execution
	// TODO LLVM 3.4 update.  profileInfo no longer exists.  commenting out for now.
    //if (LEGUP_CONFIG->getParameterInt("LLVM_PROFILE")) {
        //allocation->addPI(&getAnalysis<ProfileInfo>());
    //}
	int yyctr = 0;
	
	// initiation of global input array need to be processed after the computing input
	// width of the SFG. The IR function where initialization of global arrays occur in
	// function having name "__cxx_global_var_init" + [0-9]+
	std::stack<Function*> processLater;
    DEBUG(errs() << "Begin Schedule the operations in each function\n");
    // Schedule the operations in each function
    for (Allocation::hw_iterator i = allocation->hw_begin(), ie =
            allocation->hw_end(); i != ie; ++i) {
        GenerateRTL *HW = *i;yyctr++;
        Function *F = HW->getFunction();

		std::string fName = F->getName().str();
		
		// filtering out global array inits of arrays
		if(fName.find("__cxx_global_var_init") != std::string::npos && fName.size()>21)
		{
			errs()<<"marking "<<fName<<" as processLater\n";
			processLater.push(F);
			continue;
		}
		
		// TODO: figure out which functions to be synthesised
/*		if(F->getName().str().find("add_integer") == std::string::npos && 
			F->getName().str().find("main") == std::string::npos) continue;*/
			
		// synthesise only the prescribed functions which uses AC datatypes inside
/*		bool found = false;
		std::string st = F->getName().str();
		if(F->getName().str().find("ac_") != std::string::npos)
		{
		}
		// skip global value initializtaions
		else if(F->getName().str().find("GLOBAL") != std::string::npos
		|| isInside(st,"global") || isInside(st,"legup_memcpy_4"))	continue;*/
		
		bool found = false;
		for(std::set<std::string>::iterator ib = FunSet.begin(), e = FunSet.end(); ib!=e; ++ib)
		{
			found = (F->getName().str().find(*ib) != std::string::npos);
			if(found) break;
		}
		if(!found) continue;		
		

		
		HW->scheduleOperations();
    }
    // loop to process global input arrays
    // processLater kept as stack for future extension to multiple global input arrays
    while(!processLater.empty())
    {
		Function *F = processLater.top();
		processLater.pop();
		std::string fName = F->getName().str();
		errs()<<"global init: "<<fName<<"\n";
		int ctr = 0;
		int width;			
		for (Function::iterator b = F->begin(), be = F->end(); (b != be); b++) {
			for (BasicBlock::iterator instr = b->begin(), ie = b->end();
				 instr != ie; ++instr) {
					 errs()<<" curr inst: "<<*instr<<"\n";
					 // handles initialisztion calls for the array elements
					 if(isa<CallInst>(instr) && (instr->getOperand(instr->getNumOperands()-1)->getName().str().
					 find("EEC2Ei") != std::string::npos
					 || instr->getOperand(instr->getNumOperands()-1)->getName().str().find("EEC2Ed") != std::string::npos)){
							
							std::string temp = instr->getOperand(instr->getNumOperands()-1)->getName().str();
							
							// if function name contains ac_fixed we need to modify its name to ac_int
							// and attach a metadata node indicating integer wordlengths of the call's args
							if(isInside(temp,"ac_fixed"))
							{
								std::pair<std::string,std::string> p = modifyNameToFixed(temp);
								temp = p.first;
								std::string metadata = p.second;
								instr->getOperand(instr->getNumOperands()-1)->setName(temp);
						
								
								LLVMContext& C = instr->getContext();
								MDNode* N = MDNode::get(C, MDString::get(C, metadata));
								instr->setMetadata("psuedo", N);		// psuedo ac_fixed instructions
								errs()<<"set metadata for:- "<<*instr<<"\n";
								// stores MDnodes  for attaching to future call instructions of the current function name
								MDataLookup[instr->getOperand(instr->getNumOperands()-1)->getName().str()] = N;					
								
							}
							// if current call's parent function name is already modified by the above code but metadata
							// node is unattached to the instruction
							else if(MDataLookup.find(temp) != MDataLookup.end())
							{
								instr->setMetadata("psuedo", MDataLookup[temp]);	
							}
							
							// get the wordlength of the elements in array. This has to be done
							// only once. Hence ctr == 0 check is added
							if(ctr == 0)
							{
								std::string beg = "ILi";
								std::string end = "ELb";
								
								std::string fname = instr->getOperand(instr->getNumOperands()-1)->getName().str();
								
								size_t f = fname.find(beg);
								size_t l = fname.find(end);
								
								assert(f != std::string::npos && l!=std::string::npos);
								std::string h = fname.substr(f+3,(l-f-3));
								width = atoi(h.c_str());
								//ctr++;
							}
							// check for arrays or vals. GEP is not null for global arrays
							GEPOperator *GEP= dyn_cast<GEPOperator>(instr->getOperand(0));
							const ConstantInt *c = dyn_cast<ConstantInt>(instr->getOperand(1));
							ConstantFP *Fp;
							double fpval;
							int fr;
							int64_t ret;
							int64_t intval;
							// if array stores double values as inits
							if(!c)
							{
								Fp = dyn_cast<ConstantFP>(instr->getOperand(1));
								assert(Fp);
								APFloat apf = Fp->getValueAPF();
								fpval = apf.convertToDouble();
								errs()<<"orig fpval in Ram: "<<fpval<<"\n";									
							}
							else
								intval = c->getSExtValue();
							
							// if the global value has ac_fixed type, psuedo will be attached as metadata	
							if(instr->getMetadata("psuedo"))
							{
								// if WLO is ON get RAM width from as optimized WL stored in var inpRamWidth
								std::vector<int> mt = getMDString(instr);
								if(inpRamWidth != -1)		// WLO support for ac_fixed restricted to only one input array SISO systems
								{
									fr = inpRamWidth;
									errs()<<"detected input ram width as: "<<fr<<"\n";
								}
								else
									fr = width - mt[0];										
								// scale the initializtion val by the fractional WL
								if(!c)
									ret = getFxConstant(fpval,fr);
								else
									ret = getFxConstant(intval,fr);
							}
							//errs()<<"casted val: "<<c->getValue()<<"\n";
							errs()<<*(instr->getOperand(1))<<"\n";
							if(!GEP)	// non array global
							{
								globalValInits[instr->getOperand(0)] = std::make_pair(c->getValue(),width);
								//assert(0);
								break;
							}
							// FIXME: double init support only for arrays as of now
							// so write in code as a[] = {1.0,2.0} etc
							else // global arrays
							{
								Value *base = GEP->stripInBoundsConstantOffsets();
								APInt initval;
								if(c)		// this case comes only when WLO is OFF or in ac_int cases
									initval = c->getValue();
								else
								{
									if(fpval < 0)
										ret = ret*-1;
									errs()<<"float in Ram\n";
									initval = APInt(64,ret,true);
								}
								// store the initialization values in APInt format
								if(ctr == 0)
								{
									std::vector<APInt> temp;
									temp.push_back(initval);
									globalArrayInits[base] = std::make_pair(temp,width);
									ctr++;
								}
								else
								{
									assert(globalArrayInits.find(base) != globalArrayInits.end());
									globalArrayInits[base].first.push_back(initval);
								}
								errs()<<"initval: "<<initval<<"\n";
							}

					 }
			}
		}
		
	}

    DEBUG(errs() << "End Schedule the operations in each function "<<yyctr<<"\n");
    DEBUG(errs() << "Begin calculateRequiredFunctionalUnits\n");
    // Calculate the required functional units (multipliers/dividers) required
    // This requires scheduling information to get a complete picture of the
    // overall resource usage
    allocation->calculateRequiredFunctionalUnits();
	DEBUG(errs() << "End calculateRequiredFunctionalUnits\n");
	
    // Gather debugger information
    if (allocation->getDbgInfo()->isDatabaseEnabled()) {
        allocation->getDbgInfo()->generateVariableInfo();
        allocation->getDbgInfo()->analyzeProgram();
    }

	DEBUG(errs() << "RTL generation starts\n");
    // Generate the RTL
    for (Allocation::hw_iterator i = allocation->hw_begin(), ie =
            allocation->hw_end(); i != ie; ++i) {
        GenerateRTL *HW = *i;
        Function *F = HW->getFunction();

        allocation->addLVA(F, &getAnalysis<LiveVariableAnalysis>(*F));
        MinimizeBitwidth *MBW = &getAnalysis<MinimizeBitwidth>(*F);
        allocation->addLI(F, &getAnalysis<LoopInfo>(*F));
	
		bool found = false;
		for(std::set<std::string>::iterator ib = FunSet.begin(), e = FunSet.end(); ib!=e; ++ib)
		{
			found = (F->getName().str().find(*ib) != std::string::npos);
			if(found) break;
		}
		if(!found) continue;		
		
		
        RTLModule* rtl = HW->generateRTL(MBW);

        // Store the RTL for this module in the Allocation object
        allocation->addRTL(rtl);

        // Pair the rtl with its equivalent C function
        allocation->setModuleForFunction(rtl, F);

        // NC changes...
        if (LEGUP_CONFIG->getParameterInt("INSPECT_DEBUG")) {
            dbger.mapIRsToStates(HW);
        }
    }
	DEBUG(errs() << "RTL generation ends\n");
    // LLVM 3.4 update: doFinalization is called by the pass manager now?
    // doFinalization(M);

    // print the latency mismatch warnings
    LEGUP_CONFIG->printLatencyWarnings();

    // no modifications to IR so return false
    return false;
}

void LegupPass::pipelineLabelSanityCheck(Module &M) {
	std::set<std::string> pipelinedLabels;

    for (Module::iterator F = M.begin(), E = M.end(); F != E; ++F) {
        for (Function::iterator b = F->begin(), be = F->end(); b != be; ++b) {
			TerminatorInst *TI = b->getTerminator();
			if (getMetadataInt(TI, "legup.pipelined")) {
				std::string label = getMetadataStr(TI, "legup.label");
				pipelinedLabels.insert(label);
			}
		}
	}

	int totalLoopsPipelined = pipelinedLabels.size();

	int expected = LEGUP_CONFIG->numLoopPipelines();

	if (totalLoopsPipelined != expected) {
		errs() << "Error: Expected to find " << expected
			<< " loops to pipeline but only pipelined "
			<< totalLoopsPipelined << "\n";
		errs() << "Note: loop pipeline labels only work in the main .c file\n";
	}

	std::map<std::string, LegupConfig::LOOP_PIPELINE> &loop_pipelines =
		LEGUP_CONFIG->getAllLoopPipelines();

	for (std::map<std::string, LegupConfig::LOOP_PIPELINE>::iterator i =
			loop_pipelines.begin(), ie = loop_pipelines.end(); i != ie;
			++i) {
		std::string label = i->first;
		if (pipelinedLabels.find(label) == pipelinedLabels.end()) {
			errs() << "Couldn't pipeline loop with label: " << label << "\n";
		}
	}
}

bool LegupPass::doFinalization(Module &M) {
    errs()<<"in LegupPass::doFinalization\n";
    
    std::set<const Function*> AcceleratedFcts;

    if (M.begin() == M.end()) {
        llvm_unreachable("No functions exist in the module!\n");
    }

	for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
        if (LEGUP_CONFIG->isAccelerated(*F)) {
        	AcceleratedFcts.insert(F);
		}
	}

    allocation->addGlobalDefines();

    if (allocation->getDbgInfo()->isDatabaseEnabled()) {
        // Assign instance IDs to each module instance
        allocation->getDbgInfo()->assignInstances();
    }

    if (allocation->getDbgInfo()->isDebugRtlEnabled()) {
        allocation->getDbgInfo()->addDebugRtl();
    }

    if (allocation->getDbgInfo()->isDatabaseEnabled()) {
        allocation->getDbgInfo()->outputDebugDatabase();
    }

    // TODO - instantiate local RAMs in Generate RTL data structure instead of
    // verilog writer and re-enable this
    if (!LEGUP_CONFIG->getParameterInt("LOCAL_RAMS")) {
        if (!LEGUP_CONFIG->getParameterInt("KEEP_SIGNALS_WITH_NO_FANOUT")) {
            for (Allocation::const_rtl_iterator i = allocation->rtl_begin(), e =
                    allocation->rtl_end(); i != e; ++i) {
                RTLModule *rtl = *i;

                // delete register signals that are unconnected
                rtl->removeSignalsWithoutFanout();

                rtl->verifyConnections(allocation);

            }
        }
    }

    if (!LEGUP_CONFIG->getParameterInt("NO_LOOP_PIPELINING")) {
        for (Allocation::const_rtl_iterator i = allocation->rtl_begin(), e =
                allocation->rtl_end(); i != e; ++i) {
            RTLModule *rtl = *i;
            if (rtl->getName() == "main") {
                rtl->buildCircuitStructure();
                formatted_raw_ostream out(allocation->getPipelineDotFile());
                rtl->printPipelineDot(out);
            }
        }
    }


    printVerilog(AcceleratedFcts);


    printResourcesFile("resources.legup.rpt");

	std::list<GenerateRTL::PATH *> *overallPaths =
			allocation->getOverallLongestPaths();
    std::string fileName = "timingReport.overall.legup.rpt";
    std::string Error = "Error in printing timing report\n";
    raw_fd_ostream overallReport(fileName.c_str(), Error,
                                 llvm::sys::fs::F_None);
    overallReport << getFileHeader();
    GenerateRTL::printPath(overallReport, overallPaths);

    if (LEGUP_CONFIG->getParameterInt("INSPECT_DEBUG")) {
        dbger.fillHardwareInfo(allocation);
        dbger.fillSignals(allocation);
        dbger.fillVariables(allocation);

        dbger.StateStoreInfoMapping(allocation);
    } else if (LEGUP_CONFIG->getParameterInt(
                   "INSPECT_ONCHIP_BUG_DETECT_DEBUG")) {
        dbger.fillCurStateAndFinishSignals(allocation);
    }

	

	errs()<<"returning from LegupPass::doFinalization\n";

    return false;
}

LegupPass::~LegupPass() {
    assert(allocation);
    delete allocation;
}

void LegupPass::printVerilog(const std::set<const Function*> &AcceleratedFcts) {

    VerilogWriter writer(Out, allocation, AcceleratedFcts);
    writer.print();
    
   	// printing pending FUs
   	// ie writing the RTL for various fixed point modules that were instantiated
	// the RTL code will be verbatim printed and the design of these operators
	// are self explanatory from the code itself
	for(std::set<std::string>::iterator fu = pendingFus.begin(), fe = pendingFus.end(); fu!=fe; ++fu)
	{
		std::string fname = *fu;
		
		Out << "module "<<fname<<"\n";
		Out << "(\n";
		Out << "\tresult,\n";
		Out << "\tdataa";
		
		// addition/subtraction/multiplication of type ac_int
		if(isInside(fname,"ac_int_add") || isInside(fname,"ac_int_sub") || isInside(fname,"ac_int_mul"))
		{
			
			size_t b;
			bool add = isInside(fname,"ac_int_add");
			bool mul = isInside(fname,"ac_int_mul");
			if(add) b = fname.find("ac_int_add_"); 
			else if(mul) b = fname.find("ac_int_mul_");
			else b = fname.find("ac_int_sub_");
			b+=11;
			unsigned out,ina,inb;
			int uu = 0;
			while(fname.find("_",b) != std::string::npos && uu<3)
			{
				size_t c = fname.find("_",b);
				std::string s = fname.substr(b,c-b);
				b = c+1;
				int t = atoi(s.c_str());
				if(uu == 0) ina = t;
				else if(uu == 1) inb = t;
				else out = t;
				uu++;
			}
			
			errs()<<"ina inb out: "<<ina<<" "<<inb<<" "<<out<<"\n";
			
			Out << ",\n";
			Out << "\tdatab\n";
			Out << ");\n";
			Out << "\n";
			
			Out << "input ["<<utostr(ina-1)<<":0] dataa;\n";
			Out << "input ["<<utostr(inb-1)<<":0] datab;\n";
			Out << "output ["<<utostr(out-1)<<":0] result;\n";
			Out << "\n";
			
			if(add) Out << "assign result = dataa + datab;\n";
			else if(mul) Out << "assign result = dataa * datab;\n";
			else Out << "assign result = dataa - datab;\n";
			Out << "\n";
			
			Out << "endmodule\n";
			
		}
		else if(isInside(fname,"ac_int_bitadjust"))
		{
			
			size_t b = fname.find("ac_int_bitadjust_");
			unsigned out,ina;
			b+=17;
			int uu = 0;
			while(fname.find("_",b) != std::string::npos && uu<2)
			{
				size_t c = fname.find("_",b);
				std::string s = fname.substr(b,c-b);
				b = c+1;
				int t = atoi(s.c_str());
				if(uu == 0) ina = t;
				else out = t;
				uu++;
			}
			errs()<<"ina out: "<<ina<<" "<<out<<"\n";
			
			Out << "\n";
			Out << ");\n";
			Out << "\n";
			
			Out << "input ["<<utostr(ina-1)<<":0] dataa;\n";
			Out << "output ["<<utostr(out-1)<<":0] result;\n";
			Out << "\n";
			
			Out << "assign result = dataa["<<utostr(out-1)<<":0];\n";
			Out << "\n";
			
			Out << "endmodule\n";
			
		}
		else if(isInside(fname,"ac_fix_add") || isInside(fname,"ac_fix_sub") || isInside(fname,"ac_fix_mul"))
		{
			bool add = isInside(fname,"ac_fix_add");
			bool sub = isInside(fname,"ac_fix_sub");
			bool mul = isInside(fname,"ac_fix_mul");
			
			std::vector<std::pair<int,int> > ret = getFxFuTemplates(fname);
			assert(ret.size() == 3);
			std::string op0,op1;
			int i0,i1,i, f0,f1,f;
			i0 = ret[0].first;
			f0 = ret[0].second;
			i1 = ret[1].first;
			f1 = ret[1].second;
			i = ret[2].first;
			f = ret[2].second;
			Out << ",\n";
			Out << "\tdatab\n";
			Out << ");\n";
			Out << "\n";
			
			Out << "input signed ["<<utostr(i0+f0-1)<<":0] dataa;\n";
			Out << "input signed ["<<utostr(i1+f1-1)<<":0] datab;\n";
			Out << "output signed ["<<utostr(i+f-1)<<":0] result;\n";
			Out << "\n";
			
			op0 = "dataa";
			op1 = "datab";
			
			if(f0 < f1 && !mul)
			{
				Out << "wire signed ["<<utostr(i0+f1-1)<<":0] dataa_ext;\n";
				Out << "assign dataa_ext["+utostr(i0+f1-1)+":"+utostr(f1)+"-"+utostr(f0)+"] = dataa;\n";
				Out << "assign dataa_ext["+utostr(f1)+"-"+utostr(f0)+"-1:0] = "+utostr(f1-f0)+"'d0;\n";
				op0 = "dataa_ext";
			}
			else if(f0 > f1 && !mul)
			{
				Out << "wire signed ["<<utostr(i1+f0-1)<<":0] datab_ext;\n";
				Out << "assign datab_ext["+utostr(i1+f0-1)+":"+utostr(f0)+"-"+utostr(f1)+"] = datab;\n";
				Out << "assign datab_ext["+utostr(f0)+"-"+utostr(f1)+"-1:0] = "+utostr(f0-f1)+"'d0;\n";
				op1 = "datab_ext";
			}
			
			int out_i,out_f;
			int delta;
			if(add || sub)
			{
				out_i = max(i0,i1) + 1;
				out_f = max(f0,f1);
			}
			else
			{
				out_i = i0 + i1;
				out_f = f0 + f1;
			}
			delta = out_f-f;
			assert(delta >= 0);
			assert(out_i == i);
			Out << "wire signed ["<<utostr(out_i+out_f-1)<<":0] pre_out;\n";
			if(add)
				Out << "assign pre_out = "<<op0<<"+"<<op1<<";\n";
			else if(sub)
				Out << "assign pre_out = "<<op0<<"-"<<op1<<";\n";
			else
				Out << "assign pre_out = "<<op0<<"*"<<op1<<";\n";
					
			Out <<"assign result = pre_out["<<utostr(delta+i+f-1)<<":"<<utostr(delta)<<"];\n";
			Out << "\n";
			
			Out << "endmodule\n";
		}
		else if(isInside(fname,"ac_fix_bitadjust"))
		{
			std::vector<std::pair<int,int> > ret = getFxFuTemplates(fname);
			assert(ret.size() == 2);
			std::string op0,op1;
			int i0,i, f0,f;
			i0 = ret[0].first;
			f0 = ret[0].second;
			i = ret[1].first;
			f = ret[1].second;
			assert(i0+f0 >= i+f);
			
			Out << "\n";
			Out << ");\n";
			Out << "\n";
			
			Out << "input ["<<utostr(i0+f0-1)<<":0] dataa;\n";
			Out << "output ["<<utostr(i+f-1)<<":0] result;\n";
			Out << "\n";
			
			Out << "assign result = dataa["<<utostr(i+f-1)<<":0];\n";
			Out << "\n";
			
			Out << "endmodule\n";
		}
	}

    
}

void LegupPass::printResourcesFile(std::string fileName) {
    std::string EstimateError = "Error in printing early resource estimate\n";
    raw_fd_ostream resourceFile(fileName.c_str(), EstimateError, llvm::sys::fs::F_None);
    resourceFile << getFileHeader();

    ResourceEstimator estimator(allocation);
    estimator.print(resourceFile);
}

// we can't put this RegisterPass in MinimizeBitwidth.cpp because otherwise
// this statement will get linked into the shared library LLVMLegUp.so when
// compiling ../../Transforms/LegUp. This will cause an error that the
// MinimizeBitwidth pass has been registered twice when running:
//      opt -load=../../llvm/Debug+Asserts/lib/LLVMLegUp.so
char MinimizeBitwidth::ID = 0;
static RegisterPass<MinimizeBitwidth> X("legup-minimize-bitwidth",
        "Pre-Link Time Optimization Pass to shrink integer bitwidth to arbritrary precision");

} // End legup namespace

